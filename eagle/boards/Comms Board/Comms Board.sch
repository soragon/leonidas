<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Iduino-PC104">
<description>&lt;b&gt;PC/104 Standard/Plus connector&lt;/b&gt;&lt;p&gt;
Sources :
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;pc104standard.pdf&lt;/b&gt;&lt;p&gt;
PC/104 Specification, Version 2.3, June 1996&lt;p&gt;
Copyright 1992-96, PC/104 Consortium
&lt;li&gt;&lt;b&gt;PC104plus.pdf&lt;/b&gt;&lt;p&gt;
PC/104-Plus Specification, Version 1.1, June 1997&lt;p&gt;
Copyright 1992-98, PC/104 Consortium
&lt;/ul&gt;
include &lt;b&gt;Commcon Connector PC/104&lt;/b&gt;, con-commcon.lbr&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="PC104-STANDARD">
<description>&lt;b&gt;PC/104 Standard 16 bit&lt;/b&gt;</description>
<wire x1="0" y1="0" x2="90.2" y2="0" width="0" layer="20"/>
<wire x1="90.2" y1="0" x2="90.2" y2="95.9" width="0" layer="20"/>
<wire x1="90.2" y1="95.9" x2="0" y2="95.9" width="0" layer="20"/>
<wire x1="0" y1="95.9" x2="0" y2="0" width="0" layer="20"/>
<wire x1="5.3" y1="13.8" x2="86.27" y2="13.8" width="0.2032" layer="21"/>
<wire x1="86.27" y1="13.8" x2="86.27" y2="9.1" width="0.2032" layer="21"/>
<wire x1="86.27" y1="9.1" x2="5.3" y2="9.1" width="0.2032" layer="21"/>
<wire x1="5.3" y1="9.1" x2="5.3" y2="13.8" width="0.2032" layer="21"/>
<pad name="RX_GPS" x="6.35" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX_GPS" x="8.89" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDA_IMU" x="11.43" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCL_IMU" x="13.97" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="XBEE_TX" x="16.51" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="XBEE_RX" x="19.05" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN51" x="21.59" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN50" x="24.13" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MOSI" x="26.67" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MISO" x="29.21" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCK" x="31.75" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RST_SPI" x="34.29" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN22" x="36.83" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A4" x="39.37" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A3" x="41.91" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A2" x="44.45" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A1" x="46.99" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A0" x="49.53" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D1" x="52.07" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D2" x="54.61" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D3" x="57.15" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D4" x="59.69" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D5" x="62.23" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D6" x="64.77" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="5V" x="67.31" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="GND" x="69.85" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A8" x="6.35" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="3.3V" x="72.39" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A9" x="8.89" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG3" x="74.93" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A10" x="11.43" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG2" x="77.47" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A7" x="13.97" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG1" x="80.01" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SPARE" x="80.01" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDCS_CAM" x="82.55" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDA_CAM" x="82.55" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CSN_CAM" x="85.09" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCL_CAM" x="85.09" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<text x="5.715" y="14.605" size="1.27" layer="25">J1</text>
<pad name="A6" x="16.51" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A5" x="19.05" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="23" x="21.59" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="25" x="24.13" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="27" x="26.67" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="52" x="29.21" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="53" x="31.75" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A11" x="34.29" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="DAC0" x="36.83" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="DAC1" x="39.37" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RX3" x="41.91" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX3" x="44.45" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RX0" x="46.99" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX0" x="49.53" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="5" x="52.07" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="6" x="54.61" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="7" x="57.15" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="8" x="59.69" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="11" x="62.23" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="12" x="64.77" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="13" x="67.31" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RESET" x="69.85" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="IOREF" x="72.39" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CANRX" x="74.93" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CANTX" x="77.47" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<hole x="8.89" y="90.805" drill="3.5052"/>
<hole x="82.55" y="90.805" drill="3.5052"/>
<hole x="85.09" y="5.08" drill="3.5052"/>
<hole x="5.08" y="5.08" drill="3.5052"/>
</package>
</packages>
<symbols>
<symbol name="PC104-J1">
<wire x1="-12.7" y1="40.64" x2="12.7" y2="40.64" width="0.254" layer="94"/>
<wire x1="12.7" y1="40.64" x2="12.7" y2="-43.18" width="0.254" layer="94"/>
<wire x1="12.7" y1="-43.18" x2="-12.7" y2="-43.18" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-43.18" x2="-12.7" y2="40.64" width="0.254" layer="94"/>
<text x="-12.7" y="41.656" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-45.72" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SDA1" x="-15.24" y="-38.1" length="short" direction="pas"/>
<pin name="NC" x="-15.24" y="-35.56" length="short" direction="pas"/>
<pin name="SCL1" x="-15.24" y="-40.64" length="short" direction="pas"/>
<pin name="TX1" x="15.24" y="38.1" length="short" direction="pas" rot="R180"/>
<pin name="RX1" x="15.24" y="35.56" length="short" direction="pas" rot="R180"/>
<pin name="SDA" x="15.24" y="33.02" length="short" direction="pas" rot="R180"/>
<pin name="SCL" x="15.24" y="30.48" length="short" direction="pas" rot="R180"/>
<pin name="RX2" x="15.24" y="27.94" length="short" direction="pas" rot="R180"/>
<pin name="TX2" x="15.24" y="25.4" length="short" direction="pas" rot="R180"/>
<pin name="SAL_SEL" x="15.24" y="22.86" length="short" direction="pas" rot="R180"/>
<pin name="INT_CAM" x="15.24" y="20.32" length="short" direction="pas" rot="R180"/>
<pin name="MOSI" x="15.24" y="17.78" length="short" direction="pas" rot="R180"/>
<pin name="MISO" x="15.24" y="15.24" length="short" direction="pas" rot="R180"/>
<pin name="SCK" x="15.24" y="12.7" length="short" direction="pas" rot="R180"/>
<pin name="RSTN" x="15.24" y="10.16" length="short" direction="pas" rot="R180"/>
<pin name="PWR_D" x="15.24" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="SS5" x="15.24" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="SS4" x="15.24" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="SS3" x="15.24" y="0" length="short" direction="pas" rot="R180"/>
<pin name="SS2" x="15.24" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="SS1" x="15.24" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="D1" x="15.24" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="D2" x="15.24" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="5V" x="15.24" y="-22.86" length="short" direction="pas" rot="R180"/>
<pin name="D6" x="15.24" y="-20.32" length="short" direction="pas" rot="R180"/>
<pin name="D5" x="15.24" y="-17.78" length="short" direction="pas" rot="R180"/>
<pin name="D4" x="15.24" y="-15.24" length="short" direction="pas" rot="R180"/>
<pin name="D3" x="15.24" y="-12.7" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="15.24" y="-25.4" length="short" direction="pas" rot="R180"/>
<pin name="3.3V" x="15.24" y="-27.94" length="short" direction="pas" rot="R180"/>
<pin name="PWM2" x="15.24" y="-30.48" length="short" direction="pas" rot="R180"/>
<pin name="PWM3" x="15.24" y="-33.02" length="short" direction="pas" rot="R180"/>
<pin name="PWM4" x="15.24" y="-35.56" length="short" direction="pas" rot="R180"/>
<pin name="SD_CS" x="15.24" y="-38.1" length="short" direction="pas" rot="R180"/>
<pin name="CSN" x="15.24" y="-40.64" length="short" direction="pas" rot="R180"/>
<pin name="P_A4" x="-17.78" y="38.1" length="middle"/>
<pin name="P_A5" x="-17.78" y="35.56" length="middle"/>
<pin name="P_A6" x="-17.78" y="33.02" length="middle"/>
<pin name="P_A3" x="-17.78" y="30.48" length="middle"/>
<pin name="P_A2" x="-17.78" y="27.94" length="middle"/>
<pin name="P_A1" x="-17.78" y="25.4" length="middle"/>
<pin name="23" x="-17.78" y="22.86" length="middle"/>
<pin name="25" x="-17.78" y="20.32" length="middle"/>
<pin name="27" x="-17.78" y="17.78" length="middle"/>
<pin name="52" x="-17.78" y="15.24" length="middle"/>
<pin name="53" x="-17.78" y="12.7" length="middle"/>
<pin name="A11" x="-17.78" y="10.16" length="middle"/>
<pin name="DAC0" x="-17.78" y="7.62" length="middle"/>
<pin name="DAC1" x="-17.78" y="5.08" length="middle"/>
<pin name="RX3" x="-17.78" y="2.54" length="middle"/>
<pin name="TX3" x="-17.78" y="0" length="middle"/>
<pin name="RX0" x="-17.78" y="-2.54" length="middle"/>
<pin name="TX0" x="-17.78" y="-5.08" length="middle"/>
<pin name="5" x="-17.78" y="-7.62" length="middle"/>
<pin name="6" x="-17.78" y="-10.16" length="middle"/>
<pin name="7" x="-17.78" y="-12.7" length="middle"/>
<pin name="8" x="-17.78" y="-15.24" length="middle"/>
<pin name="11" x="-17.78" y="-17.78" length="middle"/>
<pin name="12" x="-17.78" y="-20.32" length="middle"/>
<pin name="13" x="-17.78" y="-22.86" length="middle"/>
<pin name="RESET" x="-17.78" y="-25.4" length="middle"/>
<pin name="IOREF" x="-17.78" y="-27.94" length="middle"/>
<pin name="CANRX" x="-17.78" y="-30.48" length="middle"/>
<pin name="CANTX" x="-17.78" y="-33.02" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IDUINO_PC104" prefix="X">
<description>&lt;b&gt;PC/104 Standard 16 bit&lt;/b&gt;</description>
<gates>
<gate name="-J1" symbol="PC104-J1" x="0" y="0"/>
</gates>
<devices>
<device name="1" package="PC104-STANDARD">
<connects>
<connect gate="-J1" pin="11" pad="11"/>
<connect gate="-J1" pin="12" pad="12"/>
<connect gate="-J1" pin="13" pad="13"/>
<connect gate="-J1" pin="23" pad="23"/>
<connect gate="-J1" pin="25" pad="25"/>
<connect gate="-J1" pin="27" pad="27"/>
<connect gate="-J1" pin="3.3V" pad="3.3V"/>
<connect gate="-J1" pin="5" pad="5"/>
<connect gate="-J1" pin="52" pad="52"/>
<connect gate="-J1" pin="53" pad="53"/>
<connect gate="-J1" pin="5V" pad="5V"/>
<connect gate="-J1" pin="6" pad="6"/>
<connect gate="-J1" pin="7" pad="7"/>
<connect gate="-J1" pin="8" pad="8"/>
<connect gate="-J1" pin="A11" pad="A11"/>
<connect gate="-J1" pin="CANRX" pad="CANRX"/>
<connect gate="-J1" pin="CANTX" pad="CANTX"/>
<connect gate="-J1" pin="CSN" pad="CSN_CAM"/>
<connect gate="-J1" pin="D1" pad="P_D1"/>
<connect gate="-J1" pin="D2" pad="P_D2"/>
<connect gate="-J1" pin="D3" pad="P_D3"/>
<connect gate="-J1" pin="D4" pad="P_D4"/>
<connect gate="-J1" pin="D5" pad="P_D5"/>
<connect gate="-J1" pin="D6" pad="P_D6"/>
<connect gate="-J1" pin="DAC0" pad="DAC0"/>
<connect gate="-J1" pin="DAC1" pad="DAC1"/>
<connect gate="-J1" pin="GND" pad="GND"/>
<connect gate="-J1" pin="INT_CAM" pad="PIN50"/>
<connect gate="-J1" pin="IOREF" pad="IOREF"/>
<connect gate="-J1" pin="MISO" pad="MISO"/>
<connect gate="-J1" pin="MOSI" pad="MOSI"/>
<connect gate="-J1" pin="NC" pad="SPARE"/>
<connect gate="-J1" pin="PWM2" pad="MAG3"/>
<connect gate="-J1" pin="PWM3" pad="MAG2"/>
<connect gate="-J1" pin="PWM4" pad="MAG1"/>
<connect gate="-J1" pin="PWR_D" pad="PIN22"/>
<connect gate="-J1" pin="P_A1" pad="A5"/>
<connect gate="-J1" pin="P_A2" pad="A6"/>
<connect gate="-J1" pin="P_A3" pad="A7"/>
<connect gate="-J1" pin="P_A4" pad="A8"/>
<connect gate="-J1" pin="P_A5" pad="A9"/>
<connect gate="-J1" pin="P_A6" pad="A10"/>
<connect gate="-J1" pin="RESET" pad="RESET"/>
<connect gate="-J1" pin="RSTN" pad="RST_SPI"/>
<connect gate="-J1" pin="RX0" pad="RX0"/>
<connect gate="-J1" pin="RX1" pad="TX_GPS"/>
<connect gate="-J1" pin="RX2" pad="XBEE_TX"/>
<connect gate="-J1" pin="RX3" pad="RX3"/>
<connect gate="-J1" pin="SAL_SEL" pad="PIN51"/>
<connect gate="-J1" pin="SCK" pad="SCK"/>
<connect gate="-J1" pin="SCL" pad="SCL_IMU"/>
<connect gate="-J1" pin="SCL1" pad="SCL_CAM"/>
<connect gate="-J1" pin="SDA" pad="SDA_IMU"/>
<connect gate="-J1" pin="SDA1" pad="SDA_CAM"/>
<connect gate="-J1" pin="SD_CS" pad="SDCS_CAM"/>
<connect gate="-J1" pin="SS1" pad="A0"/>
<connect gate="-J1" pin="SS2" pad="A1"/>
<connect gate="-J1" pin="SS3" pad="A2"/>
<connect gate="-J1" pin="SS4" pad="A3"/>
<connect gate="-J1" pin="SS5" pad="A4"/>
<connect gate="-J1" pin="TX0" pad="TX0"/>
<connect gate="-J1" pin="TX1" pad="RX_GPS"/>
<connect gate="-J1" pin="TX2" pad="XBEE_RX"/>
<connect gate="-J1" pin="TX3" pad="TX3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Comms-XBee-Adapter">
<packages>
<package name="XBEE-ADAPTER">
<wire x1="0" y1="0" x2="27.94" y2="0" width="0.127" layer="21"/>
<wire x1="27.94" y1="0" x2="27.94" y2="40.64" width="0.127" layer="21"/>
<wire x1="27.94" y1="40.64" x2="0" y2="40.64" width="0.127" layer="21"/>
<wire x1="0" y1="40.64" x2="0" y2="0" width="0.127" layer="21"/>
<pad name="3V" x="2.54" y="2.54" drill="0.8" shape="octagon"/>
<pad name="DTR" x="5.08" y="2.54" drill="0.8" shape="octagon"/>
<pad name="RST" x="7.62" y="2.54" drill="0.8" shape="octagon"/>
<pad name="GND" x="10.16" y="2.54" drill="0.8" shape="octagon"/>
<pad name="CTS" x="12.7" y="2.54" drill="0.8" shape="octagon"/>
<pad name="+5V" x="15.24" y="2.54" drill="0.8" shape="octagon"/>
<pad name="RX" x="17.78" y="2.54" drill="0.8" shape="octagon"/>
<pad name="TX" x="20.32" y="2.54" drill="0.8" shape="octagon"/>
<pad name="RTS" x="22.86" y="2.54" drill="0.8" shape="octagon"/>
<pad name="3V2" x="25.4" y="2.54" drill="0.8" shape="octagon"/>
<text x="2.032" y="-1.27" size="0.6096" layer="25" font="vector">3V</text>
<text x="4.318" y="-1.27" size="0.6096" layer="25" font="vector">DTR</text>
<text x="6.858" y="-1.27" size="0.6096" layer="25" font="vector">RST</text>
<text x="9.398" y="-1.27" size="0.6096" layer="25" font="vector">GND</text>
<text x="11.938" y="-1.27" size="0.6096" layer="25" font="vector">CTS</text>
<text x="14.478" y="-1.27" size="0.6096" layer="25" font="vector">+5V</text>
<text x="17.272" y="-1.27" size="0.6096" layer="25" font="vector">RX</text>
<text x="19.812" y="-1.27" size="0.6096" layer="25" font="vector">TX</text>
<text x="22.098" y="-1.27" size="0.6096" layer="25" font="vector">RTS</text>
<text x="24.892" y="-1.27" size="0.6096" layer="25" font="vector">3V</text>
<text x="0" y="41.275" size="1.27" layer="25" font="vector">XBee Adapter</text>
</package>
</packages>
<symbols>
<symbol name="XBEE-ADAPTER">
<wire x1="0" y1="0" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="0" x2="27.94" y2="20.32" width="0.254" layer="94"/>
<wire x1="27.94" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="3V" x="2.54" y="-5.08" length="middle" rot="R90"/>
<pin name="DTR" x="5.08" y="-5.08" length="middle" rot="R90"/>
<pin name="GND" x="10.16" y="-5.08" length="middle" rot="R90"/>
<pin name="RST" x="7.62" y="-5.08" length="middle" rot="R90"/>
<pin name="CTS" x="12.7" y="-5.08" length="middle" rot="R90"/>
<pin name="+5V" x="15.24" y="-5.08" length="middle" rot="R90"/>
<pin name="RX" x="17.78" y="-5.08" length="middle" rot="R90"/>
<pin name="TX" x="20.32" y="-5.08" length="middle" rot="R90"/>
<pin name="RTS" x="22.86" y="-5.08" length="middle" rot="R90"/>
<pin name="3V2" x="25.4" y="-5.08" length="middle" rot="R90"/>
<text x="0" y="21.336" size="1.778" layer="95" font="vector">XBEE-ADAPTER</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="XBEE-ADAPTER">
<gates>
<gate name="G$1" symbol="XBEE-ADAPTER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XBEE-ADAPTER">
<connects>
<connect gate="G$1" pin="+5V" pad="+5V"/>
<connect gate="G$1" pin="3V" pad="3V"/>
<connect gate="G$1" pin="3V2" pad="3V2"/>
<connect gate="G$1" pin="CTS" pad="CTS"/>
<connect gate="G$1" pin="DTR" pad="DTR"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RTS" pad="RTS"/>
<connect gate="G$1" pin="RX" pad="RX"/>
<connect gate="G$1" pin="TX" pad="TX"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Comms-RFM22B-S2">
<packages>
<package name="COMMS-RFM22B-S2">
<wire x1="13.97" y1="8.89" x2="13.97" y2="-8.89" width="0.127" layer="21"/>
<wire x1="13.97" y1="-8.89" x2="-13.97" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-8.89" x2="-13.97" y2="8.89" width="0.127" layer="21"/>
<wire x1="-13.97" y1="8.89" x2="13.97" y2="8.89" width="0.127" layer="21"/>
<pad name="ANT" x="-12.7" y="7.62" drill="0.8"/>
<pad name="NA" x="-12.7" y="5.08" drill="0.8"/>
<pad name="GND" x="-12.7" y="2.54" drill="0.8"/>
<pad name="3.3V" x="-12.7" y="0" drill="0.8"/>
<pad name="GPI00" x="-12.7" y="-2.54" drill="0.8"/>
<pad name="GPI01" x="-12.7" y="-5.08" drill="0.8"/>
<pad name="GPI02" x="-12.7" y="-7.62" drill="0.8"/>
<pad name="RXANT" x="12.7" y="7.62" drill="0.8"/>
<pad name="TXANT" x="12.7" y="5.08" drill="0.8"/>
<pad name="IRQ" x="12.7" y="2.54" drill="0.8"/>
<pad name="CSN" x="12.7" y="0" drill="0.8"/>
<pad name="SCK" x="12.7" y="-2.54" drill="0.8"/>
<pad name="SDI" x="12.7" y="-5.08" drill="0.8"/>
<pad name="SDO" x="12.7" y="-7.62" drill="0.8"/>
<text x="-13.97" y="10.16" size="1.27" layer="21" font="fixed">&gt;NAME</text>
<text x="-16.51" y="7.62" size="0.6096" layer="21" font="vector">ANT</text>
<text x="-16.51" y="2.54" size="0.6096" layer="21" font="vector">GND</text>
<text x="-16.51" y="0" size="0.6096" layer="21" font="vector">3.3V</text>
<text x="-16.51" y="-2.54" size="0.6096" layer="21" font="vector">GPIO0</text>
<text x="-16.51" y="-5.08" size="0.6096" layer="21" font="vector">GPIO1</text>
<text x="-16.51" y="-7.62" size="0.6096" layer="21" font="vector">GPIO2</text>
<text x="15.24" y="7.62" size="0.6096" layer="21" font="vector">RXANT</text>
<text x="15.24" y="5.08" size="0.6096" layer="21" font="vector">TXANT</text>
<text x="15.24" y="2.54" size="0.6096" layer="21" font="vector">IRQ</text>
<text x="15.24" y="0" size="0.6096" layer="21" font="vector">CSN</text>
<text x="15.24" y="-2.54" size="0.6096" layer="21" font="vector">SCK</text>
<text x="15.24" y="-5.08" size="0.6096" layer="21" font="vector">SDI</text>
<text x="15.24" y="-7.62" size="0.6096" layer="21" font="vector">SDO</text>
</package>
</packages>
<symbols>
<symbol name="COMMS-RFM22B-S2">
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<pin name="RXANT" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="TXANT" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="IRQ" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="CSN" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="SCK" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="SDI" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="SDO" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="ANT" x="-17.78" y="7.62" length="middle"/>
<pin name="NA" x="-17.78" y="5.08" length="middle"/>
<pin name="GND" x="-17.78" y="2.54" length="middle"/>
<pin name="3.3V" x="-17.78" y="0" length="middle"/>
<pin name="GPI00" x="-17.78" y="-2.54" length="middle"/>
<pin name="GPI01" x="-17.78" y="-5.08" length="middle"/>
<pin name="GPI02" x="-17.78" y="-7.62" length="middle"/>
<text x="-12.7" y="10.922" size="1.27" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="COMMS-RFM22B-S2">
<gates>
<gate name="RFM22" symbol="COMMS-RFM22B-S2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="COMMS-RFM22B-S2">
<connects>
<connect gate="RFM22" pin="3.3V" pad="3.3V"/>
<connect gate="RFM22" pin="ANT" pad="ANT"/>
<connect gate="RFM22" pin="CSN" pad="CSN"/>
<connect gate="RFM22" pin="GND" pad="GND"/>
<connect gate="RFM22" pin="GPI00" pad="GPI00"/>
<connect gate="RFM22" pin="GPI01" pad="GPI01"/>
<connect gate="RFM22" pin="GPI02" pad="GPI02"/>
<connect gate="RFM22" pin="IRQ" pad="IRQ"/>
<connect gate="RFM22" pin="NA" pad="NA"/>
<connect gate="RFM22" pin="RXANT" pad="RXANT"/>
<connect gate="RFM22" pin="SCK" pad="SCK"/>
<connect gate="RFM22" pin="SDI" pad="SDI"/>
<connect gate="RFM22" pin="SDO" pad="SDO"/>
<connect gate="RFM22" pin="TXANT" pad="TXANT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Clock-ZS-042">
<packages>
<package name="CLOCK-ZS-042">
<wire x1="0.01" y1="20.36" x2="0.01" y2="0.04" width="0.127" layer="21"/>
<wire x1="0.01" y1="0.04" x2="38.11" y2="0.04" width="0.127" layer="21"/>
<wire x1="0.01" y1="20.36" x2="38.11" y2="20.36" width="0.127" layer="21"/>
<wire x1="38.11" y1="20.36" x2="38.11" y2="0.04" width="0.127" layer="21"/>
<pad name="GND" x="1.38" y="3.58" drill="0.8"/>
<pad name="VCC" x="1.38" y="6.12" drill="0.8"/>
<pad name="SDA" x="1.38" y="8.66" drill="0.8"/>
<pad name="SCL" x="1.38" y="11.2" drill="0.8"/>
<pad name="SQW" x="1.38" y="13.74" drill="0.8"/>
<pad name="32K" x="1.38" y="16.28" drill="0.8"/>
<text x="0.1" y="20.9" size="1.27" layer="27" font="vector">&gt;NAME</text>
<text x="-2.54" y="16.08" size="0.6096" layer="21" font="vector">32K</text>
<text x="-2.52" y="13.46" size="0.6096" layer="21" font="vector">SQW</text>
<text x="-2.56" y="10.89" size="0.6096" layer="21" font="vector">SCL</text>
<text x="-2.54" y="8.4" size="0.6096" layer="21" font="vector">SDA</text>
<text x="-2.54" y="5.85" size="0.6096" layer="21" font="vector">Vcc</text>
<text x="-2.54" y="3.3" size="0.6096" layer="21" font="vector">GND</text>
</package>
</packages>
<symbols>
<symbol name="CLOCK-ZS-042">
<wire x1="-17.78" y1="10.16" x2="20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="10.16" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="10.16" width="0.254" layer="94"/>
<pin name="32K" x="-22.86" y="5.08" length="middle"/>
<pin name="SQW" x="-22.86" y="2.54" length="middle"/>
<pin name="SCL" x="-22.86" y="0" length="middle"/>
<pin name="SDA" x="-22.86" y="-2.54" length="middle"/>
<pin name="VCC" x="-22.86" y="-5.08" length="middle"/>
<pin name="GND" x="-22.86" y="-7.62" length="middle"/>
<text x="-17.78" y="10.922" size="1.778" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CLOCK-ZS-042">
<gates>
<gate name="G$1" symbol="CLOCK-ZS-042" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="CLOCK-ZS-042">
<connects>
<connect gate="G$1" pin="32K" pad="32K"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="SQW" pad="SQW"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="SMA90_DIP">
<description>SMA DIP Connector 90° 50 Ohm</description>
<wire x1="-3.15" y1="2.52" x2="-2.52" y2="3.15" width="0.2032" layer="51" curve="-90"/>
<wire x1="-2.52" y1="3.15" x2="2.52" y2="3.15" width="0.2032" layer="51"/>
<wire x1="2.52" y1="3.15" x2="3.15" y2="2.52" width="0.2032" layer="51" curve="-90"/>
<wire x1="3.15" y1="2.52" x2="3.15" y2="-2.52" width="0.2032" layer="51"/>
<wire x1="3.15" y1="-2.52" x2="2.52" y2="-3.15" width="0.2032" layer="51" curve="-90"/>
<wire x1="2.52" y1="-3.15" x2="-2.52" y2="-3.15" width="0.2032" layer="51"/>
<wire x1="-2.52" y1="-3.15" x2="-3.15" y2="-2.52" width="0.2032" layer="51" curve="-90"/>
<wire x1="-3.15" y1="-2.52" x2="-3.15" y2="2.52" width="0.2032" layer="51"/>
<wire x1="-2.725" y1="10.55" x2="-2.725" y2="3.15" width="0.2032" layer="51"/>
<wire x1="2.725" y1="10.55" x2="2.725" y2="3.15" width="0.2032" layer="51"/>
<wire x1="-2.225" y1="11.05" x2="2.225" y2="11.05" width="0.2032" layer="51"/>
<wire x1="-2.72" y1="10.565" x2="-2.235" y2="11.05" width="0.2032" layer="51" curve="-90"/>
<wire x1="2.725" y1="10.565" x2="2.24" y2="11.05" width="0.2032" layer="51" curve="90"/>
<wire x1="-2.75" y1="10" x2="-3" y2="9.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="9.75" x2="-2.75" y2="9.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="9.5" x2="-3" y2="9.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="9.25" x2="-2.75" y2="9" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="9" x2="-3" y2="8.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="8.75" x2="-2.75" y2="8.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="8.5" x2="-3" y2="8.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="8.25" x2="-2.75" y2="8" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="8" x2="-3" y2="7.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="7.75" x2="-2.75" y2="7.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="7.5" x2="-3" y2="7.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="7.25" x2="-2.75" y2="7" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="7" x2="-3" y2="6.75" width="0.2032" layer="51"/>
<wire x1="-3" y1="6.75" x2="-2.75" y2="6.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="6.5" x2="-3" y2="6.25" width="0.2032" layer="51"/>
<wire x1="-3" y1="6.25" x2="-2.75" y2="6" width="0.2032" layer="51"/>
<wire x1="2.75" y1="10" x2="3" y2="9.75" width="0.2032" layer="51"/>
<wire x1="3" y1="9.75" x2="2.75" y2="9.5" width="0.2032" layer="51"/>
<wire x1="2.75" y1="9.5" x2="3" y2="9.25" width="0.2032" layer="51"/>
<wire x1="3" y1="9.25" x2="2.75" y2="9" width="0.2032" layer="51"/>
<wire x1="2.75" y1="9" x2="3" y2="8.75" width="0.2032" layer="51"/>
<wire x1="3" y1="8.75" x2="2.75" y2="8.5" width="0.2032" layer="51"/>
<wire x1="2.75" y1="8.5" x2="3" y2="8.25" width="0.2032" layer="51"/>
<wire x1="3" y1="8.25" x2="2.75" y2="8" width="0.2032" layer="51"/>
<wire x1="2.75" y1="8" x2="3" y2="7.75" width="0.2032" layer="51"/>
<wire x1="3" y1="7.75" x2="2.75" y2="7.5" width="0.2032" layer="51"/>
<wire x1="2.75" y1="7.5" x2="3" y2="7.25" width="0.2032" layer="51"/>
<wire x1="3" y1="7.25" x2="2.75" y2="7" width="0.2032" layer="51"/>
<wire x1="2.75" y1="7" x2="3" y2="6.75" width="0.2032" layer="51"/>
<wire x1="3" y1="6.75" x2="2.75" y2="6.5" width="0.2032" layer="51"/>
<wire x1="2.75" y1="6.5" x2="3" y2="6.25" width="0.2032" layer="51"/>
<wire x1="3" y1="6.25" x2="2.75" y2="6" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="6" x2="2.75" y2="6.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="6.5" x2="2.75" y2="7" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="7" x2="2.75" y2="7.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="7.5" x2="2.75" y2="8" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="8" x2="2.75" y2="8.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="8.5" x2="2.75" y2="9" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="9" x2="2.75" y2="9.5" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="9.5" x2="2.75" y2="10" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="10" x2="2.75" y2="10" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="6" x2="2.75" y2="6" width="0.2032" layer="51"/>
<pad name="GND1" x="-2.54" y="2.54" drill="1.6" diameter="2.54"/>
<pad name="GND2" x="2.54" y="2.54" drill="1.6" diameter="2.54"/>
<pad name="GND4" x="2.54" y="-2.54" drill="1.6" diameter="2.54"/>
<pad name="GND3" x="-2.54" y="-2.54" drill="1.6" diameter="2.54"/>
<pad name="ANTENNA" x="0" y="0" drill="1.5" diameter="2.54"/>
<text x="-2.032" y="4.826" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.032" y="4.064" size="0.4064" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SMA_EDGELAUNCH">
<wire x1="-9.2075" y1="2.54" x2="-8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-8.255" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="3.175" x2="-7.62" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="3.175" x2="-6.985" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="3.175" x2="-5.08" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-4.445" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="3.175" x2="-3.4925" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="3.175" x2="-3.4925" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="2.54" x2="-1.5875" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="2.54" x2="-1.5875" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="-2.54" x2="-3.4925" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="-2.54" x2="-3.4925" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="-3.175" x2="-4.445" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="-3.175" x2="-5.08" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="-3.175" x2="-5.715" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="-3.175" x2="-6.35" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="-3.175" x2="-6.985" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="-3.175" x2="-7.62" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-3.175" x2="-8.255" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-3.175" x2="-8.255" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-2.54" x2="-9.2075" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-9.2075" y1="-2.54" x2="-9.2075" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-8.255" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="3.175" x2="-7.62" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="3.175" x2="-6.985" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="3.175" x2="-5.08" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-4.445" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="3.175" x2="-3.81" y2="-3.175" width="0.2032" layer="51"/>
<smd name="GND@2" x="2.032" y="2.54" dx="1.524" dy="4.064" layer="1" rot="R90" cream="no"/>
<smd name="GND@1" x="2.032" y="-2.54" dx="1.524" dy="4.064" layer="1" rot="R90" cream="no"/>
<smd name="P" x="2.032" y="0" dx="1.27" dy="4.064" layer="1" rot="R90" thermals="no" cream="no"/>
<smd name="GND@3" x="2.032" y="2.54" dx="1.524" dy="4.064" layer="16" rot="R90" cream="no"/>
<smd name="GND@4" x="2.032" y="-2.54" dx="1.524" dy="4.064" layer="16" rot="R90" cream="no"/>
<text x="5.08" y="6.985" size="1.016" layer="25" ratio="18" rot="R180">&gt;NAME</text>
<text x="5.08" y="-7.62" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<text x="5.08" y="-6.35" size="1.016" layer="25" ratio="18" rot="R180">&gt;LABEL</text>
<rectangle x1="0" y1="-0.3175" x2="3.175" y2="0.3175" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="-1.524" y="3.048"/>
<vertex x="3.81" y="3.048"/>
<vertex x="3.81" y="2.032"/>
<vertex x="0" y="2.032"/>
<vertex x="0" y="-2.032"/>
<vertex x="3.81" y="-2.032"/>
<vertex x="3.81" y="-3.048"/>
<vertex x="-1.524" y="-3.048"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.524" y="5.08"/>
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="-1.524" y="-5.08"/>
</polygon>
</package>
<package name="SMA_EDGELAUNCH_UFL">
<wire x1="-9.2075" y1="2.54" x2="-8.255" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-8.255" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="3.175" x2="-7.62" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="3.175" x2="-6.985" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="3.175" x2="-5.08" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-4.445" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="3.175" x2="-3.4925" y2="3.175" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="3.175" x2="-3.4925" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="2.54" x2="-1.5875" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="2.54" x2="-1.5875" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.5875" y1="-2.54" x2="-3.4925" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="-2.54" x2="-3.4925" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-3.4925" y1="-3.175" x2="-4.445" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="-3.175" x2="-5.08" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="-3.175" x2="-5.715" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="-3.175" x2="-6.35" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="-3.175" x2="-6.985" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="-3.175" x2="-7.62" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="-3.175" x2="-8.255" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-3.175" x2="-8.255" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="-2.54" x2="-9.2075" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-9.2075" y1="-2.54" x2="-9.2075" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="2.54" x2="-8.255" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-8.255" y1="3.175" x2="-7.62" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="3.175" x2="-6.985" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="3.175" x2="-5.715" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="3.175" x2="-5.08" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-4.445" y2="-3.175" width="0.2032" layer="51"/>
<wire x1="-4.445" y1="3.175" x2="-3.81" y2="-3.175" width="0.2032" layer="51"/>
<smd name="GND@2" x="2.032" y="2.54" dx="1.524" dy="4.064" layer="1" rot="R90" cream="no"/>
<smd name="GND@1" x="2.032" y="-2.54" dx="1.524" dy="4.064" layer="1" rot="R90" cream="no"/>
<smd name="P" x="3.048" y="0" dx="1.016" dy="2.032" layer="1" rot="R90" thermals="no" cream="no"/>
<smd name="GND@3" x="2.032" y="2.54" dx="1.524" dy="4.064" layer="16" rot="R90" cream="no"/>
<smd name="GND@4" x="2.032" y="-2.54" dx="1.524" dy="4.064" layer="16" rot="R90" cream="no"/>
<text x="5.08" y="6.985" size="1.016" layer="25" ratio="18" rot="R180">&gt;NAME</text>
<text x="5.08" y="-7.62" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<text x="5.08" y="-6.35" size="1.016" layer="25" ratio="18" rot="R180">&gt;LABEL</text>
<rectangle x1="0" y1="-0.3175" x2="3.175" y2="0.3175" layer="51"/>
<polygon width="0.127" layer="51">
<vertex x="-1.524" y="3.048"/>
<vertex x="3.81" y="3.048"/>
<vertex x="3.81" y="2.032"/>
<vertex x="0" y="2.032"/>
<vertex x="0" y="-2.032"/>
<vertex x="3.81" y="-2.032"/>
<vertex x="3.81" y="-3.048"/>
<vertex x="-1.524" y="-3.048"/>
</polygon>
<polygon width="0.127" layer="51">
<vertex x="-1.524" y="5.08"/>
<vertex x="0" y="5.08"/>
<vertex x="0" y="-5.08"/>
<vertex x="-1.524" y="-5.08"/>
</polygon>
<wire x1="3.3574" y1="0.7" x2="3.3574" y2="2.1" width="0.2032" layer="51"/>
<wire x1="3.3574" y1="2.1" x2="0.7574" y2="2.1" width="0.2032" layer="51"/>
<wire x1="0.7574" y1="-2.1" x2="3.3574" y2="-2.1" width="0.2032" layer="51"/>
<wire x1="3.3574" y1="-2.1" x2="3.3574" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="0.7574" y1="0.7" x2="0.7574" y2="2.1" width="0.2032" layer="51"/>
<wire x1="0.7574" y1="-2.1" x2="0.7574" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="3.4574" y1="0.7" x2="3.4574" y2="2" width="0.2032" layer="51"/>
<wire x1="3.4574" y1="-2" x2="3.4574" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="0.7" x2="3.5574" y2="2.1" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="-2.1" x2="3.5574" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="2.1" x2="3.3574" y2="2.1" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="0.7" x2="3.3574" y2="0.7" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="-0.7" x2="3.3574" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="3.5574" y1="-2.1" x2="3.3574" y2="-2.1" width="0.2032" layer="51"/>
<rectangle x1="3.2004" y1="-0.4826" x2="3.9624" y2="0.4826" layer="31"/>
<rectangle x1="0.889" y1="0.9382" x2="3.2258" y2="1.8526" layer="29"/>
<polygon width="0.2032" layer="1">
<vertex x="1.016" y="1.0652"/>
<vertex x="1.016" y="3.175"/>
<vertex x="3.0988" y="3.175"/>
<vertex x="3.0988" y="1.0652"/>
</polygon>
<rectangle x1="0.9906" y1="0.989" x2="3.1242" y2="1.8018" layer="31"/>
<rectangle x1="0.889" y1="-1.8526" x2="3.2258" y2="-0.9382" layer="29" rot="R180"/>
<polygon width="0.2032" layer="1">
<vertex x="3.0988" y="-1.0652"/>
<vertex x="3.0988" y="-3.175"/>
<vertex x="1.016" y="-3.175"/>
<vertex x="1.016" y="-1.0652"/>
</polygon>
<rectangle x1="0.9906" y1="-1.8018" x2="3.1242" y2="-0.989" layer="31" rot="R180"/>
<rectangle x1="0.9144" y1="-0.8636" x2="3.2004" y2="-0.635" layer="41"/>
<rectangle x1="0.9144" y1="0.635" x2="3.2004" y2="0.8636" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="SMACONNECTOR">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.175" y2="3.81" width="0.127" layer="94"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.81" width="0.127" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.175" y2="3.175" width="0.127" layer="94"/>
<wire x1="3.175" y1="3.175" x2="2.54" y2="3.81" width="0.127" layer="94"/>
<wire x1="3.175" y1="3.81" x2="3.175" y2="2.54" width="0.127" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.3592" width="0.8128" layer="94"/>
<text x="7.62" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="0" size="1.27" layer="95">&gt;VALUE</text>
<pin name="ANT" x="-7.62" y="0" visible="pin" length="short"/>
<pin name="GND1" x="-2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="GND2" x="2.54" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="GND3" x="-2.54" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<pin name="GND4" x="2.54" y="-7.62" visible="off" length="short" direction="pwr" rot="R90"/>
<wire x1="-5.08" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMACONNECTOR" prefix="X" uservalue="yes">
<description>&lt;b&gt;SMA Connector&lt;/b&gt;
&lt;p&gt;90° DIP SMA Connector, 50 Ohm (4UConnector: 07259)&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SMACONNECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMA90_DIP">
<connects>
<connect gate="G$1" pin="ANT" pad="ANTENNA"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="GND3" pad="GND3"/>
<connect gate="G$1" pin="GND4" pad="GND4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_EDGE" package="SMA_EDGELAUNCH">
<connects>
<connect gate="G$1" pin="ANT" pad="P"/>
<connect gate="G$1" pin="GND1" pad="GND@4"/>
<connect gate="G$1" pin="GND2" pad="GND@3"/>
<connect gate="G$1" pin="GND3" pad="GND@2"/>
<connect gate="G$1" pin="GND4" pad="GND@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_EDGE_UFL" package="SMA_EDGELAUNCH_UFL">
<connects>
<connect gate="G$1" pin="ANT" pad="P"/>
<connect gate="G$1" pin="GND1" pad="GND@1"/>
<connect gate="G$1" pin="GND2" pad="GND@2"/>
<connect gate="G$1" pin="GND3" pad="GND@3"/>
<connect gate="G$1" pin="GND4" pad="GND@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="Iduino-PC104" deviceset="IDUINO_PC104" device="1"/>
<part name="U$1" library="Comms-XBee-Adapter" deviceset="XBEE-ADAPTER" device=""/>
<part name="RFM22B" library="Comms-RFM22B-S2" deviceset="COMMS-RFM22B-S2" device=""/>
<part name="RTC" library="Clock-ZS-042" deviceset="CLOCK-ZS-042" device=""/>
<part name="SMA" library="adafruit" deviceset="SMACONNECTOR" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="X1" gate="-J1" x="12.7" y="43.18"/>
<instance part="U$1" gate="G$1" x="55.88" y="88.9"/>
<instance part="RFM22B" gate="RFM22" x="66.04" y="38.1"/>
<instance part="RTC" gate="G$1" x="76.2" y="2.54"/>
<instance part="SMA" gate="G$1" x="111.76" y="20.32"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="RX2"/>
<wire x1="27.94" y1="71.12" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TX"/>
<wire x1="76.2" y1="71.12" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RX"/>
<wire x1="73.66" y1="83.82" x2="73.66" y2="68.58" width="0.1524" layer="91"/>
<pinref part="X1" gate="-J1" pin="TX2"/>
<wire x1="73.66" y1="68.58" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="+5V"/>
<wire x1="71.12" y1="83.82" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<label x="71.12" y="78.74" size="1.016" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="5V"/>
<wire x1="27.94" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<label x="35.56" y="20.32" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RTC" gate="G$1" pin="VCC"/>
<wire x1="53.34" y1="-2.54" x2="48.26" y2="-2.54" width="0.1524" layer="91"/>
<label x="48.26" y="-2.54" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="66.04" y1="83.82" x2="66.04" y2="78.74" width="0.1524" layer="91"/>
<label x="66.04" y="78.74" size="1.016" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="GND"/>
<wire x1="27.94" y1="17.78" x2="35.56" y2="17.78" width="0.1524" layer="91"/>
<label x="35.56" y="17.78" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RFM22B" gate="RFM22" pin="GND"/>
<wire x1="48.26" y1="40.64" x2="43.18" y2="40.64" width="0.1524" layer="91"/>
<label x="43.18" y="40.64" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RTC" gate="G$1" pin="GND"/>
<wire x1="53.34" y1="-5.08" x2="48.26" y2="-5.08" width="0.1524" layer="91"/>
<label x="48.26" y="-5.08" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SMA" gate="G$1" pin="GND3"/>
<wire x1="109.22" y1="12.7" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<label x="109.22" y="10.16" size="1.016" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="SMA" gate="G$1" pin="GND4"/>
<wire x1="114.3" y1="12.7" x2="114.3" y2="10.16" width="0.1524" layer="91"/>
<label x="114.3" y="10.16" size="1.016" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="SMA" gate="G$1" pin="GND1"/>
<wire x1="109.22" y1="27.94" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<label x="109.22" y="30.48" size="1.016" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="SMA" gate="G$1" pin="GND2"/>
<wire x1="114.3" y1="27.94" x2="114.3" y2="30.48" width="0.1524" layer="91"/>
<label x="114.3" y="30.48" size="1.016" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="NSEL" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="CSN"/>
<wire x1="83.82" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<label x="88.9" y="38.1" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="52"/>
<wire x1="-5.08" y1="58.42" x2="-10.16" y2="58.42" width="0.1524" layer="91"/>
<label x="-10.16" y="58.42" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IRQ" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="IRQ"/>
<wire x1="83.82" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="88.9" y="40.64" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="53"/>
<wire x1="-5.08" y1="55.88" x2="-10.16" y2="55.88" width="0.1524" layer="91"/>
<label x="-10.16" y="55.88" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="SCK"/>
<wire x1="83.82" y1="35.56" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
<wire x1="99.06" y1="35.56" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<pinref part="X1" gate="-J1" pin="SCK"/>
<wire x1="99.06" y1="55.88" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="MOSI"/>
<wire x1="27.94" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="60.96" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RFM22B" gate="RFM22" pin="SDI"/>
<wire x1="101.6" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="MISO"/>
<wire x1="27.94" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="58.42" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<pinref part="RFM22B" gate="RFM22" pin="SDO"/>
<wire x1="104.14" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="GPI00"/>
<wire x1="48.26" y1="35.56" x2="38.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="38.1" y1="35.56" x2="38.1" y2="50.8" width="0.1524" layer="91"/>
<wire x1="38.1" y1="50.8" x2="91.44" y2="50.8" width="0.1524" layer="91"/>
<wire x1="91.44" y1="50.8" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<pinref part="RFM22B" gate="RFM22" pin="TXANT"/>
<wire x1="91.44" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="GPI01"/>
<wire x1="48.26" y1="33.02" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<wire x1="35.56" y1="33.02" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="35.56" y1="53.34" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="93.98" y1="53.34" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<pinref part="RFM22B" gate="RFM22" pin="RXANT"/>
<wire x1="93.98" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="3.3V"/>
<wire x1="27.94" y1="15.24" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<label x="35.56" y="15.24" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RFM22B" gate="RFM22" pin="3.3V"/>
<wire x1="48.26" y1="38.1" x2="43.18" y2="38.1" width="0.1524" layer="91"/>
<label x="43.18" y="38.1" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="SDA"/>
<wire x1="27.94" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
<label x="38.1" y="76.2" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RTC" gate="G$1" pin="SDA"/>
<wire x1="53.34" y1="0" x2="48.26" y2="0" width="0.1524" layer="91"/>
<label x="48.26" y="0" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="SCL"/>
<wire x1="27.94" y1="73.66" x2="38.1" y2="73.66" width="0.1524" layer="91"/>
<label x="38.1" y="73.66" size="1.016" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RTC" gate="G$1" pin="SCL"/>
<wire x1="53.34" y1="2.54" x2="48.26" y2="2.54" width="0.1524" layer="91"/>
<label x="48.26" y="2.54" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANT" class="0">
<segment>
<pinref part="RFM22B" gate="RFM22" pin="ANT"/>
<wire x1="48.26" y1="45.72" x2="43.18" y2="45.72" width="0.1524" layer="91"/>
<label x="43.18" y="45.72" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SMA" gate="G$1" pin="ANT"/>
<wire x1="104.14" y1="20.32" x2="99.06" y2="20.32" width="0.1524" layer="91"/>
<label x="99.06" y="20.32" size="1.016" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
