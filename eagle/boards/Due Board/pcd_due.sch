<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Iduino-Due">
<packages>
<package name="IDUINO-DUE">
<wire x1="-43.3" y1="28" x2="-43.3" y2="-25.3" width="0.127" layer="20"/>
<pad name="TX3" x="37.211" y="23.114" drill="0.8"/>
<pad name="RX3" x="37.211" y="20.574" drill="0.8"/>
<pad name="TX2" x="37.211" y="18.034" drill="0.8"/>
<pad name="RX2" x="37.211" y="15.494" drill="0.8"/>
<pad name="TX1" x="37.211" y="12.954" drill="0.8"/>
<pad name="RX1" x="37.211" y="10.414" drill="0.8"/>
<pad name="SDA" x="37.211" y="7.874" drill="0.8"/>
<pad name="SCL" x="37.211" y="5.334" drill="0.8"/>
<pad name="CANTX" x="37.211" y="-2.286" drill="0.8"/>
<pad name="CANRX" x="37.211" y="-4.826" drill="0.8"/>
<pad name="DAC1" x="37.211" y="-7.366" drill="0.8"/>
<pad name="DAC0" x="37.211" y="-9.906" drill="0.8"/>
<pad name="A11" x="37.211" y="-12.446" drill="0.8"/>
<pad name="A10" x="37.211" y="-14.986" drill="0.8"/>
<pad name="A9" x="37.211" y="-17.526" drill="0.8"/>
<pad name="A8" x="37.211" y="-20.066" drill="0.8"/>
<pad name="5V2" x="31.75" y="23.114" drill="0.8"/>
<pad name="23" x="31.75" y="20.574" drill="0.8"/>
<pad name="25" x="31.75" y="18.034" drill="0.8"/>
<pad name="51" x="31.75" y="-14.986" drill="0.8"/>
<pad name="53" x="31.75" y="-17.526" drill="0.8"/>
<pad name="GND-BASE2" x="31.75" y="-20.066" drill="0.8"/>
<pad name="5V1" x="29.21" y="23.114" drill="0.8"/>
<pad name="22" x="29.21" y="20.574" drill="0.8"/>
<pad name="24" x="29.21" y="18.034" drill="0.8"/>
<pad name="50" x="29.21" y="-14.986" drill="0.8"/>
<pad name="52" x="29.21" y="-17.526" drill="0.8"/>
<pad name="GND-BASE1" x="29.21" y="-20.066" drill="0.8"/>
<pad name="RX0" x="19.812" y="25.4" drill="0.8"/>
<pad name="TX0" x="17.272" y="25.4" drill="0.8"/>
<pad name="PWM2" x="14.732" y="25.4" drill="0.8"/>
<pad name="PWM3" x="12.192" y="25.4" drill="0.8"/>
<pad name="PWM4" x="9.652" y="25.4" drill="0.8"/>
<pad name="PWM5" x="7.112" y="25.4" drill="0.8"/>
<pad name="PWM6" x="4.572" y="25.4" drill="0.8"/>
<pad name="PWM7" x="2.032" y="25.4" drill="0.8"/>
<pad name="PWM8" x="-2.286" y="25.4" drill="0.8"/>
<pad name="PWM9" x="-4.826" y="25.4" drill="0.8"/>
<pad name="PWM10" x="-7.366" y="25.4" drill="0.8"/>
<pad name="PWM11" x="-9.906" y="25.4" drill="0.8"/>
<pad name="PWM12" x="-12.446" y="25.4" drill="0.8"/>
<pad name="PWM13" x="-14.986" y="25.4" drill="0.8"/>
<pad name="GND" x="-17.526" y="25.4" drill="0.8"/>
<pad name="AREF" x="-20.066" y="25.4" drill="0.8"/>
<pad name="SDA1" x="-22.606" y="25.4" drill="0.8"/>
<pad name="SCL1" x="-25.146" y="25.4" drill="0.8"/>
<pad name="A7" x="24.384" y="-22.86" drill="0.8"/>
<pad name="A6" x="21.844" y="-22.86" drill="0.8"/>
<pad name="A4" x="16.764" y="-22.86" drill="0.8"/>
<pad name="A5" x="19.304" y="-22.86" drill="0.8"/>
<pad name="A3" x="14.224" y="-22.86" drill="0.8"/>
<pad name="A2" x="11.684" y="-22.86" drill="0.8"/>
<pad name="A1" x="9.144" y="-22.86" drill="0.8"/>
<pad name="A0" x="6.604" y="-22.86" drill="0.8"/>
<pad name="VIN" x="1.524" y="-22.86" drill="0.8"/>
<pad name="GND-POWER2" x="-1.016" y="-22.86" drill="0.8"/>
<pad name="GND-POWER1" x="-3.556" y="-22.86" drill="0.8"/>
<pad name="5V" x="-6.096" y="-22.86" drill="0.8"/>
<pad name="RESET" x="-11.176" y="-22.86" drill="0.8"/>
<pad name="3V3" x="-8.636" y="-22.86" drill="0.8"/>
<pad name="I0REF" x="-13.716" y="-22.86" drill="0.8"/>
<pad name="NOTUSED" x="-16.256" y="-22.86" drill="0.8"/>
<hole x="-30.734" y="25.4" drill="4.365625"/>
<hole x="39.624" y="1.524" drill="4.365625"/>
<pad name="5V-SPI" x="24.003" y="4.572" drill="0.8"/>
<pad name="MOSI" x="24.003" y="2.032" drill="0.8"/>
<pad name="GND-SPI" x="24.003" y="-0.508" drill="0.8"/>
<pad name="RESET-SPI" x="21.463" y="-0.508" drill="0.8"/>
<pad name="SCK" x="21.463" y="2.032" drill="0.8"/>
<pad name="MISO" x="21.463" y="4.572" drill="0.8"/>
<wire x1="41" y1="27" x2="41" y2="15" width="0.127" layer="21"/>
<wire x1="41" y1="15" x2="43" y2="13" width="0.127" layer="21"/>
<wire x1="41" y1="-25.3" x2="41" y2="-23" width="0.127" layer="21"/>
<wire x1="41" y1="-23" x2="43" y2="-21" width="0.127" layer="21"/>
<wire x1="-43.3" y1="-25.3" x2="41" y2="-25.3" width="0.127" layer="21"/>
<wire x1="43" y1="-21" x2="43" y2="13" width="0.127" layer="21"/>
<wire x1="-43.3" y1="28" x2="40" y2="28" width="0.127" layer="21"/>
<wire x1="40" y1="28" x2="41" y2="27" width="0.127" layer="21"/>
<pad name="26" x="29.21" y="15.494" drill="0.8"/>
<pad name="27" x="31.75" y="15.494" drill="0.8"/>
<pad name="28" x="29.21" y="12.954" drill="0.8"/>
<pad name="29" x="31.75" y="12.954" drill="0.8"/>
<pad name="30" x="29.21" y="10.414" drill="0.8"/>
<pad name="31" x="31.75" y="10.414" drill="0.8"/>
</package>
</packages>
<symbols>
<symbol name="IDUINO-DUE">
<wire x1="-25.4" y1="50.8" x2="25.4" y2="50.8" width="0.254" layer="94"/>
<wire x1="25.4" y1="50.8" x2="25.4" y2="-50.8" width="0.254" layer="94"/>
<wire x1="25.4" y1="-50.8" x2="-25.4" y2="-50.8" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-50.8" x2="-25.4" y2="50.8" width="0.254" layer="94"/>
<pin name="NOTUSED" x="-30.48" y="45.72" length="middle"/>
<pin name="IOREF" x="-30.48" y="43.18" length="middle"/>
<pin name="RESET" x="-30.48" y="40.64" length="middle"/>
<pin name="5V" x="-30.48" y="35.56" length="middle"/>
<pin name="3V3" x="-30.48" y="38.1" length="middle"/>
<pin name="GND-POWER1" x="-30.48" y="33.02" length="middle"/>
<pin name="GND-POWER2" x="-30.48" y="30.48" length="middle"/>
<pin name="VIN" x="-30.48" y="27.94" length="middle"/>
<pin name="A0" x="-30.48" y="22.86" length="middle"/>
<pin name="A1" x="-30.48" y="20.32" length="middle"/>
<pin name="A2" x="-30.48" y="17.78" length="middle"/>
<pin name="A3" x="-30.48" y="15.24" length="middle"/>
<pin name="A4" x="-30.48" y="12.7" length="middle"/>
<pin name="A5" x="-30.48" y="10.16" length="middle"/>
<pin name="A6" x="-30.48" y="7.62" length="middle"/>
<pin name="A7" x="-30.48" y="5.08" length="middle"/>
<pin name="SCL1" x="-30.48" y="0" length="middle"/>
<pin name="SDA1" x="-30.48" y="-2.54" length="middle"/>
<pin name="AREF" x="-30.48" y="-5.08" length="middle"/>
<pin name="GND" x="-30.48" y="-7.62" length="middle"/>
<pin name="PWM12" x="-30.48" y="-12.7" length="middle"/>
<pin name="PWM13" x="-30.48" y="-10.16" length="middle"/>
<pin name="PWM11" x="-30.48" y="-15.24" length="middle"/>
<pin name="PWM10" x="-30.48" y="-17.78" length="middle"/>
<pin name="PWM9" x="-30.48" y="-20.32" length="middle"/>
<pin name="PWM8" x="-30.48" y="-22.86" length="middle"/>
<pin name="PWM7" x="-30.48" y="-27.94" length="middle"/>
<pin name="PWM6" x="-30.48" y="-30.48" length="middle"/>
<pin name="PWM5" x="-30.48" y="-33.02" length="middle"/>
<pin name="PWM4" x="-30.48" y="-35.56" length="middle"/>
<pin name="PWM3" x="-30.48" y="-38.1" length="middle"/>
<pin name="PWM2" x="-30.48" y="-40.64" length="middle"/>
<pin name="TX0" x="-30.48" y="-43.18" length="middle"/>
<pin name="RX0" x="-30.48" y="-45.72" length="middle"/>
<pin name="5V1" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="5V2" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="22" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="23" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="24" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="25" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="50" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="51" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="52" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="53" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="GND-BOTTOM1" x="30.48" y="-40.64" length="middle" rot="R180"/>
<pin name="GND-BOTTOM2" x="30.48" y="-43.18" length="middle" rot="R180"/>
<pin name="A9" x="17.78" y="-55.88" length="middle" rot="R90"/>
<pin name="A8" x="20.32" y="-55.88" length="middle" rot="R90"/>
<pin name="A10" x="15.24" y="-55.88" length="middle" rot="R90"/>
<pin name="A11" x="12.7" y="-55.88" length="middle" rot="R90"/>
<pin name="CANRX" x="5.08" y="-55.88" length="middle" rot="R90"/>
<pin name="DAC1" x="7.62" y="-55.88" length="middle" rot="R90"/>
<pin name="DAC0" x="10.16" y="-55.88" length="middle" rot="R90"/>
<pin name="CANTX" x="2.54" y="-55.88" length="middle" rot="R90"/>
<pin name="SCL" x="-2.54" y="-55.88" length="middle" rot="R90"/>
<pin name="SDA" x="-5.08" y="-55.88" length="middle" rot="R90"/>
<pin name="RX1" x="-7.62" y="-55.88" length="middle" rot="R90"/>
<pin name="TX1" x="-10.16" y="-55.88" length="middle" rot="R90"/>
<pin name="RX2" x="-12.7" y="-55.88" length="middle" rot="R90"/>
<pin name="TX2" x="-15.24" y="-55.88" length="middle" rot="R90"/>
<pin name="RX3" x="-17.78" y="-55.88" length="middle" rot="R90"/>
<pin name="TX3" x="-20.32" y="-55.88" length="middle" rot="R90"/>
<pin name="5V-SPI" x="2.54" y="55.88" length="middle" rot="R270"/>
<pin name="RESET-SPI" x="0" y="55.88" length="middle" rot="R270"/>
<pin name="MOSI" x="5.08" y="55.88" length="middle" rot="R270"/>
<pin name="SCK" x="-2.54" y="55.88" length="middle" rot="R270"/>
<pin name="GND-SPI" x="7.62" y="55.88" length="middle" rot="R270"/>
<pin name="MISO" x="-5.08" y="55.88" length="middle" rot="R270"/>
<text x="-25.4" y="53.34" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="26" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="27" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="28" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="29" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="30" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="31" x="30.48" y="17.78" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IDUINO-DUE" prefix="DUE">
<description>This is a library file for the headout pins of the Idunio Due Pro.</description>
<gates>
<gate name="G$1" symbol="IDUINO-DUE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IDUINO-DUE">
<connects>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="5V-SPI" pad="5V-SPI"/>
<connect gate="G$1" pin="5V1" pad="5V1"/>
<connect gate="G$1" pin="5V2" pad="5V2"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A10" pad="A10"/>
<connect gate="G$1" pin="A11" pad="A11"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="A8" pad="A8"/>
<connect gate="G$1" pin="A9" pad="A9"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="CANRX" pad="CANRX"/>
<connect gate="G$1" pin="CANTX" pad="CANTX"/>
<connect gate="G$1" pin="DAC0" pad="DAC0"/>
<connect gate="G$1" pin="DAC1" pad="DAC1"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND-BOTTOM1" pad="GND-BASE1"/>
<connect gate="G$1" pin="GND-BOTTOM2" pad="GND-BASE2"/>
<connect gate="G$1" pin="GND-POWER1" pad="GND-POWER1"/>
<connect gate="G$1" pin="GND-POWER2" pad="GND-POWER2"/>
<connect gate="G$1" pin="GND-SPI" pad="GND-SPI"/>
<connect gate="G$1" pin="IOREF" pad="I0REF"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NOTUSED" pad="NOTUSED"/>
<connect gate="G$1" pin="PWM10" pad="PWM10"/>
<connect gate="G$1" pin="PWM11" pad="PWM11"/>
<connect gate="G$1" pin="PWM12" pad="PWM12"/>
<connect gate="G$1" pin="PWM13" pad="PWM13"/>
<connect gate="G$1" pin="PWM2" pad="PWM2"/>
<connect gate="G$1" pin="PWM3" pad="PWM3"/>
<connect gate="G$1" pin="PWM4" pad="PWM4"/>
<connect gate="G$1" pin="PWM5" pad="PWM5"/>
<connect gate="G$1" pin="PWM6" pad="PWM6"/>
<connect gate="G$1" pin="PWM7" pad="PWM7"/>
<connect gate="G$1" pin="PWM8" pad="PWM8"/>
<connect gate="G$1" pin="PWM9" pad="PWM9"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="RESET-SPI" pad="RESET-SPI"/>
<connect gate="G$1" pin="RX0" pad="RX0"/>
<connect gate="G$1" pin="RX1" pad="RX1"/>
<connect gate="G$1" pin="RX2" pad="RX2"/>
<connect gate="G$1" pin="RX3" pad="RX3"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SCL1" pad="SCL1"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="SDA1" pad="SDA1"/>
<connect gate="G$1" pin="TX0" pad="TX0"/>
<connect gate="G$1" pin="TX1" pad="TX1"/>
<connect gate="G$1" pin="TX2" pad="TX2"/>
<connect gate="G$1" pin="TX3" pad="TX3"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Iduino-PC104">
<description>&lt;b&gt;PC/104 Standard/Plus connector&lt;/b&gt;&lt;p&gt;
Sources :
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;pc104standard.pdf&lt;/b&gt;&lt;p&gt;
PC/104 Specification, Version 2.3, June 1996&lt;p&gt;
Copyright 1992-96, PC/104 Consortium
&lt;li&gt;&lt;b&gt;PC104plus.pdf&lt;/b&gt;&lt;p&gt;
PC/104-Plus Specification, Version 1.1, June 1997&lt;p&gt;
Copyright 1992-98, PC/104 Consortium
&lt;/ul&gt;
include &lt;b&gt;Commcon Connector PC/104&lt;/b&gt;, con-commcon.lbr&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="PC104-STANDARD">
<description>&lt;b&gt;PC/104 Standard 16 bit&lt;/b&gt;</description>
<wire x1="0" y1="0" x2="90.2" y2="0" width="0" layer="20"/>
<wire x1="90.2" y1="0" x2="90.2" y2="95.9" width="0" layer="20"/>
<wire x1="90.2" y1="95.9" x2="0" y2="95.9" width="0" layer="20"/>
<wire x1="0" y1="95.9" x2="0" y2="0" width="0" layer="20"/>
<wire x1="5.3" y1="13.8" x2="86.27" y2="13.8" width="0.2032" layer="21"/>
<wire x1="86.27" y1="13.8" x2="86.27" y2="9.1" width="0.2032" layer="21"/>
<wire x1="86.27" y1="9.1" x2="5.3" y2="9.1" width="0.2032" layer="21"/>
<wire x1="5.3" y1="9.1" x2="5.3" y2="13.8" width="0.2032" layer="21"/>
<pad name="RX_GPS" x="6.35" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX_GPS" x="8.89" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDA_IMU" x="11.43" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCL_IMU" x="13.97" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="XBEE_TX" x="16.51" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="XBEE_RX" x="19.05" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN51" x="21.59" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN50" x="24.13" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MOSI" x="26.67" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MISO" x="29.21" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCK" x="31.75" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RST_SPI" x="34.29" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="PIN22" x="36.83" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A4" x="39.37" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A3" x="41.91" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A2" x="44.45" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A1" x="46.99" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A0" x="49.53" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D1" x="52.07" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D2" x="54.61" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D3" x="57.15" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D4" x="59.69" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D5" x="62.23" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="P_D6" x="64.77" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="5V" x="67.31" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="GND" x="69.85" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A8" x="6.35" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="3.3V" x="72.39" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A9" x="8.89" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG3" x="74.93" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A10" x="11.43" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG2" x="77.47" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A7" x="13.97" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="MAG1" x="80.01" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SPARE" x="80.01" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDCS_CAM" x="82.55" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SDA_CAM" x="82.55" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CSN_CAM" x="85.09" y="12.7" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="SCL_CAM" x="85.09" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<text x="5.715" y="14.605" size="1.27" layer="25">J1</text>
<pad name="A6" x="16.51" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A5" x="19.05" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="23" x="21.59" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="25" x="24.13" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="27" x="26.67" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="52" x="29.21" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="53" x="31.75" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="A11" x="34.29" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="DAC0" x="36.83" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="DAC1" x="39.37" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RX3" x="41.91" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX3" x="44.45" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RX0" x="46.99" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="TX0" x="49.53" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="5" x="52.07" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="6" x="54.61" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="7" x="57.15" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="8" x="59.69" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="11" x="62.23" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="12" x="64.77" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="13" x="67.31" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="RESET" x="69.85" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="IOREF" x="72.39" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CANRX" x="74.93" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<pad name="CANTX" x="77.47" y="10.16" drill="0.8998" diameter="1.397" shape="octagon"/>
<hole x="8.89" y="90.805" drill="3.5052"/>
<hole x="82.55" y="90.805" drill="3.5052"/>
<hole x="85.09" y="5.08" drill="3.5052"/>
<hole x="5.08" y="5.08" drill="3.5052"/>
</package>
</packages>
<symbols>
<symbol name="PC104-J1">
<wire x1="-12.7" y1="40.64" x2="12.7" y2="40.64" width="0.254" layer="94"/>
<wire x1="12.7" y1="40.64" x2="12.7" y2="-43.18" width="0.254" layer="94"/>
<wire x1="12.7" y1="-43.18" x2="-12.7" y2="-43.18" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-43.18" x2="-12.7" y2="40.64" width="0.254" layer="94"/>
<text x="-12.7" y="41.656" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-45.72" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SDA1" x="-15.24" y="-38.1" length="short" direction="pas"/>
<pin name="NC" x="-15.24" y="-35.56" length="short" direction="pas"/>
<pin name="SCL1" x="-15.24" y="-40.64" length="short" direction="pas"/>
<pin name="TX1" x="15.24" y="38.1" length="short" direction="pas" rot="R180"/>
<pin name="RX1" x="15.24" y="35.56" length="short" direction="pas" rot="R180"/>
<pin name="SDA" x="15.24" y="33.02" length="short" direction="pas" rot="R180"/>
<pin name="SCL" x="15.24" y="30.48" length="short" direction="pas" rot="R180"/>
<pin name="RX2" x="15.24" y="27.94" length="short" direction="pas" rot="R180"/>
<pin name="TX2" x="15.24" y="25.4" length="short" direction="pas" rot="R180"/>
<pin name="SAL_SEL" x="15.24" y="22.86" length="short" direction="pas" rot="R180"/>
<pin name="INT_CAM" x="15.24" y="20.32" length="short" direction="pas" rot="R180"/>
<pin name="MOSI" x="15.24" y="17.78" length="short" direction="pas" rot="R180"/>
<pin name="MISO" x="15.24" y="15.24" length="short" direction="pas" rot="R180"/>
<pin name="SCK" x="15.24" y="12.7" length="short" direction="pas" rot="R180"/>
<pin name="RSTN" x="15.24" y="10.16" length="short" direction="pas" rot="R180"/>
<pin name="PWR_D" x="15.24" y="7.62" length="short" direction="pas" rot="R180"/>
<pin name="SS5" x="15.24" y="5.08" length="short" direction="pas" rot="R180"/>
<pin name="SS4" x="15.24" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="SS3" x="15.24" y="0" length="short" direction="pas" rot="R180"/>
<pin name="SS2" x="15.24" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="SS1" x="15.24" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="D1" x="15.24" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="D2" x="15.24" y="-10.16" length="short" direction="pas" rot="R180"/>
<pin name="5V" x="15.24" y="-22.86" length="short" direction="pas" rot="R180"/>
<pin name="D6" x="15.24" y="-20.32" length="short" direction="pas" rot="R180"/>
<pin name="D5" x="15.24" y="-17.78" length="short" direction="pas" rot="R180"/>
<pin name="D4" x="15.24" y="-15.24" length="short" direction="pas" rot="R180"/>
<pin name="D3" x="15.24" y="-12.7" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="15.24" y="-25.4" length="short" direction="pas" rot="R180"/>
<pin name="3.3V" x="15.24" y="-27.94" length="short" direction="pas" rot="R180"/>
<pin name="PWM2" x="15.24" y="-30.48" length="short" direction="pas" rot="R180"/>
<pin name="PWM3" x="15.24" y="-33.02" length="short" direction="pas" rot="R180"/>
<pin name="PWM4" x="15.24" y="-35.56" length="short" direction="pas" rot="R180"/>
<pin name="SD_CS" x="15.24" y="-38.1" length="short" direction="pas" rot="R180"/>
<pin name="CSN" x="15.24" y="-40.64" length="short" direction="pas" rot="R180"/>
<pin name="P_A4" x="-17.78" y="38.1" length="middle"/>
<pin name="P_A5" x="-17.78" y="35.56" length="middle"/>
<pin name="P_A6" x="-17.78" y="33.02" length="middle"/>
<pin name="P_A3" x="-17.78" y="30.48" length="middle"/>
<pin name="P_A2" x="-17.78" y="27.94" length="middle"/>
<pin name="P_A1" x="-17.78" y="25.4" length="middle"/>
<pin name="23" x="-17.78" y="22.86" length="middle"/>
<pin name="25" x="-17.78" y="20.32" length="middle"/>
<pin name="27" x="-17.78" y="17.78" length="middle"/>
<pin name="52" x="-17.78" y="15.24" length="middle"/>
<pin name="53" x="-17.78" y="12.7" length="middle"/>
<pin name="A11" x="-17.78" y="10.16" length="middle"/>
<pin name="DAC0" x="-17.78" y="7.62" length="middle"/>
<pin name="DAC1" x="-17.78" y="5.08" length="middle"/>
<pin name="RX3" x="-17.78" y="2.54" length="middle"/>
<pin name="TX3" x="-17.78" y="0" length="middle"/>
<pin name="RX0" x="-17.78" y="-2.54" length="middle"/>
<pin name="TX0" x="-17.78" y="-5.08" length="middle"/>
<pin name="5" x="-17.78" y="-7.62" length="middle"/>
<pin name="6" x="-17.78" y="-10.16" length="middle"/>
<pin name="7" x="-17.78" y="-12.7" length="middle"/>
<pin name="8" x="-17.78" y="-15.24" length="middle"/>
<pin name="11" x="-17.78" y="-17.78" length="middle"/>
<pin name="12" x="-17.78" y="-20.32" length="middle"/>
<pin name="13" x="-17.78" y="-22.86" length="middle"/>
<pin name="RESET" x="-17.78" y="-25.4" length="middle"/>
<pin name="IOREF" x="-17.78" y="-27.94" length="middle"/>
<pin name="CANRX" x="-17.78" y="-30.48" length="middle"/>
<pin name="CANTX" x="-17.78" y="-33.02" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IDUINO_PC104" prefix="X">
<description>&lt;b&gt;PC/104 Standard 16 bit&lt;/b&gt;</description>
<gates>
<gate name="-J1" symbol="PC104-J1" x="0" y="0"/>
</gates>
<devices>
<device name="1" package="PC104-STANDARD">
<connects>
<connect gate="-J1" pin="11" pad="11"/>
<connect gate="-J1" pin="12" pad="12"/>
<connect gate="-J1" pin="13" pad="13"/>
<connect gate="-J1" pin="23" pad="23"/>
<connect gate="-J1" pin="25" pad="25"/>
<connect gate="-J1" pin="27" pad="27"/>
<connect gate="-J1" pin="3.3V" pad="3.3V"/>
<connect gate="-J1" pin="5" pad="5"/>
<connect gate="-J1" pin="52" pad="52"/>
<connect gate="-J1" pin="53" pad="53"/>
<connect gate="-J1" pin="5V" pad="5V"/>
<connect gate="-J1" pin="6" pad="6"/>
<connect gate="-J1" pin="7" pad="7"/>
<connect gate="-J1" pin="8" pad="8"/>
<connect gate="-J1" pin="A11" pad="A11"/>
<connect gate="-J1" pin="CANRX" pad="CANRX"/>
<connect gate="-J1" pin="CANTX" pad="CANTX"/>
<connect gate="-J1" pin="CSN" pad="CSN_CAM"/>
<connect gate="-J1" pin="D1" pad="P_D1"/>
<connect gate="-J1" pin="D2" pad="P_D2"/>
<connect gate="-J1" pin="D3" pad="P_D3"/>
<connect gate="-J1" pin="D4" pad="P_D4"/>
<connect gate="-J1" pin="D5" pad="P_D5"/>
<connect gate="-J1" pin="D6" pad="P_D6"/>
<connect gate="-J1" pin="DAC0" pad="DAC0"/>
<connect gate="-J1" pin="DAC1" pad="DAC1"/>
<connect gate="-J1" pin="GND" pad="GND"/>
<connect gate="-J1" pin="INT_CAM" pad="PIN50"/>
<connect gate="-J1" pin="IOREF" pad="IOREF"/>
<connect gate="-J1" pin="MISO" pad="MISO"/>
<connect gate="-J1" pin="MOSI" pad="MOSI"/>
<connect gate="-J1" pin="NC" pad="SPARE"/>
<connect gate="-J1" pin="PWM2" pad="MAG3"/>
<connect gate="-J1" pin="PWM3" pad="MAG2"/>
<connect gate="-J1" pin="PWM4" pad="MAG1"/>
<connect gate="-J1" pin="PWR_D" pad="PIN22"/>
<connect gate="-J1" pin="P_A1" pad="A5"/>
<connect gate="-J1" pin="P_A2" pad="A6"/>
<connect gate="-J1" pin="P_A3" pad="A7"/>
<connect gate="-J1" pin="P_A4" pad="A8"/>
<connect gate="-J1" pin="P_A5" pad="A9"/>
<connect gate="-J1" pin="P_A6" pad="A10"/>
<connect gate="-J1" pin="RESET" pad="RESET"/>
<connect gate="-J1" pin="RSTN" pad="RST_SPI"/>
<connect gate="-J1" pin="RX0" pad="RX0"/>
<connect gate="-J1" pin="RX1" pad="TX_GPS"/>
<connect gate="-J1" pin="RX2" pad="XBEE_TX"/>
<connect gate="-J1" pin="RX3" pad="RX3"/>
<connect gate="-J1" pin="SAL_SEL" pad="PIN51"/>
<connect gate="-J1" pin="SCK" pad="SCK"/>
<connect gate="-J1" pin="SCL" pad="SCL_IMU"/>
<connect gate="-J1" pin="SCL1" pad="SCL_CAM"/>
<connect gate="-J1" pin="SDA" pad="SDA_IMU"/>
<connect gate="-J1" pin="SDA1" pad="SDA_CAM"/>
<connect gate="-J1" pin="SD_CS" pad="SDCS_CAM"/>
<connect gate="-J1" pin="SS1" pad="A0"/>
<connect gate="-J1" pin="SS2" pad="A1"/>
<connect gate="-J1" pin="SS3" pad="A2"/>
<connect gate="-J1" pin="SS4" pad="A3"/>
<connect gate="-J1" pin="SS5" pad="A4"/>
<connect gate="-J1" pin="TX0" pad="TX0"/>
<connect gate="-J1" pin="TX1" pad="RX_GPS"/>
<connect gate="-J1" pin="TX2" pad="XBEE_RX"/>
<connect gate="-J1" pin="TX3" pad="TX3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="DUE1" library="Iduino-Due" deviceset="IDUINO-DUE" device=""/>
<part name="X1" library="Iduino-PC104" deviceset="IDUINO_PC104" device="1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="DUE1" gate="G$1" x="121.92" y="43.18"/>
<instance part="X1" gate="-J1" x="30.48" y="45.72"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="GND-POWER2"/>
<wire x1="68.58" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<label x="68.58" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="GND"/>
<wire x1="45.72" y1="20.32" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<label x="48.26" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="VIN"/>
<wire x1="91.44" y1="71.12" x2="78.74" y2="71.12" width="0.1524" layer="91"/>
<label x="78.74" y="71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="5V"/>
<wire x1="45.72" y1="22.86" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<label x="55.88" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="SDA"/>
<wire x1="116.84" y1="-12.7" x2="116.84" y2="-27.94" width="0.1524" layer="91"/>
<label x="116.84" y="-27.94" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SDA"/>
<wire x1="45.72" y1="78.74" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<label x="50.8" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="RX1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RX1"/>
<wire x1="114.3" y1="-12.7" x2="114.3" y2="-20.32" width="0.1524" layer="91"/>
<label x="114.3" y="-20.32" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RX1"/>
<wire x1="45.72" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<label x="53.34" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="TX1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="TX1"/>
<wire x1="111.76" y1="-12.7" x2="111.76" y2="-27.94" width="0.1524" layer="91"/>
<label x="111.76" y="-27.94" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="TX1"/>
<wire x1="45.72" y1="83.82" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
<label x="50.8" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM2"/>
<wire x1="91.44" y1="2.54" x2="81.28" y2="2.54" width="0.1524" layer="91"/>
<label x="81.28" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="PWM2"/>
<wire x1="45.72" y1="15.24" x2="50.8" y2="15.24" width="0.1524" layer="91"/>
<label x="50.8" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM3"/>
<wire x1="91.44" y1="5.08" x2="81.28" y2="5.08" width="0.1524" layer="91"/>
<label x="81.28" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="PWM3"/>
<wire x1="45.72" y1="12.7" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<label x="55.88" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM4"/>
<wire x1="91.44" y1="7.62" x2="81.28" y2="7.62" width="0.1524" layer="91"/>
<label x="81.28" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="PWM4"/>
<wire x1="45.72" y1="10.16" x2="50.8" y2="10.16" width="0.1524" layer="91"/>
<label x="50.8" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM9"/>
<wire x1="91.44" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<label x="81.28" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SD_CS"/>
<wire x1="45.72" y1="7.62" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<label x="55.88" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CSN" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM10"/>
<wire x1="91.44" y1="25.4" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<label x="81.28" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="CSN"/>
<wire x1="45.72" y1="5.08" x2="50.8" y2="5.08" width="0.1524" layer="91"/>
<label x="50.8" y="5.08" size="1.778" layer="95" xref="yes"/>
<label x="50.8" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="SCL1"/>
<wire x1="91.44" y1="43.18" x2="81.28" y2="43.18" width="0.1524" layer="91"/>
<label x="81.28" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SCL1"/>
<wire x1="15.24" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="91"/>
<label x="7.62" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A0"/>
<wire x1="91.44" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<label x="83.82" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SS1"/>
<wire x1="45.72" y1="40.64" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<label x="60.96" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A1"/>
<wire x1="91.44" y1="63.5" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<label x="88.9" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SS2"/>
<wire x1="45.72" y1="43.18" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<label x="50.8" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A2"/>
<wire x1="91.44" y1="60.96" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
<label x="83.82" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SS3"/>
<wire x1="45.72" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<label x="60.96" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A3"/>
<wire x1="91.44" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<label x="88.9" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SS4"/>
<wire x1="45.72" y1="48.26" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
<label x="50.8" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A4"/>
<wire x1="91.44" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<label x="83.82" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SS5"/>
<wire x1="45.72" y1="50.8" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<label x="60.96" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="TX2" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="TX2"/>
<wire x1="106.68" y1="-12.7" x2="106.68" y2="-27.94" width="0.1524" layer="91"/>
<label x="106.68" y="-27.94" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="TX2"/>
<wire x1="45.72" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<label x="53.34" y="71.12" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="SCL"/>
<wire x1="119.38" y1="-12.7" x2="119.38" y2="-20.32" width="0.1524" layer="91"/>
<label x="119.38" y="-20.32" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SCL"/>
<wire x1="45.72" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<label x="53.34" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="RX2" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RX2"/>
<wire x1="109.22" y1="-12.7" x2="109.22" y2="-20.32" width="0.1524" layer="91"/>
<label x="109.22" y="-20.32" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RX2"/>
<wire x1="45.72" y1="73.66" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
<label x="48.26" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="SDA1"/>
<wire x1="91.44" y1="40.64" x2="81.28" y2="40.64" width="0.1524" layer="91"/>
<label x="81.28" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SDA1"/>
<wire x1="15.24" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="91"/>
<label x="0" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="INTERRUPT" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="INT_CAM"/>
<wire x1="45.72" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<label x="53.34" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="50"/>
<wire x1="152.4" y1="12.7" x2="165.1" y2="12.7" width="0.1524" layer="91"/>
<label x="165.1" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SAL_SEL" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="SAL_SEL"/>
<wire x1="45.72" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<label x="48.26" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="51"/>
<wire x1="152.4" y1="10.16" x2="157.48" y2="10.16" width="0.1524" layer="91"/>
<label x="157.48" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="MISO"/>
<wire x1="116.84" y1="99.06" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
<label x="116.84" y="104.14" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="MISO"/>
<wire x1="45.72" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<label x="53.34" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="SCK"/>
<wire x1="119.38" y1="99.06" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
<label x="119.38" y="116.84" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="SCK"/>
<wire x1="45.72" y1="58.42" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<label x="48.26" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="RSTN" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RESET-SPI"/>
<wire x1="121.92" y1="99.06" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
<label x="121.92" y="109.22" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RSTN"/>
<wire x1="45.72" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<label x="53.34" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="MOSI"/>
<wire x1="127" y1="99.06" x2="127" y2="111.76" width="0.1524" layer="91"/>
<label x="127" y="111.76" size="1.778" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="MOSI"/>
<wire x1="45.72" y1="63.5" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<label x="48.26" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_D" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="22"/>
<wire x1="152.4" y1="83.82" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<label x="162.56" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="PWR_D"/>
<wire x1="45.72" y1="53.34" x2="48.26" y2="53.34" width="0.1524" layer="91"/>
<label x="48.26" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A5"/>
<wire x1="91.44" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<label x="88.9" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A1"/>
<wire x1="12.7" y1="71.12" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<label x="-10.16" y="71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A6"/>
<wire x1="91.44" y1="50.8" x2="83.82" y2="50.8" width="0.1524" layer="91"/>
<label x="83.82" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A2"/>
<wire x1="12.7" y1="73.66" x2="5.08" y2="73.66" width="0.1524" layer="91"/>
<label x="5.08" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P3" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A7"/>
<wire x1="91.44" y1="48.26" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<label x="88.9" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A3"/>
<wire x1="12.7" y1="76.2" x2="-7.62" y2="76.2" width="0.1524" layer="91"/>
<label x="-7.62" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P4" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A8"/>
<wire x1="142.24" y1="-12.7" x2="142.24" y2="-22.86" width="0.1524" layer="91"/>
<label x="142.24" y="-22.86" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A4"/>
<wire x1="12.7" y1="83.82" x2="5.08" y2="83.82" width="0.1524" layer="91"/>
<label x="5.08" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P5" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A9"/>
<wire x1="139.7" y1="-12.7" x2="139.7" y2="-15.24" width="0.1524" layer="91"/>
<label x="139.7" y="-15.24" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A5"/>
<wire x1="12.7" y1="81.28" x2="-10.16" y2="81.28" width="0.1524" layer="91"/>
<label x="-10.16" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P6" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A10"/>
<wire x1="137.16" y1="-12.7" x2="137.16" y2="-22.86" width="0.1524" layer="91"/>
<label x="137.16" y="-22.86" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="P_A6"/>
<wire x1="12.7" y1="78.74" x2="5.08" y2="78.74" width="0.1524" layer="91"/>
<label x="5.08" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D1"/>
<wire x1="45.72" y1="38.1" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
<label x="50.8" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="30"/>
<wire x1="152.4" y1="63.5" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<label x="162.56" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D2"/>
<wire x1="45.72" y1="35.56" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<label x="55.88" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="28"/>
<wire x1="152.4" y1="68.58" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<label x="162.56" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D3"/>
<wire x1="45.72" y1="33.02" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
<label x="50.8" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="26"/>
<wire x1="152.4" y1="73.66" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<label x="160.02" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D4"/>
<wire x1="45.72" y1="30.48" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<label x="55.88" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="24"/>
<wire x1="152.4" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<label x="160.02" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D5"/>
<wire x1="45.72" y1="27.94" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<label x="50.8" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="31"/>
<wire x1="152.4" y1="60.96" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<label x="154.94" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="D6"/>
<wire x1="45.72" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<label x="53.34" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="29"/>
<wire x1="152.4" y1="66.04" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="157.48" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="23" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="23"/>
<wire x1="152.4" y1="81.28" x2="157.48" y2="81.28" width="0.1524" layer="91"/>
<label x="157.48" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="23"/>
<wire x1="12.7" y1="68.58" x2="5.08" y2="68.58" width="0.1524" layer="91"/>
<label x="5.08" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="25" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="25"/>
<wire x1="152.4" y1="76.2" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="157.48" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="25"/>
<wire x1="12.7" y1="66.04" x2="5.08" y2="66.04" width="0.1524" layer="91"/>
<label x="5.08" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="27" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="27"/>
<wire x1="152.4" y1="71.12" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="157.48" y="71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="27"/>
<wire x1="12.7" y1="63.5" x2="5.08" y2="63.5" width="0.1524" layer="91"/>
<label x="5.08" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="A11"/>
<wire x1="134.62" y1="-12.7" x2="134.62" y2="-17.78" width="0.1524" layer="91"/>
<label x="134.62" y="-17.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="A11"/>
<wire x1="12.7" y1="55.88" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
<label x="5.08" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAC0" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="DAC0"/>
<wire x1="132.08" y1="-12.7" x2="132.08" y2="-17.78" width="0.1524" layer="91"/>
<label x="132.08" y="-17.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="DAC0"/>
<wire x1="12.7" y1="53.34" x2="5.08" y2="53.34" width="0.1524" layer="91"/>
<label x="5.08" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="52" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="52"/>
<wire x1="152.4" y1="7.62" x2="157.48" y2="7.62" width="0.1524" layer="91"/>
<label x="157.48" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="52"/>
<wire x1="12.7" y1="60.96" x2="5.08" y2="60.96" width="0.1524" layer="91"/>
<label x="5.08" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="53" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="53"/>
<wire x1="152.4" y1="5.08" x2="157.48" y2="5.08" width="0.1524" layer="91"/>
<label x="157.48" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="53"/>
<wire x1="12.7" y1="58.42" x2="5.08" y2="58.42" width="0.1524" layer="91"/>
<label x="5.08" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RX3" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RX3"/>
<wire x1="104.14" y1="-12.7" x2="104.14" y2="-17.78" width="0.1524" layer="91"/>
<label x="104.14" y="-17.78" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RX3"/>
<wire x1="12.7" y1="48.26" x2="5.08" y2="48.26" width="0.1524" layer="91"/>
<label x="5.08" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX3" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="TX3"/>
<wire x1="101.6" y1="-12.7" x2="101.6" y2="-20.32" width="0.1524" layer="91"/>
<label x="101.6" y="-20.32" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="TX3"/>
<wire x1="12.7" y1="45.72" x2="5.08" y2="45.72" width="0.1524" layer="91"/>
<label x="5.08" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RX0" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RX0"/>
<wire x1="91.44" y1="-2.54" x2="81.28" y2="-2.54" width="0.1524" layer="91"/>
<label x="81.28" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RX0"/>
<wire x1="12.7" y1="43.18" x2="5.08" y2="43.18" width="0.1524" layer="91"/>
<label x="5.08" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TX0" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="TX0"/>
<wire x1="91.44" y1="0" x2="81.28" y2="0" width="0.1524" layer="91"/>
<label x="81.28" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="TX0"/>
<wire x1="12.7" y1="40.64" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
<label x="5.08" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IOREF" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="IOREF"/>
<wire x1="91.44" y1="86.36" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
<label x="83.82" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="IOREF"/>
<wire x1="12.7" y1="17.78" x2="5.08" y2="17.78" width="0.1524" layer="91"/>
<label x="5.08" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="RESET"/>
<wire x1="91.44" y1="83.82" x2="83.82" y2="83.82" width="0.1524" layer="91"/>
<label x="83.82" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="RESET"/>
<wire x1="12.7" y1="20.32" x2="5.08" y2="20.32" width="0.1524" layer="91"/>
<label x="5.08" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="5" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM5"/>
<wire x1="91.44" y1="10.16" x2="81.28" y2="10.16" width="0.1524" layer="91"/>
<label x="81.28" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="5"/>
<wire x1="12.7" y1="38.1" x2="5.08" y2="38.1" width="0.1524" layer="91"/>
<label x="5.08" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="6" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM6"/>
<wire x1="91.44" y1="12.7" x2="83.82" y2="12.7" width="0.1524" layer="91"/>
<label x="83.82" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="6"/>
<wire x1="12.7" y1="35.56" x2="5.08" y2="35.56" width="0.1524" layer="91"/>
<label x="5.08" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="7" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM7"/>
<wire x1="91.44" y1="15.24" x2="83.82" y2="15.24" width="0.1524" layer="91"/>
<label x="83.82" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="7"/>
<wire x1="12.7" y1="33.02" x2="5.08" y2="33.02" width="0.1524" layer="91"/>
<label x="5.08" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="8" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM8"/>
<wire x1="91.44" y1="20.32" x2="83.82" y2="20.32" width="0.1524" layer="91"/>
<label x="83.82" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="8"/>
<wire x1="12.7" y1="30.48" x2="5.08" y2="30.48" width="0.1524" layer="91"/>
<label x="5.08" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="11" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM11"/>
<wire x1="91.44" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
<label x="83.82" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="11"/>
<wire x1="12.7" y1="27.94" x2="5.08" y2="27.94" width="0.1524" layer="91"/>
<label x="5.08" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="12" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM12"/>
<wire x1="91.44" y1="30.48" x2="83.82" y2="30.48" width="0.1524" layer="91"/>
<label x="83.82" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="12"/>
<wire x1="12.7" y1="25.4" x2="5.08" y2="25.4" width="0.1524" layer="91"/>
<label x="5.08" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="13" class="0">
<segment>
<pinref part="X1" gate="-J1" pin="13"/>
<wire x1="12.7" y1="22.86" x2="10.16" y2="22.86" width="0.1524" layer="91"/>
<label x="10.16" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="DUE1" gate="G$1" pin="PWM13"/>
<wire x1="91.44" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<label x="83.82" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="DAC1"/>
<wire x1="129.54" y1="-12.7" x2="129.54" y2="-20.32" width="0.1524" layer="91"/>
<label x="129.54" y="-20.32" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="DAC1"/>
<wire x1="12.7" y1="50.8" x2="5.08" y2="50.8" width="0.1524" layer="91"/>
<label x="5.08" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CANRX" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="CANRX"/>
<wire x1="127" y1="-12.7" x2="127" y2="-22.86" width="0.1524" layer="91"/>
<label x="127" y="-22.86" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="CANRX"/>
<wire x1="12.7" y1="15.24" x2="7.62" y2="15.24" width="0.1524" layer="91"/>
<label x="7.62" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CANTX" class="0">
<segment>
<pinref part="DUE1" gate="G$1" pin="CANTX"/>
<wire x1="124.46" y1="-12.7" x2="124.46" y2="-27.94" width="0.1524" layer="91"/>
<label x="124.46" y="-27.94" size="1.778" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="X1" gate="-J1" pin="CANTX"/>
<wire x1="12.7" y1="12.7" x2="7.62" y2="12.7" width="0.1524" layer="91"/>
<label x="7.62" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
