To add this to Eagle:

1. Copy it over the folders to the /lbr directory inside Eagle
2. In the Eagle Control Panel, expand the folder, right click on each .lbr file and click “Use”.