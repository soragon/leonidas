// Include whatever you are testing here, _before_ including gtest.
#include "buffer.h"

// Include the testing suite.
#include "gtest/gtest.h"

class BufferTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if
  // the body is empty.

  BufferTest() {
    // The constructor; any set-up operations can go here.
  }

  virtual ~BufferTest() {
    // The destructor; any clean-up operations can go here.
  }

  virtual void SetUp() {
    // This function is called immediately before each test (before
    // the constructor). Rather use this over the constructor.
  }

  virtual void TearDown() {
    // This function is called immediately after each test (before
    // the destructor). Rather use this over the destructor.
  }

  // You can declare some member variables down here which will
  // be available to all tests.
};

// All test methods should be of this format. Make sure the name states
// the inputs and expected output that the test is trying to achieve.
TEST_F(BufferTest, TestBufferConstruction) {
  Buffer<100, char> buffer;

  EXPECT_EQ(0, buffer.size());
  EXPECT_EQ(100, buffer.max_size());
  EXPECT_TRUE(buffer.empty());
}

TEST_F(BufferTest, TestBasicInsertAndRemoval) {
  Buffer<100, char> buffer;

  EXPECT_EQ(0, buffer.size());
  buffer.Add('a');
  buffer.Add('b');
  buffer.Add('c');
  EXPECT_EQ(3, buffer.size());

  EXPECT_EQ('a', buffer.Peek());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ(2, buffer.size());

  EXPECT_EQ('b', buffer.Peek());
  EXPECT_EQ('b', buffer.Next());
  EXPECT_EQ(1, buffer.size());

  EXPECT_EQ('c', buffer.Peek());
  EXPECT_EQ('c', buffer.Next());
  EXPECT_EQ(0, buffer.size());
}

TEST_F(BufferTest, TestAddAllAddsItemsCorrectly) {
  Buffer<100, char> buffer;

  char data[10];
  for (unsigned int i = 0; i < sizeof(data); i++) {
    data[i] = 'a';
  }

  // Add all of the data.
  buffer.AddAll(data, sizeof(data));

  for (unsigned int i = 0; i < sizeof(data); i++) {
    EXPECT_EQ(sizeof(data) - i, buffer.size());
    EXPECT_EQ('a', buffer.Peek());
    EXPECT_EQ('a', buffer.Next());
    EXPECT_EQ(sizeof(data) - i - 1, buffer.size());
  }
}

TEST_F(BufferTest, TestResetActuallyResetsData) {
  Buffer<100, char> buffer;

  buffer.Reset('x');

  for (unsigned int i = 0; i < buffer.max_size(); i++) {
    EXPECT_EQ('x', buffer.data()[i]);
  }
}

TEST_F(BufferTest, TestSimpleLoopAround) {
  Buffer<10, char> buffer;

  // Fill up and remove some data.
  const char* data = "aaaaa";
  buffer.AddAll(data, strlen(data));
  for (unsigned int i = 0; i < strlen(data); i++) {
    EXPECT_EQ(data[i], buffer.Next());
  }

  EXPECT_EQ(0, buffer.size());
  EXPECT_TRUE(buffer.empty());

  // Buffer should now look like this:
  //   aaaaaXXXXX, size=0
  //        ↑
  //       S/E
  //
  // Try to insert 8 more things, and make sure they can be pulled out.
  const char* data2 = "xxxxxxxx";
  buffer.AddAll(data2, strlen(data2));
  for (unsigned int i = 0; i < strlen(data2); i++) {
    EXPECT_EQ(data2[i], buffer.Next());
  }

  EXPECT_EQ(0, buffer.size());
}

TEST_F(BufferTest, TestBufferOverflow) {
  Buffer<10, char> buffer;
  const char* data = "aaaaa";
  const char* data2 = "xxxxxxxx";

  // If we add both of these, we should be in this state:
  //    aaaaaxxxxx, size=10
  //    ↑
  //   S/E
  //
  // The additional data should be dropped.
  buffer.AddAll(data, strlen(data));
  buffer.AddAll(data2, strlen(data2));

  EXPECT_EQ(10, buffer.size());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ('a', buffer.Next());
  EXPECT_EQ('x', buffer.Next());
  EXPECT_EQ('x', buffer.Next());
  EXPECT_EQ('x', buffer.Next());
  EXPECT_EQ('x', buffer.Next());
  EXPECT_EQ('x', buffer.Next());
  EXPECT_EQ(0, buffer.size());
}

TEST_F(BufferTest, TestFindReturnsTrueWhenNoOverflow) {
  Buffer<10, char> buffer;
  const char* data = "aaaaaa8";

  buffer.AddAll(data, strlen(data));
  EXPECT_TRUE(buffer.Find('a'));
  EXPECT_TRUE(buffer.Find('8'));
  EXPECT_FALSE(buffer.Find('x'));
}

TEST_F(BufferTest, TestFindReturnsTrueWhenOverflow) {
  Buffer<10, char> buffer;

  // Fill up and empty the buffer.
  const char* data = "aaaaa";
  buffer.AddAll(data, strlen(data));
  while (!buffer.empty()) {
    buffer.Next();
  }

  // Fill up the buffer with extra data. We should be in this state:
  //    bcdaaxxxxx, size=8
  //       ↑ ↑
  //       E S
  const char* data2 = "xxxxxbcd";
  buffer.AddAll(data2, strlen(data2));

  EXPECT_TRUE(buffer.Find('x'));
  EXPECT_TRUE(buffer.Find('b'));
  EXPECT_TRUE(buffer.Find('c'));
  EXPECT_TRUE(buffer.Find('d'));
  EXPECT_FALSE(buffer.Find('a'));
}
