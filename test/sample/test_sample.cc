// Include whatever you are testing here, _before_ including gtest.

// Include the testing suite.
#include "gtest/gtest.h"

class SampleTest : public ::testing::Test {
 protected:
  // You can remove any or all of the following functions if
  // the body is empty.

  SampleTest() {
    // The constructor; any set-up operations can go here.
  }

  virtual ~SampleTest() {
    // The destructor; any clean-up operations can go here.
  }

  virtual void SetUp() {
    // This function is called immediately before each test (before
    // the constructor). Rather use this over the constructor.
  }

  virtual void TearDown() {
    // This function is called immediately after each test (before
    // the destructor). Rather use this over the destructor.
  }

  // You can declare some member variables down here which will
  // be available to all tests.
};

// All test methods should be of this format. Make sure the name states
// the inputs and expected output that the test is trying to achieve.
TEST_F(SampleTest, ObjectXDoesYWhenInputIsZ) {
  // All 'tests' should be EXPECT_*(). See https://code.google.com/p/googletest/wiki/Documentation
  // for more information.
  EXPECT_EQ(1, 1);
}
