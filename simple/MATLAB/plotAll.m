close all
clear
clc

run importData.m

linewidth = 1.5;

plots = figure('units', 'normalized','outerposition', [1/2 0 1/2 1], 'name', 'Plots', 'Color',[1 1 1]);



%
subplot(3,2,1);
hold on;
plot(millis, ax, 'LineWidth', linewidth);
plot(millis, ay, 'LineWidth', linewidth);
plot(millis, az, 'LineWidth', linewidth);
title('Accelerometer Data');
xlabel('Time (s)');
ylabel('m/s/s');

%%

subplot(3,2,2);
hold on;
plot([one.t], [one.input.d], 'LineWidth', linewidth);
% plot([two.t], [two.input.d], 'LineWidth', linewidth);
plot([three.t], [three.input.d], 'LineWidth', linewidth);
plot([four.t], [four.input.d], 'LineWidth', linewidth);
plot([five.t], [five.input.d], 'LineWidth', linewidth);
title('Input distance, d');
xlabel('Time (s)');
ylabel('Joystick Position');
plot(one.t(1:end), 0*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);

legend('Trial 1', 'Trial 2', 'Trial 3', 'Trial 4');
legend boxoff;

subplot(3,2,3);
hold on;
plot([one.t], [one.meas.phi], 'LineWidth', linewidth);
% plot([two.t], [two.meas.phi], 'LineWidth', linewidth);
plot([three.t], [three.meas.phi], 'LineWidth', linewidth);
plot([four.t], [four.meas.phi], 'LineWidth', linewidth);
plot([five.t], [five.meas.phi], 'LineWidth', linewidth);
title('Measurement phi, \phi');
xlabel('Time (s)');
ylabel('\phi (deg)');
plot(one.t(1:end), 0*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);
plot(one.t(1:end), 220*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);
plot(one.t(1:end), 180*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);
plot(one.t(1:end), 280*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);
plot(one.t(1:end), 120*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);

subplot(3,2,4);
hold on;
plot([one.t], [one.input.phi], 'LineWidth', linewidth);
% plot([two.t], [two.input.phi], 'LineWidth', linewidth);
plot([three.t], [three.input.phi], 'LineWidth', linewidth);
plot([four.t], [four.input.phi], 'LineWidth', linewidth);
plot([five.t], [five.input.phi], 'LineWidth', linewidth);
title('Input phi, \phi');
xlabel('Time (s)');
ylabel('Joystick Position');
plot(one.t(1:end), 0*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);

subplot(3,2,5);
hold on;
plot([one.t], [one.meas.alpha], 'LineWidth', linewidth);
% plot([two.t], [two.meas.alpha], 'LineWidth', linewidth);
plot([three.t], [three.meas.alpha], 'LineWidth', linewidth);
plot([four.t], [four.meas.alpha], 'LineWidth', linewidth);
plot([five.t], [five.meas.alpha], 'LineWidth', linewidth);
% if (abs(max(ys(3,:))) > rad2deg(settings.fov+0.1))
%     plot(ts(1:end-1), sign(max(ys(3,:)))*30*ones(size(ys(1,:))), 'Color', [1 0.6 0.6])
% end
% dots5 = plot(ts(1), ys(3,1), 'ko', 'MarkerFaceColor', 'k');
title('Measurement alpha, \alpha');
xlabel('Time (s)');
ylabel('\alpha (deg)');
plot(one.t(1:end), 0*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);

subplot(3,2,6);
hold on;
plot([one.t], [one.input.alpha], 'LineWidth', linewidth);
% plot([two.t], [two.input.alpha], 'LineWidth', linewidth);
plot([three.t], [three.input.alpha], 'LineWidth', linewidth);
plot([four.t], [four.input.alpha], 'LineWidth', linewidth);
plot([five.t], [five.input.alpha], 'LineWidth', linewidth);
% dots6 = plot(ts(1), us(3,1), 'ko', 'MarkerFaceColor', 'k');
title('Input alpha, \alpha');
xlabel('Time (s)');
ylabel('Joystick Position');
plot(one.t(1:end), 0*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6]);

print('/Users/Michel/Dropbox/University/Thesis/Writeup/Chapters/Results/rename', '-dpng', '-r200');

%%
% hold on;
% plot(one.t, [one.meas.phi], 'LineWidth', linewidth);
% plot(two.t, [two.meas.phi], 'LineWidth', linewidth);
% plot(three.t, [three.meas.phi], 'LineWidth', linewidth);
% plot(four.t, [four.meas.phi], 'LineWidth', linewidth);
% plot(five.t, [five.meas.phi], 'LineWidth', linewidth);
% 
% plot(one.t(1:end), 180*ones(size(one.t)), '--', 'Color', [0.6 0.6 0.6])
% 
% legend('\phi = 1', '\phi = 3', '\phi = 5', '\phi = 10', '\phi = 20');
% legend boxoff;
% 
% ylabel('\phi, heading from magnetic north (deg)');
% xlabel('Time (s)');
% box on;
% 
% print('/Users/Michel/Dropbox/University/Thesis/Writeup/Chapters/Results/phi_meas', '-dpng', '-r200');


%%
% plots = figure('units', 'normalized', 'PaperUnits','centimeters','PaperPosition', printsize, 'outerposition', [0 0 1/2 1], 'name', 'Plots', 'Color',[1 1 1]);
% hold on;
% plot(one.t, [one.input.phi], 'LineWidth', linewidth);
% plot(two.t, [two.input.phi], 'LineWidth', linewidth);
% plot(three.t, [three.input.phi], 'LineWidth', linewidth);
% plot(four.t, [four.input.phi], 'LineWidth', linewidth);
% plot(five.t, [five.input.phi], 'LineWidth', linewidth);
% 
% plot(one.t(1:end), zeros(size(one.t)), '--', 'Color', [0.6 0.6 0.6])
% 
% legend('\phi = 1', '\phi = 3', '\phi = 5', '\phi = 10', '\phi = 20');
% legend boxoff;
% 
% ylabel('Joystick Position');
% xlabel('Time (s)');
% box on;
% % axis square
% 
% print('/Users/Michel/Dropbox/University/Thesis/Writeup/Chapters/Results/phi_input', '-dpng', '-r200');

