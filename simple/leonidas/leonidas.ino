/*********************************************************
** LEONIDAS							                    **
**********************************************************
** Author: Michel Fathallah                             **
** Date: June 2015                                      **
*********************************************************/

/* SD Card library */
#include <ArduCAM.h>
#include <memorysaver.h>
#include <SD.h>
#include <SPI.h>
#include <UTFT_SPI.h>

/* GPS Library */
#include <Adafruit_GPS.h>
#define mySerial Serial1

/* IMU Sensor Module Libraries */
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_L3GD20_U.h>
#include <Adafruit_10DOF.h>

/* Defines */
#define TIMELOG  100
#define xbeeSerial Serial2

/* Create instances of IMU objects */
Adafruit_10DOF                dof   = Adafruit_10DOF();
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(30302);
Adafruit_BMP085_Unified       bmp   = Adafruit_BMP085_Unified(18001);
Adafruit_L3GD20_Unified       gyro  = Adafruit_L3GD20_Unified(20);

/* Create structs of sensor data */
sensors_event_t   accel_event;
sensors_event_t   mag_event;
sensors_event_t   bmp_event;
sensors_event_t   gyro_event;
sensors_vec_t     orientation;

/* Setup Adafruit GPS */
Adafruit_GPS GPS(&mySerial);
#define GPSECHO  false // DEBUG: relay to console


/* Globals */
File myFile;
File logFile;
float altitude = 0;
float altitudeOffset = 0;
float temperature = 0;
unsigned long mainMillis = 0;
int analogVal0 = 0;
int analogVal1 = 0;
int analogVal2 = 0;

// TODO: Implement camera - take more quickly when the norm of the acceleration is > x
int i = 0;


void setup() {
  // Setup serial port
  SerialUSB.begin(9600);
  xbeeSerial.begin(9600);

  delay(5000);

  // Initialise SD card
  SerialUSB.print("Initializing SD card...");

  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);

  if (!SD.begin(9)) {
    SerialUSB.println("Initialization failed!");
    //return;
  } else {
    SerialUSB.println("Initialization done.");
  }

  // Open file and test it
  myFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (myFile) {
    SerialUSB.print("Writing to test.txt...");
    myFile.println("Testing 1, 2, 3.");
    // close the file:
    myFile.close();
    SerialUSB.println("Done.");
  }
  else {
    // if the file didn't open, print an error:
    SerialUSB.println("Error opening test.txt");
  }

  // re-open the file for reading:
  myFile = SD.open("test.txt");
  if (myFile) {
    SerialUSB.println("test.txt:");
    // read from the file until there's nothing else in it:
    while (myFile.available()) {
      SerialUSB.write(myFile.read());
    }
    // close the file:
    myFile.close();
  }
  else {
    // if the file didn't open, print an error:
    SerialUSB.println("Error opening test.txt");
  }


  // Initiate sensors
  initSensors();

  // Calibrate barometer to take current height
  zeroBarometer();

  // Initialise GPS
  GPS.begin(9600);
  mySerial.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA); // turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);   // 1 Hz update rate
  //  GPS.sendCommand(PGCMD_ANTENNA); // DEBUG: request updates on antenna status, comment out to keep quiet



}



void loop() {

  // query the GPS everytime in the main loop
  char c = GPS.read();
  if (GPSECHO)
    if (c) SerialUSB.print(c);

  // if a sentence is received then check checksum and parse it
  if (GPS.newNMEAreceived()) {
    GPS.parse(GPS.lastNMEA());   // this sets the newNMEAreceived() flag to false
    // TODO: if NOT, then just return = fail to parse => wait for another
  }


  // wrap around for millis() timer - shouldn't happen (days long) but just in case
  if (mainMillis > millis()) mainMillis = millis();

  // log every so often
  if (millis() - mainMillis >= TIMELOG) {
    mainMillis = millis();

    /******************* Acquire IMU & barometer data *********************/
    /* Get pitch and roll from the raw accelerometer data */
    accel.getEvent(&accel_event);

    /* Get the heading using the magnetometer */
    mag.getEvent(&mag_event);

    /* Merge accel/mag data for AHRS algorithm */
    dof.fusionGetOrientation(&accel_event, &mag_event, &orientation);

    /* Get barometer data */
    bmp.getEvent(&bmp_event);

    /* Get temperature */
    bmp.getTemperature(&temperature);

    /* Convert atmospheric pressure, SLP and temp to altitude */
    altitude = bmp.pressureToAltitude(SENSORS_PRESSURE_SEALEVELHPA,
                                      bmp_event.pressure,
                                      temperature) - altitudeOffset;

    /* Get the gyroscope data */
    gyro.getEvent(&gyro_event);

    /* Read analog stuff */
    analogVal0 = analogRead(A0);
    analogVal1 = analogRead(A1);
    analogVal2 = analogRead(A2);


    /******************** Log Data **********************/
    logIMU();
    logGPS();

    /******************* Print to XBee *************************/
//    sendIMU();
//    sendGPS();

    /******************* Print to USB Data *********************/
    usbIMU();
    usbGPS();
    
  }
  
  
}




/************************************************************
*                        FUNCTIONS                          *
************************************************************/
// log IMU data
void logIMU(void) {

  logFile = SD.open("log.txt", FILE_WRITE);
  if (logFile) {

    // Log data
    logFile.print(mainMillis);

    logFile.print("\t"); logFile.print(accel_event.acceleration.x);
    logFile.print("\t"); logFile.print(accel_event.acceleration.y);
    logFile.print("\t"); logFile.print(accel_event.acceleration.z);

    logFile.print("\t"); logFile.print(mag_event.magnetic.x);
    logFile.print("\t"); logFile.print(mag_event.magnetic.y);
    logFile.print("\t"); logFile.print(mag_event.magnetic.z);

    logFile.print("\t"); logFile.print(gyro_event.gyro.x);
    logFile.print("\t"); logFile.print(gyro_event.gyro.y);
    logFile.print("\t"); logFile.print(gyro_event.gyro.z);

    logFile.print("\t"); logFile.print(orientation.roll);
    logFile.print("\t"); logFile.print(orientation.pitch);
    logFile.print("\t"); logFile.print(orientation.heading);
    logFile.print("\t"); logFile.print(bmp_event.pressure);
    logFile.print("\t"); logFile.print(temperature);

    logFile.print("\t"); logFile.print(analogVal0);
    logFile.print("\t"); logFile.print(analogVal1);
    logFile.print("\t"); logFile.print(analogVal2);

    // close the file:
    logFile.close();

  }

}

void sendIMU(void) {

  xbeeSerial.print(mainMillis);

  float norm = sqrt(pow(accel_event.acceleration.x, 2) + pow(accel_event.acceleration.y, 2) + pow(accel_event.acceleration.z, 2));
  xbeeSerial.print("\t"); xbeeSerial.print(norm);

  //  xbeeSerial.print("\t"); xbeeSerial.print(accel_event.acceleration.x);
  //  xbeeSerial.print("\t"); xbeeSerial.print(accel_event.acceleration.y);
  //  xbeeSerial.print("\t"); xbeeSerial.print(accel_event.acceleration.z);
  //
  //  xbeeSerial.print("\t"); xbeeSerial.print(mag_event.magnetic.x);
  //  xbeeSerial.print("\t"); xbeeSerial.print(mag_event.magnetic.y);
  //  xbeeSerial.print("\t"); xbeeSerial.print(mag_event.magnetic.z);
  //
  //  xbeeSerial.print("\t"); xbeeSerial.print(gyro_event.gyro.x);
  //  xbeeSerial.print("\t"); xbeeSerial.print(gyro_event.gyro.y);
  //  xbeeSerial.print("\t"); xbeeSerial.print(gyro_event.gyro.z);
  //
  //  xbeeSerial.print("\t"); xbeeSerial.print(orientation.roll);
  //  xbeeSerial.print("\t"); xbeeSerial.print(orientation.pitch);
  //  xbeeSerial.print("\t"); xbeeSerial.print(orientation.heading);
  //
  //  xbeeSerial.print("\t"); xbeeSerial.print(bmp_event.pressure);
  //  xbeeSerial.print("\t"); xbeeSerial.print(temperature);
  //
  //  xbeeSerial.print("\t"); xbeeSerial.print(analogVal0);
  //  xbeeSerial.print("\t"); xbeeSerial.print(analogVal1);
  //  xbeeSerial.print("\t"); xbeeSerial.print(analogVal2);

}


// print IMU to usb function
void usbIMU(void) {

  SerialUSB.print(mainMillis);

  SerialUSB.print("\t"); SerialUSB.print(accel_event.acceleration.x);
  SerialUSB.print("\t"); SerialUSB.print(accel_event.acceleration.y);
  SerialUSB.print("\t"); SerialUSB.print(accel_event.acceleration.z);

  SerialUSB.print("\t"); SerialUSB.print(mag_event.magnetic.x);
  SerialUSB.print("\t"); SerialUSB.print(mag_event.magnetic.y);
  SerialUSB.print("\t"); SerialUSB.print(mag_event.magnetic.z);

  SerialUSB.print("\t"); SerialUSB.print(gyro_event.gyro.x);
  SerialUSB.print("\t"); SerialUSB.print(gyro_event.gyro.y);
  SerialUSB.print("\t"); SerialUSB.print(gyro_event.gyro.z);

  SerialUSB.print("\t"); SerialUSB.print(orientation.roll);
  SerialUSB.print("\t"); SerialUSB.print(orientation.pitch);
  SerialUSB.print("\t"); SerialUSB.print(orientation.heading);

  SerialUSB.print("\t"); SerialUSB.print(bmp_event.pressure);
  SerialUSB.print("\t"); SerialUSB.print(temperature);

  SerialUSB.print("\t"); SerialUSB.print(analogVal0);
  SerialUSB.print("\t"); SerialUSB.print(analogVal1);
  SerialUSB.print("\t"); SerialUSB.print(analogVal2);

}

// GPS

void logGPS(void) {

  logFile = SD.open("log.txt", FILE_WRITE);
  if (logFile) {
    // fix & quality & satellites
    logFile.print("\t"); logFile.print((int)GPS.fix);
    logFile.print("\t"); logFile.print((int)GPS.fixquality);
    logFile.print("\t"); logFile.print((int)GPS.satellites);

    // location
    logFile.print("\t"); logFile.print(GPS.latitude, 4); logFile.print(GPS.lat);
    logFile.print("\t"); logFile.print(GPS.longitude, 4); logFile.print(GPS.lon);
    logFile.print("\t"); logFile.print(GPS.altitude);

    // geoidheight & HDOP
    logFile.print("\t"); logFile.print(GPS.geoidheight);
    logFile.print("\t"); logFile.print(GPS.HDOP);
    // time
    logFile.print("\t"); logFile.print(GPS.hour, DEC);
    logFile.print("\t"); logFile.print(GPS.minute, DEC);
    logFile.print("\t"); logFile.print(GPS.seconds, DEC);

    // newline
    logFile.print("\n");

    // close the file
    logFile.close();
  }


}

void sendGPS(void) {
  // fix & quality & satellites
  xbeeSerial.print("\t"); xbeeSerial.print((int)GPS.fix);
  xbeeSerial.print("\t"); xbeeSerial.print((int)GPS.fixquality);
  xbeeSerial.print("\t"); xbeeSerial.print((int)GPS.satellites);

  // location
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.latitude, 4); xbeeSerial.print(GPS.lat);
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.longitude, 4); xbeeSerial.print(GPS.lon);
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.altitude);

  // geoidheight & HDOP
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.geoidheight);
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.HDOP);

  // time
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.hour, DEC);
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.minute, DEC);
  xbeeSerial.print("\t"); xbeeSerial.print(GPS.seconds, DEC);

  // newline
  xbeeSerial.print("\n");

}


void usbGPS(void) {

  /******************* Acquire GPS data *********************/
  // fix & quality & satellites
  SerialUSB.print("\t"); SerialUSB.print((int)GPS.fix);
  SerialUSB.print("\t"); SerialUSB.print((int)GPS.fixquality);
  SerialUSB.print("\t"); SerialUSB.print((int)GPS.satellites);

  // location
  SerialUSB.print("\t"); SerialUSB.print(GPS.latitude, 4); SerialUSB.print(GPS.lat);
  SerialUSB.print("\t"); SerialUSB.print(GPS.longitude, 4); SerialUSB.print(GPS.lon);
  SerialUSB.print("\t"); SerialUSB.print(GPS.altitude);

  // geoidheight & HDOP
  SerialUSB.print("\t"); SerialUSB.print(GPS.geoidheight);
  SerialUSB.print("\t"); SerialUSB.print(GPS.HDOP);

  // time
  SerialUSB.print("\t"); SerialUSB.print(GPS.hour, DEC);
  SerialUSB.print("\t"); SerialUSB.print(GPS.minute, DEC);
  SerialUSB.print("\t"); SerialUSB.print(GPS.seconds, DEC);

  //  // date
  //  SerialUSB.print("\t"); SerialUSB.print(GPS.day, DEC);
  //  SerialUSB.print("\t"); SerialUSB.print(GPS.month, DEC);
  //  SerialUSB.print("\t"); SerialUSB.print(GPS.year, DEC);

  // newline
  SerialUSB.print("\n");


}









// Initialise IMU sensors
void initSensors(void) {
  if (!mag.begin()) {
    SerialUSB.println("No LSM303 detected.\n");
  }
  if (!accel.begin()) {
    SerialUSB.println("No LSM303 detected.\n");
  }
  if (!bmp.begin()) {
    SerialUSB.println("No BMP180 detected.\n");
  }

  // Enable gyro auto-ranging
  gyro.enableAutoRange(true);
  if (!gyro.begin()) {
    SerialUSB.println("No L3GD20 detected.\n");
  }


}


// Store current barometer height
void zeroBarometer(void) {
  bmp.getEvent(&bmp_event);
  bmp.getTemperature(&temperature);
  altitudeOffset = bmp.pressureToAltitude(SENSORS_PRESSURE_SEALEVELHPA,
                                          bmp_event.pressure,
                                          temperature);
}




