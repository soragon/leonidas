#ifndef _SELFIECAM_H_
#define _SELFIECAM_H_

#include <ArduCAM.h>
#include <memorysaver.h>
#include <SD.h>
#include <SPI.h>
#include <UTFT_SPI.h>
#include <Wire.h>

#include "log.h"

//
// Example usage of SelfieCam:
//    SelfieCam camera;
//    camera.Initialise();
//    camera.DisplayImage("SELFIE.BMP");
//    camera.CaptureImage("SSELFIE.JPG");
//
class SelfieCam {
 public:
  SelfieCam(const int spi_cs);
  ~SelfieCam();

  ///
  /// Initialise the selfiecam. This must be called before any other
  /// methods will do anything. Will return `false` if something
  /// goes wrong. Before this is run:
  ///   1. Wire must be initialised.
  ///   2. SD card module must be started.
  ///   3. SPI must be started.
  ///
  bool Initialise();

  ///
  /// Capture the image currently visible by the camera and save the result
  /// into `filename`.
  ///
  bool CaptureImage(const char* filename);

  ///
  /// Display the given image on the screen. The given filename must be a
  /// 16-bit BMP file using the RGB666 encoding and no colour space information.
  /// The image _must_ be 320x240 (W x H).
  ///
  bool DisplayImage(const char* filename);

  ///
  /// Take a selfie using the given input and output filenames.
  ///
  bool TakeSelfie(const char* input, const char* output);

  ///
  /// Put the selfie cam into a low-power mode (turn off the LCD).
  ///
  void Disable();

  ///
  /// Bring the selfie cam back up after being switched to low power mode.
  ///
  void Enable();

  UTFT* display() { return &_display; }
  ArduCAM* camera() { return &_camera; }

 private:
  ArduCAM _camera;
  UTFT _display;

  // True if the IMU has been initialised.
  bool _initialised;
};

#endif  // _SELFIECAM_H_
