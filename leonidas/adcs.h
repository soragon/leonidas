#ifndef _ADCS_H_
#define _ADCS_H_

#include <DueTimer.h>
#include <PID_v1.h>

#include "imu.h"
#include "log.h"
#include "magnetorquer.h"
#include "sunsensor.h"

class ADCS {
 public:
  // Refresh rate of the ADCS controller (in ms).
  static const int kRefreshRate = 100;

  // Enum to make naming directions easier.
  enum Direction {
    X=0, Y=1, Z=2, NUM_DIRECTIONS=3
  };

  ///
  /// Constructor + Destructor.
  ///
  ADCS(DueTimer* timer=&Timer2);
  ~ADCS();

  ///
  /// Setup the ADCS, including setting up the controllers, timer, sensors and
  /// actuators. Will return `false` if anything couldn't be initialised.
  ///
  bool Initialise();

  ///
  /// Enable to ADCS with some goal heading. The system will attempt to align
  /// the satellite with the given x, y and z degree values. It will do this one
  /// axis at a time. Completion can be checked using GoalReached().
  ///
  void Enable(int x, int y, int z);

  ///
  /// Enable the ADCS without a goal heading; the location of the sun will be
  /// used.
  ///
  void Enable();

  ///
  /// Turn off the ADCS. The control algorithms will no longer be used and any
  /// guarantee of the pointing accuracy will be lost.
  ///
  void Disable();

  ///
  /// Check to see if the current goal has been reached. This will happen when
  /// the current heading is within kRequiredAccuracy degrees of the requested
  /// heading. Note that this result may not be repeatable, especially early in
  /// the control algorithm.
  ///
  bool GoalReached();

  double* outputs() { return _outputs; }
  PID* pids() { return _controllers; }
  IMU* imu() { return &_imu; }

 private:
  // Sensors.
  IMU _imu;
  SunSensor _sun_sensors[5];

  // Actuators.
  Magnetorquer _magnetorquers[NUM_DIRECTIONS];

  // Other members.
  DueTimer* _timer;
  bool _initialised;

  // If set the true, use the sun to determine the setpoints.
  bool _use_sun;

  // Control variables.
  double _inputs[NUM_DIRECTIONS];
  double _prev_inputs[NUM_DIRECTIONS];
  double _outputs[NUM_DIRECTIONS];
  double _setpoints[NUM_DIRECTIONS];

  // Controllers.
  PID _controllers[NUM_DIRECTIONS];

  // Control constants.
  static const double kP[NUM_DIRECTIONS];
  static const double kI[NUM_DIRECTIONS];
  static const double kD[NUM_DIRECTIONS];
  static const double kRequiredAccuracy = 1;

  // Actually run an instance of PID control. Only one controller will be
  // enabled at any time (to avoid interference).
  void Control();

  // Load a set of readings from the IMU and average them into the given set of
  // readings with a short delay inbetween each reading (in ms).
  void GetAverageResults(int n, int ms, sensors_vec_t *average);

  friend void _adcs_control_interrupt();
  static ADCS* _instance;
};

#endif  // _ADCS_H_
