#ifndef _SERIAL_TRANSMITTER_H_
#define _SERIAL_TRANSMITTER_H_

#include <DueTimer.h>

#include "log.h"
#include "transmitter.h"

class SerialTransmitter : public Transmitter {
 public:
  static const int kSerialBaudRate = 38400; // bps
  static const int kTransmitFrequency = 100; // us
  static const int kReceiveFrequency = 100; // us

  ///
  /// Constructor + Destructor.
  ///
  SerialTransmitter(Transmitter::CharacterBuffer* tx,
    Transmitter::CharacterBuffer* rx, Stream* serial=&Serial2,
    DueTimer* tx_timer=&Timer3, DueTimer* rx_timer=&Timer4);
  ~SerialTransmitter();

  ///
  /// Setup the serial transmitter.
  ///
  bool Initialise();

  ///
  /// Temporarily stop/start TX/RX.
  ///
  void StopRX() { _rx_timer->stop(); }
  void StopTX() { _tx_timer->stop(); }
  void StartRX() { _rx_timer->start(); }
  void StartTX() { _tx_timer->start(); }

 private:
  Stream* _serial;
  DueTimer* _tx_timer;
  DueTimer* _rx_timer;

  // This transmitter has to be a singleton because of the interrupts.
  static SerialTransmitter* _instance;

  friend void _serial_transmitter_transmit();
  friend void _serial_transmitter_receive();
};

#endif  // _SERIAL_TRANSMITTER_H_
