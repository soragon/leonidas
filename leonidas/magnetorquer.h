#ifndef _MAGNETORQUER_H_
#define _MAGNETORQUER_H_

#include "log.h"

class Magnetorquer {
 public:
  enum Direction {
    CW, CCW
  };

  Magnetorquer(int pwm_pin, int ain1, int ain2);
  ~Magnetorquer();

  bool Initialise();
  void SetOutput(int level);
  void ReverseDirection();
  int GetLevel() { return _level; }

 private:
  int _pin, _ain1, _ain2, _direction, _level;
  bool _initialised;
};

#endif  // _MAGNETORQUER_H_
