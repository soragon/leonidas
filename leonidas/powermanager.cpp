#include "powermanager.h"

PowerManager::Battery PowerManager::_active_battery = PowerManager::BATTERY_1;

bool PowerManager::Initialise() {
  // Setup the power switch.
  pinMode(kPowerSwitch, OUTPUT);

  // Setup the power bus readings.
  pinMode(kBattery1CurrentPin, INPUT);
  pinMode(kBattery1VoltagePin, INPUT);
  pinMode(kBattery2CurrentPin, INPUT);
  pinMode(kBattery2VoltagePin, INPUT);
  pinMode(k33VCurrentPin, INPUT);
  pinMode(k5VCurrentPin, INPUT);

  // Enable battery 1 by default.
  if (BatteryVoltage(BATTERY_1) > BatteryVoltage(BATTERY_2)) {
    _active_battery = BATTERY_1;
  } else {
    _active_battery = BATTERY_2;
  }

  SetBattery();

  Log::Info("Power Manager: Initialised!");
  return true;
}

// Set different batteries on/off.
void PowerManager::SetBattery(Battery battery) {
  if (battery == ACTIVE) {
    battery = _active_battery;
  }

  switch (battery) {
    case BATTERY_1:
      digitalWrite(kPowerSwitch, LOW);
      _active_battery = BATTERY_1;
      break;
    case BATTERY_2:
      digitalWrite(kPowerSwitch, HIGH);
      _active_battery = BATTERY_2;
      break;
  }
}

// Get diagnostics about the power bus.
double PowerManager::BatteryCurrent(Battery battery) {
  if (battery == ACTIVE) {
    battery = _active_battery;
  }

  switch (battery) {
    case BATTERY_1:
      return ReadCurrent(kBattery1CurrentPin);
    case BATTERY_2:
      return ReadCurrent(kBattery2CurrentPin);
  }
}

double PowerManager::BatteryVoltage(Battery battery) {
  if (battery == ACTIVE) {
    battery = _active_battery;
  }

  switch (battery) {
    case BATTERY_1:
      return ReadCurrent(kBattery1VoltagePin);
    case BATTERY_2:
      return ReadCurrent(kBattery2VoltagePin);
  }
}

double PowerManager::BusCurrent(Bus bus) {
  switch (bus) {
    case B33V:
      return ReadCurrent(k33VCurrentPin);
    case B5V:
      return ReadCurrent(k33VCurrentPin);
  }
}

double PowerManager::ReadVoltage(const int pin) {
  return (analogRead(pin) / kPossibleADValues) * kMaxVoltage;
}

double PowerManager::ReadCurrent(const int pin) {
  return ReadVoltage(pin) / kResistance;
}
