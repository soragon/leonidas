///
/// Selfiecam Module Includes
///
// Dependencies.
#include <Wire.h>

// ArduCAM Libraries.
#include <ArduCAM.h>
#include <memorysaver.h>
#include <SD.h>
#include <SPI.h>
#include <UTFT_SPI.h>

// IMU Libraries.
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_Simple_AHRS.h>

// UV Libraries.
#include "Adafruit_SI1145.h"

// GPS Libraries
#include <Adafruit_GPS.h>
#include <DueTimer.h>

// ADCS Libraries.
#include <PID_v1.h>

// Actual Code.
#include "gps.h"
#include "adcs.h"
#include "selfiecam.h"
#include "sunsensor.h"

// Utility: Logger.
#include "log.h"

// Main Program.
#include "leonidas.h"

Leonidas satellite;

void setup() {
  satellite.Initialise();
}

void loop() {
  satellite.Run();
}
