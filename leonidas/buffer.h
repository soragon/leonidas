#ifndef _BUFFER_H_
#define _BUFFER_H_

template <unsigned int SIZE, typename T>
class Buffer {
 public:
  ///
  /// Constructor + Destructor.
  ///
  Buffer(T invalid_item=T())
    : _size(0), _start(0), _end(0), _invalid_item(invalid_item) {

  }

  ~Buffer() {

  }

  ///
  /// Add a single item to the buffer. This will physically copy the item.
  /// Because of this, the item should have the = operator defined.
  ///
  void Add(const T item) {
    // If we have reached our maximum size, do nothing.
    if (size() == max_size()) {
      return;
    }

    // Add the item to the end.
    _buffer[_end] = item;

    // Move the end along and increase the size.
    _end++;
    _size++;

    // If end has overflowed, then loop around.
    if (_end >= max_size()) {
      _end = 0;
    }
  }

  ///
  /// Add an array of items to the buffer. The array of items will be treated
  /// as constants (they will not be changed, they will only be copied). This is
  /// just a shortcut for calling Add() repeatedly.
  ///
  void AddAll(const T* items, unsigned int size) {
    for (unsigned int i = 0; i < size; i++) {
      Add(items[i]);
    }
  }

  ///
  /// Return the item at the _start position in the buffer. Will return
  /// invalid_item if empty.
  ///
  T Peek() {
    if (empty()) {
      return _invalid_item;
    }

    return _buffer[_start];
  }

  ///
  /// Return the item at the _start position and remove it from the buffer. Will
  /// return invalid_item if empty.
  ///
  T Next() {
    if (empty()) {
      return _invalid_item;
    }

    // Get the next item.
    T next = _buffer[_start];

    // Move the start pointer and reduce the size.
    _start++;
    _size--;

    // Loop start around if necessary.
    if (_start >= max_size()) {
      _start = 0;
    }

    return next;
  }

  ///
  /// Get the item at index `i`. This method will take the circular nature of
  /// the buffer into account when returning the item. If `i` is larger than the
  /// size of the array, this will return `invalid_item`.
  ///
  T Get(unsigned int index) {
    if (index >= max_size()) {
      return _invalid_item;
    }

    // Compute the real index.
    int real_index = _start + index;
    if (real_index >= max_size()) {
      real_index = real_index - max_size();
    }

    // Get the item.
    return _buffer[real_index];
  }

  ///
  /// Find the given item within the populated part of the buffer. If the item
  /// exists, will return `true`. This is basically a shortcut for using Get().
  ///
  bool Find(T val) {
    for (unsigned int i = 0; i < size(); i++) {
      if (Get(i) == val) {
        return true;
      }
    }

    return false;
  }

  ///
  /// Reset the buffer to some default value. By default, it will be reset to
  /// _invalid_item.
  ///
  void Reset(T val) {
    // Reset the stats.
    _start = _end = _size = 0;

    // Reset the contents of the buffer.
    for (unsigned int i = 0; i < max_size(); i++) {
      _buffer[i] = val;
    }
  }

  void Reset() {
    Reset(_invalid_item);
  }

  ///
  /// Returns true if the buffer is empty.
  ///
  bool empty() const { return size() == 0; }

  ///
  /// Return the current size of the buffer.
  ///
  unsigned int size() const { return _size; }

  ///
  /// Return the maximum size of the buffer.
  ///
  unsigned int max_size() const { return SIZE; }

  ///
  /// Actually get a pointer to the data. This probably shouldn't be used in all
  /// but the most special circumstances.
  ///
  T* data() { return _buffer; }

 private:
  // Actual data.
  T _buffer[SIZE];

  // Size of the buffer (starts at 0).
  int _size;

  // Insertion and deletion pointers.
  int _start, _end;

  // Item to use as the 'invalid item' placeholder.
  const T _invalid_item;
};

#endif  // _BUFFER_H_
