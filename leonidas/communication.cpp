#include "communication.h"

const char Communication::AX25::kDestinationAddress[] = "USYD  ";
const char Communication::AX25::kSourceAddress[]  = "LEON  ";
Communication* Communication::_instance = NULL;

void _communication_send_beacon() {
  if (Communication::instance() != NULL) {
    Communication::instance()->SendBeacon();
  }
}

Communication::Communication(DueTimer* beacon_timer)
  : _tx(kTerminatingCharacter), _rx(kTerminatingCharacter)
  , _tx_on(true), _rx_on(true)
  , _transmitter(&_tx, &_rx)
  , _beacon_timer(beacon_timer) {
  _tx.Reset(kTerminatingCharacter);
  _tx.Reset(kTerminatingCharacter);
}

Communication::~Communication() {

}

bool Communication::Initialise() {
  // Setup the transmitter.
  if (not _transmitter.Initialise()) {
    Log::Error("Communication: could not initialise transmitter.");
    return false;
  }

  // Setup the beacon.
  _beacon_timer->attachInterrupt(_communication_send_beacon);
  _beacon_timer->start(kBeaconTransmitFrequency * 1000 * 1000);

  return true;
}

void Communication::LogMessage(const char* message) {
  char format[256];
  snprintf(format, sizeof(format), "LOG %s%c", message, kTerminatingCharacter);
  AddCommandToTransmitBuffer(format, false, false);
}

void Communication::SendFileListing() {
  StopTX();

  // Add the required header.
  AttachAX25Header();

  // Add the command.
  char format[32];
  snprintf(format, sizeof(format), "LIST%c", kTerminatingCharacter);
  _tx.AddAll(format, strlen(format));

  // Add the file listing.
  int file_listing_length = 0;
  File root = SD.open("/");
  root.rewindDirectory();
  while (File f = root.openNextFile()) {
    if (file_listing_length + strlen(f.name()) + 1 > AX25::kInfoLength - strlen(format)) {
      break;
    }

    file_listing_length += strlen(f.name()) + 1;
    _tx.AddAll(f.name(), strlen(f.name()));
    _tx.Add(' ');
  }

  // Pad with zeroes.
  PadZeroes(AX25::kInfoLength - (file_listing_length + strlen(format)));

  // Add the required footer.
  AttachAX25Tail();

  StartTX();
}

void Communication::ACK(const char* command) {
  char format[32];
  snprintf(format, sizeof(format), "ACK %s%c", command, kTerminatingCharacter);
  AddCommandToTransmitBuffer(format, false, false);
}

void Communication::NACK(const char* command, const char* reason) {
  char format[32];
  snprintf(format, sizeof(format)
    , "NACK %s %s%c", command, reason, kTerminatingCharacter);
  AddCommandToTransmitBuffer(format, false, false);
}

void Communication::Send(const char* command, const char* parameter) {
  char format[32];
  snprintf(format, sizeof(format)
    , "%s %s%c", command, parameter, kTerminatingCharacter);
  AddCommandToTransmitBuffer(format, false, true);
}

bool Communication::TransmitBlock(const char* filename, int block) {
  // Make sure we can read the given file.
  File file = SD.open(filename, FILE_READ);
  if (!file) {
    return false;
  }

  // Check to see if an invalid block was selected. If it was, it is likely we
  // have finished transmitting the file.
  if (block * kBlockSize > file.size()) {
    file.close();
    return false;
  }

  // Seek to the correct location in the file.
  int block_start = block * kBlockSize;
  int total_blocks = ceil(file.size() / (float)kBlockSize);
  file.seek(block_start);

  // Number of bytes we are going to read.
  int bytes_to_read = min(kBlockSize, file.size() - block_start);

  // Add command prefix to transmit buffer.
  char format[64];
  snprintf(format, sizeof(format)
    , "BLOCK %s %d %d %d%c"
    , filename, block, total_blocks, bytes_to_read, kTerminatingCharacter);

  // Actually transmit the block.
  StopTX();

  // Add the ax25 Header
  AttachAX25Header(true, true);

  // Add the command.
  _tx.AddAll(format, strlen(format));

  // Transmit the next block.
  for (int i = 0; i < bytes_to_read; i++) {
    _tx.Add(file.read());
  }

  // Fill any extra data with 0
  PadZeroes(AX25::kInfoLength - (strlen(format) + bytes_to_read));

  // Add the tail.
  AttachAX25Tail();

  // Done! Restart TX.
  StartTX();

  // Close the file.
  file.close();

  // Blocks will now be sent when the transmitter wakes up.
  return true;
}

void Communication::SendBeacon() {
  // Don't send the beacon if transmission is disabled.
  if (not _tx_on) {
    return;
  }

  char format[32];
  snprintf(format, sizeof(format), "PING%c", kTerminatingCharacter);
  AddCommandToTransmitBuffer(format, false, false);
}

void Communication::AddCommandToTransmitBuffer(
  const char* command, bool science_data, bool expect_ack) {
  // Stop transmit while we add to it.
  StopTX();

  // Add the header.
  AttachAX25Header(science_data, expect_ack);

  // Add the payload (command).
  _tx.AddAll(command, strlen(command));

  // Pad with zeroes.
  PadZeroes(AX25::kInfoLength - strlen(command));

  // Add the tail.
  AttachAX25Tail();

  // Keep transmitting.
  StartTX();
}

void Communication::PadZeroes(int count) {
  char zeroes[count];
  memset(zeroes, '\0', count);
  _tx.AddAll(zeroes, count);
}

void Communication::AttachAX25Header(bool science_data, bool expect_ack)
{
  // Initial flag.
  _tx.Add(AX25::kFlag);

  // Destination address.
  _tx.AddAll(AX25::kDestinationAddress, strlen(AX25::kDestinationAddress));

  // Destination SSID.
  if (science_data) {
    _tx.Add(AX25::kSciencePackage);
  } else {
    _tx.Add(AX25::kOrbitPackage);
  }

  // Source address.
  _tx.AddAll(AX25::kSourceAddress, strlen(AX25::kSourceAddress));

  // Source SSID.
  _tx.Add(AX25::kSourceSSID);

  // Control byte.
  if (expect_ack) {
    _tx.Add(AX25::kControlNeedAck);
  } else {
    _tx.Add(AX25::kControlNoAck);
  }

  // PID.
  _tx.Add(AX25::kPid);
}

void Communication::AttachAX25Tail()
{
  // CRC data.
  // TODO(jeshua,bas): actually calculate the CRC.
  _tx.Add(0xFF);
  _tx.Add(0xFF);

  // Final flag.
  _tx.Add(AX25::kFlag);
}

Communication* Communication::instance() {
  return _instance;
}
