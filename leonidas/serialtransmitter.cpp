#include "serialtransmitter.h"

SerialTransmitter* SerialTransmitter::_instance;

void _serial_transmitter_transmit() {
  SerialTransmitter* t = SerialTransmitter::_instance;
  if (t != NULL) {
    while (!t->_tx->empty()) {
      t->_serial->print(t->_tx->Next());
    }
  }
}

void _serial_transmitter_receive() {
  SerialTransmitter* t = SerialTransmitter::_instance;
  if (t != NULL) {
    while (t->_serial->available() > 0) {
      t->_rx->Add(t->_serial->read());
    }
  }
}

SerialTransmitter::SerialTransmitter(
  Transmitter::CharacterBuffer* tx, Transmitter::CharacterBuffer* rx,
  Stream* serial, DueTimer* tx_timer, DueTimer* rx_timer)
  : Transmitter(tx, rx), _serial(serial), _tx_timer(tx_timer)
  , _rx_timer(rx_timer) {
  _instance = this;
}

SerialTransmitter::~SerialTransmitter() {

}

bool SerialTransmitter::Initialise() {
  // Setup the serial comms.
  if (reinterpret_cast<typeof(Serial)*>(_serial) != NULL) {
    reinterpret_cast<typeof(Serial)*>(_serial)->begin(kSerialBaudRate);
  }

  if (reinterpret_cast<typeof(SerialUSB)*>(_serial) != NULL) {
    reinterpret_cast<typeof(SerialUSB)*>(_serial)->begin(kSerialBaudRate);
  }

  // Setup the TX timer.
  _tx_timer->attachInterrupt(_serial_transmitter_transmit);
  _rx_timer->attachInterrupt(_serial_transmitter_receive);
  _tx_timer->start(kTransmitFrequency);
  _rx_timer->start(kReceiveFrequency);
}
