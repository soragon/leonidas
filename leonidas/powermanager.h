#ifndef _POWER_MANAGER_H_
#define _POWER_MANAGER_H_

#include <Arduino.h>

#include "log.h"

class PowerManager {
public:
  enum Battery {
    BATTERY_1, BATTERY_2, ACTIVE
  };

  enum Bus {
    B33V, B5V
  };

  static bool Initialise();

  // Set different batteries on/off.
  static void SetBattery(Battery battery=ACTIVE);

  // Get diagnostics about the power bus.
  static double BatteryCurrent(Battery battery=ACTIVE);
  static double BatteryVoltage(Battery battery=ACTIVE);
  static double BusCurrent(Bus bus);

private:
  // Switch to enable/disable battery 1 or 2.
  static const int kPowerSwitch = 22;

  // Voltage and current readings for battery 1.
  static const int kBattery1CurrentPin = A5;
  static const int kBattery1VoltagePin = A9;

  // Voltage and current readings for battery 2.
  static const int kBattery2CurrentPin = A6;
  static const int kBattery2VoltagePin = A10;

  // Current on the 3.3V and 5V busses.
  static const int k33VCurrentPin = A7;
  static const int k5VCurrentPin = A8;

  // Currently active battery.
  static Battery _active_battery;

  // Number of possible A/D converter values.
  #if defined (__arm__)
    static const double kPossibleADValues = 4096.0; // 2 ^ 12
  #else
    static const double kPossibleADValues = 1024.0; // 2 ^ 10
  #endif

  // Maximum voltage as a reference to the A/D converter.
  static const double kMaxVoltage = 3.3;

  // Resistance constant used to calculate current (I = V / R).
  static const double kResistance = 0.1;

  // Read the voltage/current from the given analog pin.
  static double ReadVoltage(const int pin);
  static double ReadCurrent(const int pin);
};

#endif  // _POWER_MANAGER_H_
