#ifndef _RTC_H_
#define _RTC_H_

#include "DS3231.h"

#include "gps.h"
#include "log.h"

class LRTC {
public:
  typedef struct ts DateTime;

  ///
  /// Setup the RTC.
  ///
  static bool Initialise();


  ///
  /// Update the current time using the GPS.
  ///
  static void Update(GPS& gps);

  ///
  /// Get the current time from the RTC.
  ///
  static DateTime Get();

private:

};

#endif  // _RTC_H_
