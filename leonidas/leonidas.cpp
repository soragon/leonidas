#include "leonidas.h"

using Utility::Equal;
using Utility::SDCopy;

// Reset the satellite.
void ResetSatellite() {
  // Disable all timers.
  Timer0.stop(); Timer1.stop(); Timer2.stop(); Timer3.stop(); Timer4.stop();
  Timer5.stop(); Timer6.stop(); Timer7.stop(); Timer8.stop();

  NVIC_SystemReset();
}

// Watchdog timer.
void _watchdog_timer_function() {
  Log::Info("Watchdog bite!");
  ResetSatellite();
}

// Die.
void Die() {
  while (true);
}

// Feed the watchdog timer.
void Leonidas::FeedWatchdog() {
  _watchdog_timer->stop();
  _watchdog_timer->start(kWatchdogTimerTimeout * 1000 * 1000);
}

Leonidas::Leonidas()
  : _camera(kSPI_CS), _gps(Serial1), _adcs(), _comms(), _commands(_comms.rx())
  , _in_safe_mode(false), _watchdog_timer(&Timer8) {

}

Leonidas::~Leonidas() {

}

void Leonidas::Initialise() {
  /// Setup the watchdog timer.
  _watchdog_timer->attachInterrupt(_watchdog_timer_function);
  _watchdog_timer->start(kWatchdogTimerTimeout * 1000 * 1000);

  ///
  /// Setup the logger. This should be done first.
  ///
  Log::Initialise();
  Log::max_level(Log::DEBUG);
  SerialUSB.begin(115200);
  Log::serial(&SerialUSB);

  FeedWatchdog(); // nom nom...

  // Setup the communications array.
  if (not _comms.Initialise()) {
    Log::Error("Leonidas: Could not initialise communications.");
    Die();
  }

  // Setup comms logging.
  Log::comms(&_comms);
  Log::Info("Leonidas: Communications initialised!");

  FeedWatchdog(); // nom nom...

  ///
  /// Common setup. This should be done before everything else.
  ///
  // Setup debug stuff.
  Log::Info("Leonidas: Initialising...");

  // Setup power.
  if (not PowerManager::Initialise()) {
    Log::Error("Leonidas: Could not initialise the power manager.");
    Die();
  }

  FeedWatchdog(); // nom nom...

  // Start the wire module.
  #if defined (__AVR__)
    Wire.begin();
  #elif defined (__arm__)
    Wire1.begin();
  #endif

  // Use the 12-bit anlog input available on the arm.
  #if defined (__arm__)
    analogReadResolution(12);
  #endif

  // Setup SPI.
  pinMode(kSPI_CS, OUTPUT);
  pinMode(kSD_CS, OUTPUT);

  // Setup the SD card.
  // Note: this _must_ be done before SPI.begin() otherwise the ArduCam module
  // _will not work_. Don't ask me why.
  if (!SD.begin(kSD_CS)) {
    Log::Error("Leonidas: Could not begin SD Card subsystem.");
    Die();
  } else {
    Log::Info("Leonidas: SD card initialised!");
  }

  FeedWatchdog(); // nom nom...

  SPI.begin();

  // Setup the selfiecam.
  if (not _camera.Initialise()) {
    Log::Error("Leonidas: Could not initialise the Selfie Camera.");
    Die();
  }

  FeedWatchdog(); // nom nom...

  // Enrich logging a little bit. Note that this will slow
  // down logging quite a bit and should only be done when
  // serial is difficult to access.
  Log::file("leonidas.log");
  Log::display(_camera.display());
  Log::Info("Leonidas: Log Initialised!");

  // Setup the ADCS.
  if (not _adcs.Initialise()) {
    Log::Error("Leonidas: Could not initialise ADCS.");
    Die();
  }

  _adcs.Enable(0, 0, 0);
  FeedWatchdog(); // nom nom...

  // Setup the GPS.
  _gps.SetTimezone(GPS::AEDT);
  if (not _gps.Initialise()) {
    Log::Error("Leonidas: Could not initialise the GPS.");
    Die();
  }

  FeedWatchdog(); // nom nom...

  // Setup the RTC.
  if (not LRTC::Initialise()) {
    Log::Error("Leonidas: Could not initialise the RTC.");
    Die();
  }

  FeedWatchdog(); // nom nom...

  Log::Info("Leonidas: Initialised!");
}

int adcs_timer = millis();
void Leonidas::Run() {
  // Nom nom nom...
  FeedWatchdog();

  // Most things probably shouldn't operate in safe mode.
  if (!_in_safe_mode) {
    // Update the RTC with a GPS signal.
    LRTC::Update(_gps);
  }

  // Process any commands we may have received. We are assuming we can do this
  // fast enough so that we don't receive multiple commands without being able
  // to process them.
  if (!_commands->empty() && Command::IsValid(_commands)) {
    // Parse the command. Pause RX while we do this.
    _comms.StopRX();
    Command command = Command::Parse(_commands);
    _comms.StartRX();

    // Just print it for now.
    Log::Debug("Command received: %d", command.id());

    // SET command.
    switch (command.id()) {
      case Command::SET:
        Set(command.data()->args.SET.key, command.data()->args.SET.value);
        break;

      // GET command.
      case Command::GET:
        Get(command.data()->args.GET.key);
        break;

      // LIST command.
      case Command::LIST:
        _comms.SendFileListing();
        break;

      // REMOVE command.
      case Command::REMOVE:
        Remove(command.data()->args.REMOVE.filename);
        break;

      // DOWNLOAD command.
      case Command::DOWNLOAD:
      case Command::DOWNLOAD_BLOCK:
        Download(command);
        break;

      // UPLOAD command.
      case Command::UPLOAD:
        Upload(command);
        break;

      // SELFIE command.
      case Command::SELFIE:
        Selfie();
        break;

      // RESET command.
      case Command::RESET:
        ResetSatellite();
        while (true); // shouldn't get here...
        break;

      // TELEMETERY command.
      case Command::TELEMETERY:
        _adcs.Disable();
        if (command.data()->args.TELEMETERY.use_sun) {
          _adcs.Enable();
        } else {
          _adcs.Enable(
            command.data()->args.TELEMETERY.x
            , command.data()->args.TELEMETERY.y
            , command.data()->args.TELEMETERY.z);
        }
        break;

      // Invalid command.
      default: {
        _comms.NACK("?", "UNKNOWN-COMMAND");
      }
    }
  }
}

void Leonidas::Set(const char* key, const char* value) {
  // Parameter: log level.
  if (Equal(key, "LOG-LEVEL")) {
    if (Equal(value, "DEBUG")) {
      Log::max_level(Log::DEBUG);
    } else if (Equal(value, "INFO")) {
      Log::max_level(Log::INFO);
    } else if (Equal(value, "ERROR")) {
      Log::max_level(Log::ERROR);
    } else {
      _comms.NACK("SET", "INVALID-ARGUMENT");
      return;
    }
  }

  // Parameter: display logging.
  else if (Equal(key, "DISPLAY-LOG")) {
    if (Equal(value, "ON")) {
      Log::display(_camera.display());
    } else if (Equal(value, "OFF")) {
      Log::display(NULL);
    } else {
      _comms.NACK("SET", "INVALID-ARGUMENT");
      return;
    }
  }

  // Paramter: battery switch.
  else if (Equal(key, "BATTERY")) {
    if (Equal(value, "1")) {
      PowerManager::SetBattery(PowerManager::BATTERY_1);
    } else if (Equal(value, "2")) {
      PowerManager::SetBattery(PowerManager::BATTERY_2);
    } else {
      _comms.NACK("SET", "INVALID-ARGUMENT");
      return;
    }
  }

  // Paramter: enable/disable comms TX
  else if (Equal(key, "TX")) {
    if (Equal(value, "ON")) {
      _comms.StartTX();
      Log::Info("TX ON");
    } else if (Equal(value, "OFF")) {
      _comms.StopTX();
      Log::Info("TX OFF");
    } else {
      _comms.NACK("SET", "INVALID-ARGUMENT");
      return;
    }
  }

  // Parameter: current satellite mode (SAFE or NORMAL).
  else if (Equal(key, "MODE")) {
    if (Equal(value, "SAFE")) {
      Log::Info("Entering SAFE mode.");
      _in_safe_mode = true;
      // TODO(jeshua): Actually turn off unnecessary stuff.
    } else if (Equal(value, "NORMAL")) {
      Log::Info("Entering NORMAL mode.");
      _in_safe_mode = false;
    } else {
      _comms.NACK("SET", "INVALID-ARGUMENT");
      return;
    }
  }

  // Unknown parameter.
  else {
    _comms.NACK("SET", "UNKNOWN-PARAMETER");
    return;
  }

  // If we got here, the SET operation must have succeeded.
  _comms.ACK("SET");
}

void Leonidas::Get(const char* key) {
  // Parameter: LOG-LEVEL.
  if (Equal(key, "LOG-LEVEL")) {
    if (Log::max_level() == Log::DEBUG) {
      _comms.Send("GET LOG-LEVEL", "DEBUG");
    } else if (Log::max_level() == Log::INFO) {
      _comms.Send("GET LOG-LEVEL", "INFO");
    } else if (Log::max_level() == Log::ERROR) {
      _comms.Send("GET LOG-LEVEL", "ERROR");
    }
  }

  // Paramter: POWER-USAGE
  else if (Equal(key, "POWER-USAGE")) {
    // Send power usage data.
    Log::Info("B1=%.2fV, %.2fA; B2=%.2fV, %.2fA; 3.3v=%.2fA; 5v=%.2fA"
      , PowerManager::BatteryVoltage(PowerManager::BATTERY_1)
      , PowerManager::BatteryCurrent(PowerManager::BATTERY_1)
      , PowerManager::BatteryVoltage(PowerManager::BATTERY_2)
      , PowerManager::BatteryCurrent(PowerManager::BATTERY_2)
      , PowerManager::BusCurrent(PowerManager::B33V)
      , PowerManager::BusCurrent(PowerManager::B5V));
  }

  // Parameter: TELEMETRY
  else if (Equal(key, "TELEMETRY")) {
    // Send telemetry data.
    IMU* imu = _adcs.imu();
    sensors_vec_t o = imu->Orientation();
    double roll = o.roll, pitch = o.pitch, yaw = o.heading;
    const GPS::Location* loc = _gps.location();
    Log::Info("roll=%.2f, pitch=%.2f, yaw=%.2f, lat=%.5f, lng=%.5f, alt=%dm"
      , roll, pitch, yaw, loc->latitude, loc->longitude, loc->altitude);

    // Send temperture + pressure information.
    Log::Info("temp=%.2fC, pressure=%.2fkPa", imu->Temperature(), imu->Pressure());

    // Send power information.
    Log::Info("B1=%.2fV, %.2fA; B2=%.2fV, %.2fA; 3.3v=%.2fA; 5v=%.2fA"
      , PowerManager::BatteryVoltage(PowerManager::BATTERY_1)
      , PowerManager::BatteryCurrent(PowerManager::BATTERY_1)
      , PowerManager::BatteryVoltage(PowerManager::BATTERY_2)
      , PowerManager::BatteryCurrent(PowerManager::BATTERY_2)
      , PowerManager::BusCurrent(PowerManager::B33V)
      , PowerManager::BusCurrent(PowerManager::B5V));

    // Send the time.
    LRTC::DateTime t = LRTC::Get();
    Log::Info("Time=%02d/%02d/%04d %02d:%02d:%02d"
      , t.mday, t.mon, t.year, t.hour, t.min, t.sec);
  }

  // Invalid parameter.
  else {
    _comms.NACK("GET", "UNKNOWN-PARAMETER");
  }
}

void Leonidas::Remove(const char* filename) {
  SD.remove(const_cast<char*>(filename));
  _comms.ACK("REMOVE");
}

void Leonidas::Download(const Command& command) {
  // Extract the filename.
  const char* filename = command.data()->args.DOWNLOAD.filename;

  // Special cases.
  if (Equal(filename, "LOG")) {
    // Copy the current state of the log file (to avoid infinite downloads).
    if (command == Command::DOWNLOAD) {
      if (!SDCopy("leonidas.log", "log.txt")) {
        Log::Debug("Leonidas: Could not copy log file for transmit.");
      }
    }

    filename = "log.txt";
  }

  // Check if the file exists.
  if (!SD.exists(const_cast<char*>(filename))) {
    _comms.NACK("DOWNLOAD", "FILE-NOT-FOUND");
    return;
  }

  // Extract the block number (-1 to get index).
  int block = 0;
  if (command == Command::DOWNLOAD_BLOCK) {
    block = command.data()->args.DOWNLOAD_BLOCK.block;
  }

  // Transmit the file.
  if (!_comms.TransmitBlock(filename, block)) {
    _comms.NACK("DOWNLOAD", "INVALID-BLOCK");
  }
}

void Leonidas::Upload(const Command& command) {
  const char* filename = command.data()->args.UPLOAD.filename;

  // Get the block index and size.
  const char* block_data = command.data()->args.UPLOAD.block_data;
  int block_start = command.data()->args.UPLOAD.block_start;
  int block_length = command.data()->args.UPLOAD.block_length;

  // Receive the file.
  File file = SD.open(filename, FILE_WRITE);
  if (!file) {
    Log::Info("Could not open file %s.", filename);
    _comms.NACK("UPLOAD", "INVALID-FILE");
    return;
  }

  // If the file isn't the right size, we got sent the wrong block.
  if (file.size() != block_start) {
    Log::Debug("Invalid block start %d.", block_start);
    file.close();
    _comms.NACK("UPLOAD", "INVALID-START");
    return;
  }

  // Load the correct location in the file.
  file.seek(block_start);

  // Get the block data.
  for (int i = 0; i < block_length; i++) {
    file.write(block_data[i]);
  }

  // Clean up the file.
  file.close();

  // Ack.
  _comms.ACK("UPLOAD");
}

void Leonidas::Selfie() {
  // Send an ACK now; this might take a while.
  _comms.ACK("SELFIE");

  // Go through each files on the SD card. If the file ends in .bmp then
  // it is a file we need to take a selfie of. In this case (assuming
  // there is no .jpg file already), load the picture onto the screen and
  // take a photo.
  File root = SD.open("/");
  root.rewindDirectory();
  while (File f = root.openNextFile()) {
    // Check if we have a JPG file.
    char* filename = f.name();
    char* extension = strstr(filename, ".BMP");
    if (extension != NULL) {
      char output_filename[32];

      // Save the filename.
      memset(output_filename, '\0', sizeof(output_filename));
      memcpy(output_filename, filename, (extension - filename));
      strcat(output_filename, ".JPG");

      // If the output filename doesn't exist... time to selfie!
      if (!SD.exists(output_filename)) {
        Log::Info("Taking selfie of {%s} into {%s}", filename, output_filename);
        _camera.Enable();
        _camera.TakeSelfie(filename, output_filename);
        _camera.Disable();
      }
    }
  }
}
