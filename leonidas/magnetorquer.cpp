#include "magnetorquer.h"

Magnetorquer::Magnetorquer(int pin, int ain1, int ain2)
  : _pin(pin), _ain1(ain1), _ain2(ain2), _direction(1), _initialised(false) {

}

Magnetorquer::~Magnetorquer() {

}

bool Magnetorquer::Initialise() {
  pinMode(_pin, OUTPUT);
  pinMode(_ain1, OUTPUT);
  pinMode(_ain2, OUTPUT);
  analogWrite(_pin, 0);
  digitalWrite(_ain1, LOW);
  digitalWrite(_ain2, LOW);
  Log::Debug("Magnetorquer: %d initialised!", _pin);
  return true;
}

void Magnetorquer::ReverseDirection() {
  _direction *= -1;
}

void Magnetorquer::SetOutput(int level) {
  _level = level;
  level = _direction * level;
  if (level > 0) {
    digitalWrite(_ain1, HIGH);
    digitalWrite(_ain2, LOW);
  } else if (level <= 0) {
    digitalWrite(_ain1, LOW);
    digitalWrite(_ain2, HIGH);
  }

  analogWrite(_pin, abs(level));
}
