#ifndef _SUNSENSOR_H_
#define _SUNSENSOR_H_

#include <Arduino.h>

///
/// Interface for interacting the a TSL250R sensor.
///
class SunSensor {
 public:
  // Supply voltage level in volts. Can in the range [2.2, 5] volts.
  static const double kSupplyVoltageLevel = 5.0;

  ///
  /// Constructor + Destructor.
  ///
  SunSensor(int pin, int x, int y, int z);
  ~SunSensor();

  ///
  /// Get the current voltage output of the sensor. The returned value will be
  /// in the range [0, kSupplyVoltageLevel].
  ///
  double Value() const;

  ///
  /// Get a normalised value from the sensor, in the range [0, 1].
  ///
  double ValueNormalised() const;

  ///
  /// Calculate the angle reported by the sun sensor using the normalized
  /// voltage.
  ///
  double Theta() const;

  ///
  /// Find the angle between two sun sensors.
  ///
  static void FindSun(
    const SunSensor& a, const SunSensor& b//, const SunSensor& c
    , double* sun_vector);

 private:
  int _pin;
  int _direction_vector[3];

  ///
  /// Utility method: given a sun sensor, populate the appropriate element in
  /// the `sun_sensor` array wit the calculated angle.
  ///
  static void CalculateSunVectorComponent(const SunSensor& a, double* sun_vector);
};

#endif  // _SUNSENSOR_H_
