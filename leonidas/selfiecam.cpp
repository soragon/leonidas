#include "selfiecam.h"

SelfieCam::SelfieCam(const int spi_cs)
  : _camera(OV2640, spi_cs), _display(spi_cs), _initialised(false) {

}

SelfieCam::~SelfieCam() {

}

bool SelfieCam::Initialise() {
  // Verify the ArduCAM SPI bus.
  _camera.write_reg(ARDUCHIP_TEST1, 0x55);
  if (_camera.read_reg(ARDUCHIP_TEST1) != 0x55) {
    Log::Error("SelfieCam: SPI not initialised.");
    return false;
  }

  // Setup the display.
  _display.InitLCD();

  // Verify we have the correct camera.
  uint8_t vid = 0, pid = 0;
  _camera.write_reg(ARDUCHIP_MODE, 0x00);
  _camera.rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
  _camera.rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);

  if ((vid != 0x26) || (pid != 0x42)) {
    Log::Error("SelfieCam: Couldn't find OV2640 Camera Module!");
    return false;
  }

  Log::Debug("SelfieCam: OV2640 Detected!");

  // Finish the camera setup.
  _camera.set_format(JPEG);
  _camera.InitCAM();
  _camera.OV2640_set_JPEG_size(OV2640_640x480); // --> 17 KB

  // Turn off the screen by default.
  Disable();

  Log::Info("SelfieCam: Initialised!");
  _initialised = true;
  return true;
}

bool SelfieCam::CaptureImage(const char* filename) {
  if (!_initialised) {
    return false;
  }

  // Validate the output filename.
  if (filename == NULL) {
    Log::Error("SelfieCam: Enter a valid filename for the output.");
    return false;
  }

  // Start capturing.
  Log::Debug("SelfieCam: Starting capture...");
  _camera.flush_fifo();
  _camera.clear_fifo_flag();
  _camera.start_capture();

  // Wait until the image has been captured.
  // TODO(jeshua): Make this async?
  while (!(_camera.read_reg(ARDUCHIP_TRIG) & CAP_DONE_MASK)) {
    // Do nothing...
  }

  Log::Debug("SelfieCam: Capture done!");

  // Open the output file.
  Log::Debug("SelfieCam: Saving output to file...");
  File output_file = SD.open(filename, FILE_WRITE);
  if (!output_file) {
    Log::Error("SelfieCam: Couldn't open output file!");
    return false;
  }

  // Keep looping until we see 0xFFD9 (END of JPG).
  uint8_t byte = 0, prev_byte = 0;
  while ((prev_byte != 0xFF) || (byte != 0xD9)) {
    prev_byte = byte;
    byte = _camera.read_fifo();
    output_file.write(byte);
  }

  output_file.close();
  _camera.clear_fifo_flag();
  _camera.flush_fifo();
  Log::Debug("SelfieCam: File saved!");

  return true;
}

bool SelfieCam::DisplayImage(const char* filename) {
  if (!_initialised) {
    return false;
  }

  // Validate the input filename.
  if (filename == NULL) {
    Log::Error("SelfieCam: Cannot open a NULL image file!");
    return false;
  }

  // Open the input file from the SD card in READ mode.
  File input = SD.open(filename, FILE_READ);

  // `input` will evaluate to `false` if the file is invalid.
  if (!input) {
    Log::Error("SelfieCam: Could not open file to display!");
    return false;
  }

  // Setup the display (clear the screen and reset it's drawing pixel).
  _display.clrScr();
  _display.resetXY();

  // Draw the bitmap image.
  _display.dispBitmap(input);

  // Clean up the input file.
  input.close();

  return true;
}

bool SelfieCam::TakeSelfie(const char* input, const char* output) {
  return DisplayImage(input) && CaptureImage(output);
}

void SelfieCam::Disable() {
  // Turn off the screen.
  _display.clrScr();
  _display.lcdOff();
}

void SelfieCam::Enable() {
  // Turn on the screen.
  _display.lcdOn();
}
