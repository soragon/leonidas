#ifndef _TRANSMITTER_H_
#define _TRANSMITTER_H_

#include "buffer.h"

class Transmitter {
 public:
  static const int kMaxBufferLength = 4096;
  typedef Buffer<kMaxBufferLength, char> CharacterBuffer;

  ///
  /// Constructor + Destructor.
  ///
  Transmitter(CharacterBuffer* tx, CharacterBuffer* rx);
  virtual ~Transmitter();

  ///
  /// Setup the transmitter. This should setup all required timer interrupts to
  /// read from/write to the provided buffers.
  ///
  virtual bool Initialise() = 0;

  ///
  /// Temporarily stop/start TX/RX.
  ///
  virtual void StopRX() = 0;
  virtual void StopTX() = 0;
  virtual void StartRX() = 0;
  virtual void StartTX() = 0;

 protected:
  CharacterBuffer* _tx;
  CharacterBuffer* _rx;
};

#endif  // _TRANSMITTER_H_
