#ifndef _LOG_H_
#define _LOG_H_

#include <stdarg.h>

#include <Arduino.h>
#include <SD.h>
#include <UTFT_SPI.h>

// Forward declare this as to not interfere with anything.
class Communication;

// This module is a bit big, and won't always be necessary. So, I've
// included a way to compile in just certain parts of it. Just comment
// out the modules you don't want and you could save some memory!
// You can save about 1% by disabling screen and file logging.
#define LOG_SERIAL_ENABLE
#define LOG_COMMS_ENABLE
//#define LOG_SCREEN_ENABLE
#define LOG_FILE_ENABLE

class Log {
 public:
  enum Level {
    DEBUG=0, INFO=1, ERROR=2
  };

  ///
  /// Setup the logging module. Will return false if setup fails.
  ///
  static bool Initialise();

  ///
  /// Log a message to both serial and the display with a
  /// particular log level.
  ///
  static void Debug(const char* fmt, ...);
  static void Info(const char* fmt, ...);
  static void Error(const char* fmt, ...);

  ///
  /// Log a message over serial.
  ///
  static void Serial(Level level, const char* message);

  ///
  /// Log a message over the given communication medium.
  ///
  static void Comms(Level level, const char* message);

  ///
  /// Log a message to the display.
  ///
  static void Screen(Level level, const char* message);

  ///
  /// Log a message to the opened file. If no file has been
  /// created, nothing will be done.
  ///
  static void File(Level level, const char* message);

  // Setter methods.
  static void serial(Stream* serial);
  static void comms(Communication* comms);
  static void display(UTFT* display);
  static void file(const char* filename, bool overwrite=false);
  static void max_level(Level max_level) { _max_level = max_level; }
  static Level max_level() { return _max_level; }

 private:
  // Maximum level that will be logged.
  static Level _max_level;

#ifdef LOG_SERIAL_ENABLE
  // Serial stream to write to. It must be initialised to work properly.
  static Stream* _serial;
#endif

#ifdef LOG_COMMS_ENABLE
  // Communication device to write log to.
  static Communication* _comms;
#endif

#ifdef LOG_SCREEN_ENABLE
  // Display device to write to. Can be set to NULL to disable
  // display writing.
  static UTFT* _display;

  // Current line we are displaying information on.
  static int _current_display_line;
#endif

#ifdef LOG_FILE_ENABLE
  // File we are writing too.
  static ::File _file;
  static char* _filename;
#endif

  /// Buffers to use for storing format strings.
  // Final format string (containing common log info).
  static char _format_buffer[];
  static const int kFormatBufferSize = 512; // bytes

  // Format string provided by the user.
  static char _user_format_buffer[];
  static const int kUserFormatBufferSize = 512; // bytes

  // Pin with the LED attached.
  static const int kLED = 13;

  // True if the IMU has been initialised.
  static bool _initialised;

  // Utility method: format the given format string and arguments
  // into the provided buffer (up to a maximum size).
  static void FormatInto(
      char* buffer, int size, const char* fmt, va_list args);

  // Utility method: build up the system format string from the
  // provided message. Will return the buffer containing the formatted
  // message. The system format string will have this format:
  //     [LEVEL] %s
  static const char* FormatHeader(Level level, const char* message);

  // Utility method: log the given message to all channels with
  // a particular level.
  static void LogToAll(Level level, const char* fmt, va_list args);

  // Given a level, convert it to a string.
  static const char* level_to_string(Level level);
};

#endif
