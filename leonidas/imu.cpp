#include "imu.h"

IMU::IMU()
  : _accelerometer(30301), _magnetometer(30302), _barometer(18001)
  , _ahrs(&_accelerometer, &_magnetometer)
  , _sea_level_pressure(SENSORS_PRESSURE_SEALEVELHPA), _uv()
  , _initialised(false) {

}

IMU::~IMU() {

}

bool IMU::Initialise() {
  // Initialize the IMU.
  // TODO(michel): need to check if initialised.
  if (!_accelerometer.begin()) {
    Log::Error("IMU: Could not detect accelerometer.");
    return false;
  }

  if (!_magnetometer.begin()) {
    Log::Error("IMU: Could not detect magnetometer.");
    return false;
  }

  if (!_barometer.begin()) {
    Log::Error("IMU: Could not detect barometer.");
    return false;
  }

  // // Initialize the UV sensor
  // if (!_uv.begin()) {
  //   Log::Error("IMU: Could not detect UV sensor.");
  //   return false;
  // }

  Log::Info("IMU: Initialised!");
  _initialised = true;
  return true;
}


sensors_vec_t IMU::Orientation() {
  if (!_initialised) {
    return sensors_vec_t();
  }

  sensors_vec_t orientation;
  _ahrs.getOrientation(&orientation);
  return orientation;
}

double IMU::Temperature() {
  float temperature;
  _barometer.getTemperature(&temperature);
  return temperature;
}

double IMU::Pressure() {
  float pressure;
  _barometer.getPressure(&pressure);
  return pressure;
}

int IMU::ReadUV() {
  if (!_initialised) {
    return -1;
  }

  return _uv.readUV();
}

int IMU::ReadIR() {
  if (!_initialised) {
    return -1;
  }

  return _uv.readIR();
}

int IMU::ReadVisible() {
  if (!_initialised) {
    return -1;
  }

  return _uv.readVisible();
}

int IMU::ReadProx() {
  if (!_initialised) {
    return -1;
  }

  return _uv.readProx();
}
