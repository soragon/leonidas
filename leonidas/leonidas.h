#ifndef _LEONIDAS_H_
#define _LEONIDAS_H_

#include "adcs.h"
#include "buffer.h"
#include "command.h"
#include "communication.h"
#include "gps.h"
#include "imu.h"
#include "powermanager.h"
#include "rtc.h"
#include "selfiecam.h"
#include "sunsensor.h"
#include "utility.h"

class Leonidas {
 public:
  static const int kSD_CS = 9;
  static const int kSPI_CS = 10;
  static const int kWatchdogTimerTimeout = 10; // seconds

  Leonidas();
  ~Leonidas();

  void Initialise();
  void Run();

 private:
  SelfieCam _camera;
  GPS _gps;
  ADCS _adcs;
  Communication _comms;

  // Command flags.
  Transmitter::CharacterBuffer* _commands;

  // True if we are in safe mode.
  bool _in_safe_mode;

  // Watchdog timer.
  DueTimer* _watchdog_timer;

  // Utility function: process the setting of a parameter.
  void Set(const char* key, const char* value);

  // Utility function: process the getting of a parameter.
  void Get(const char* key);

  // Utility function: remove the requested file.
  void Remove(const char* filename);

  // Utility function: process a download request.
  void Download(const Command& command);

  // Utility function: process an upload request.
  void Upload(const Command& command);

  // Utility function: process a selfie request.
  void Selfie();

  // System function: feed the angry watchdog.
  void FeedWatchdog();
};

#endif  // _LEONIDAS_H_
