#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

#include <DueTimer.h>
#include <SD.h>

#include "log.h"
#include "serialtransmitter.h"

///
/// Communication controller. Does all of the parsing of commands and handles TX
/// and RX buffers. Communicates with the main system (Leonidas).
///
class Communication {
 public:
  ///
  /// AX.25 Frame Data
  /// See: https://www.tapr.org/pub_ax25.html
  ///
  struct AX25 {
    // Flag bit at the start and end of the frame
    static const char kFlag = 0b01111110;

    // Address information.
    static const char kDestinationAddress[];
    static const char kSourceAddress[];

    // The destination SSID changes based on transmission of science or
    // navigation data.
    static const char kSciencePackage = 0b00001111;
    static const char kOrbitPackage = 0b00001110;
    static const char kSourceSSID = 0b01100000;

    // Control frames indicating UI frames and responses.
    static const char kControlNeedAck = 0b00010011;
    static const char kControlNoAck = 0b00000011;

    // PID bit indicating no Layer 3 protocol is used
    static const char kPid = 0xF0;

    // Length of the information packet.
    static const int kInfoLength = 512;

    // Overall length of the whole frame.
    // FLAG + ADDRESSES + CONTROL + PID + INFO + FCS + FLAG.
    static const int kFrameLength = 1 + 14 + 1 + 1 + kInfoLength + 2 + 1;
  };

  // Command terminating character, used to parse commands.
  static const char kTerminatingCharacter = '\n';

  // 512 Ax25 Info length - 64 string format length - 2 for space
  static const int kBlockSize = AX25::kInfoLength - 64 - 2;

  // Amount of time between pings.
  static const int kBeaconTransmitFrequency = 10; // seconds

  ///
  /// Constructor + Destructor.
  ///
  Communication(DueTimer* beacon_timer=&Timer5);
  ~Communication();

  ///
  /// Setup the communication module.
  ///
  bool Initialise();

  ///
  /// Pause RX/TX for a while (to stop race conditions).
  ///
  void StopRX() { _transmitter.StopRX(); _rx_on = false; }
  void StopTX() { _transmitter.StopTX(); _tx_on = false; }
  void StartRX() { _transmitter.StartRX(); _rx_on = true; }
  void StartTX() { _transmitter.StartTX(); _tx_on = true; }

  ///
  /// Send a logging message over comms.
  ///
  void LogMessage(const char* message);

  ///
  /// Send a listing of all files on the SD card to the server.
  ///
  void SendFileListing();

  ///
  /// Command ACK/NACKs.
  ///
  void ACK(const char* command);
  void NACK(const char* command, const char* reason);

  ///
  /// Send a block of data in the form "<command> <parameter>\n".
  ///
  void Send(const char* command, const char* parameter);

  ///
  /// Send the given block of the given file. Will return `true` if the data
  /// could be sent, `false` otherwise.
  ///
  bool TransmitBlock(const char* filename, int block);

  ///
  /// Send a beacon signal to the ground station.
  ///
  void SendBeacon();

  ///
  /// Get access to the TX/RX buffers directly.
  ///
  Transmitter::CharacterBuffer* rx() { return &_rx; }
  Transmitter::CharacterBuffer* tx() { return &_tx; }


 private:
  Transmitter::CharacterBuffer _tx, _rx;
  bool _tx_on, _rx_on;
  SerialTransmitter _transmitter;
  DueTimer* _beacon_timer;

  //
  // Add a given simple command to the transmit buffer. This will not add any
  // data and should only be used for basic commands (i.e. not sending blocks).
  //
  void AddCommandToTransmitBuffer(
    const char* command, bool science_data=false, bool expect_ack=false);

  //
  // Pad the transmission buffer with as many zeroes as requested.
  //
  void PadZeroes(int count);

  //
  // Writ the AX25 header to the transmit buffer.
  //
  void AttachAX25Header(bool science_data=false, bool expect_ack=false);

  //
  // Write the AX25 tail to the transmit buffer.
  //
  void AttachAX25Tail();

  //
  // Get the created instance of communication.
  //
  static Communication* instance();
  static Communication* _instance;

  // Friend function to send the ping beacon.
  friend void _communication_send_beacon();
};

#endif  // _COMMUNICATION_H_
