#include "rtc.h"

bool LRTC::Initialise() {
  Log::Info("RTC: Initialised!");
  return true;
}

void LRTC::Update(GPS& gps) {
  const GPS::Location* location = gps.location();

  // Only update if we have a fix.
  if (location->fix) {
    DateTime t;
    t.sec = location->second;
    t.min = location->minute;
    t.hour = location->hour;
    t.mon = location->month;
    t.year = location->year + 2000;
    t.mday = location->day;
    DS3231_set(t);
  }
}

LRTC::DateTime LRTC::Get() {
  DateTime t;
  DS3231_get(&t);
  return t;
}
