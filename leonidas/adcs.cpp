#include "adcs.h"

ADCS* ADCS::_instance = NULL;
const double ADCS::kP[ADCS::NUM_DIRECTIONS] = { 100,  100,  100};
const double ADCS::kI[ADCS::NUM_DIRECTIONS] = {   0,    0,    0};
const double ADCS::kD[ADCS::NUM_DIRECTIONS] = {7000, 7000, 7000};

const double MAGNETIC_FIELD_LINE1 = (90.0 / 180.0) * PI;
const double MAGNETIC_FIELD_LINE2 = (270.0 / 180.0) * PI;

void _adcs_control_interrupt() {
  ADCS* _adcs = ADCS::_instance;
  if (_adcs != NULL) {
    _adcs->Control();
  }
}

ADCS::ADCS(DueTimer* timer)
  : _imu()
  , _sun_sensors({
    SunSensor(A0, 0, 1, 0),
    SunSensor(A1, -1, 0, 0),
    SunSensor(A2, 0, -1, 0),
    SunSensor(A3, 1, 0, 0),
    SunSensor(A4, 0, 0, 1)
  })
  , _magnetorquers({{2, 48, 49}, {5, 52, 53}, {3, 50, 51}})
  , _timer(timer), _initialised(false), _use_sun(false)
  , _inputs({0, 0, 0}), _prev_inputs({0, 0, 0}), _outputs({0, 0, 0})
  , _setpoints({(90.0 / 180.0) * PI, (90.0 / 180.0) * PI, (90.0 / 180.0) * PI})
  , _controllers(
    {PID(&_inputs[0], &_outputs[0], &_setpoints[0], kP[0], kI[0], kD[0], DIRECT)
   , PID(&_inputs[1], &_outputs[1], &_setpoints[1], kP[1], kI[1], kD[1], DIRECT)
   , PID(&_inputs[2], &_outputs[2], &_setpoints[2], kP[2], kI[2], kD[2], DIRECT)
  }) {
  _instance = this;

  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    _controllers[i].SetSampleTime(kRefreshRate);
    _controllers[i].SetOutputLimits(-255, 255);
    _controllers[i].SetMode(AUTOMATIC);
  }
}

ADCS::~ADCS() {

}

bool ADCS::Initialise() {
  // // Setup the IMU.
  // if (not _imu.Initialise()) {
  //   Log::Error("ADCS: Could not initialise the IMU or UV sensor.");
  //   return false;
  // }

  // // TODO(jeshuam): Initialise the Sun Sensors.

  // // Setup the magnetorquers.
  // for (int i = 0; i < NUM_DIRECTIONS; i++) {
  //   if (not _magnetorquers[i].Initialise()) {
  //     Log::Error("ADCS: Could not initialise magnetorquer %d.", i);
  //     return false;
  //   }
  // }

  // Setup the timer, but don't start it yet.
  _timer->attachInterrupt(_adcs_control_interrupt);

  Log::Info("ADCS: Initialised!");
  _initialised = true;
  return true;
}

void ADCS::Enable(int x, int y, int z) {
  if (!_initialised) {
    return;
  }

  // Set setpoint values.
  _use_sun = false;
  _setpoints[X] = x;
  _setpoints[Y] = y;
  _setpoints[Z] = z;

  // Enable all controllers.
  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    _controllers[i].SetMode(AUTOMATIC);
  }

  // Start updates.
  _timer->setPeriod(kRefreshRate * 1000).start();
}

void ADCS::Enable() {
  if (!_initialised) {
    return;
  }

  // Enable sun control.
  _use_sun = true;
  _setpoints[X] = 0;
  _setpoints[Y] = 0;
  _setpoints[Z] = 0;

  // Enable all controllers.
  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    _controllers[i].SetMode(AUTOMATIC);
  }

  // Start updates.
  _timer->setPeriod(kRefreshRate * 1000).start();
}

void ADCS::Disable() {
  if (!_initialised) {
    return;
  }

  // Stop future updates.
  _timer->stop();

  // Also disable all controllers.
  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    _controllers[i].SetMode(MANUAL);
  }
}

bool ADCS::GoalReached() {
  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    if (_controllers[i].GetMode() == AUTOMATIC) {
      return false;
    }
  }

  return true;
}

//int last_temp = 0;
void ADCS::Control() {
  if (!_initialised) {
    return;
  }

  // Find the sun if necessary.
  if (_use_sun) {
    // Largest X direction sensor.
    SunSensor* max_x = &_sun_sensors[1];
    if (_sun_sensors[3].Value() > max_x->Value()) {
      max_x = &_sun_sensors[3];
    }

    // Largest Y direction sensor.
    SunSensor* max_y = &_sun_sensors[0];
    if (_sun_sensors[2].Value() > max_x->Value()) {
      max_y = &_sun_sensors[2];
    }

    // Largest Z direction sensor.
    SunSensor* max_z = &_sun_sensors[4];

    // Find the sun!
    double sun_vector[3];
    SunSensor::FindSun(*max_x, *max_y, sun_vector);

    // Set the setpoints.
    _setpoints[0] = sun_vector[0];
    _setpoints[1] = sun_vector[1];
    _setpoints[2] = sun_vector[2];
  }

  // Get the new inputs.
  sensors_vec_t orientation;
  GetAverageResults(5, 2, &orientation);

  // Compute each controller.
  for (int i = 0; i < NUM_DIRECTIONS; i++) {
    // Set input.
    _prev_inputs[i] = _inputs[i];
    _inputs[i] = round(((orientation.v[i] / 180.0) * PI) * 10.0) / 10.0;

    // Compute.
    _controllers[i].Compute();

    // If the input crossed the magnetic field line, invert the direction.
    double last = _prev_inputs[i];
    double curr = _inputs[i];

    if ((last < MAGNETIC_FIELD_LINE1 and curr > MAGNETIC_FIELD_LINE1) or
        (last > MAGNETIC_FIELD_LINE1 and curr < MAGNETIC_FIELD_LINE1) or
        (last < MAGNETIC_FIELD_LINE2 and curr > MAGNETIC_FIELD_LINE2) or
        (last > MAGNETIC_FIELD_LINE2 and curr < MAGNETIC_FIELD_LINE2)) {
      _magnetorquers[i].ReverseDirection();
    }

    _magnetorquers[i].SetOutput(_outputs[i]);

    // This can be used to test a magnetorquer (useful for checking wiring).
    // // Read output.
    // if (millis() - last_temp > 5000) {
    //   if (_magnetorquers[2].GetLevel() < 0) {
    //     _magnetorquers[2].SetOutput(255);
    //   } else {
    //     _magnetorquers[2].SetOutput(-255);
    //   }

    //   Log::Info("Setting to %d", _magnetorquers[2].GetLevel());
    //   last_temp = millis();
    // }
  }
}

void ADCS::GetAverageResults(int n, int ms, sensors_vec_t *average) {
  average->x = 0;
  average->y = 0;
  average->z = 0;

  for (int i = 0; i < n; i++) {
    sensors_vec_t orientation = _imu.Orientation();

    average->x = orientation.x;
    average->y = orientation.y;
    average->z = orientation.z;
    delayMicroseconds(ms * 1000);
  }

  // Actually do the average.
  average->x /= n;
  average->y /= n;
  average->z /= n;
}
