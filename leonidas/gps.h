#ifndef _GPS_H_
#define _GPS_H_

#include <Adafruit_GPS.h>
#include <DueTimer.h>

#include "log.h"

class GPS {
public:
  // Amount of time to wait between trying to read GPS bytes. This can be made
  // faster if the GPS module is failing to read many sentences.
  static const int kReadDelay = 1; // ms

  ///
  /// Struct to encode the current location returned by the GPS. We should use
  /// this instead of the Adafruit one as we only have to store what we need
  /// (and we know the units of everything).
  ///
  struct Location {
    // Time information; what time was this reading taken?
    // Can be converted to different timezones.
    int hour, minute, second, year, month, day, milliseconds;

    // Location information.
    float latitude, longitude; // decimal degrees
    float altitude; // meters above sea level

    // Other information.
    float speed; // m/s
    float angle; // track angle, in degrees

    // Fix information.
    bool fix;
    int fix_quality, satellites;
  };

  ///
  /// List of supported timezone shortcuts. Not likely anything extra will have
  /// to be added here.
  ///
  enum Timezone {
    UTC, AEST, AEDT
  };

  ///
  /// Constructor + Destructor.
  ///
  GPS(HardwareSerial& connection, DueTimer* timer=&Timer1);
  ~GPS();

  ///
  /// Initialise the GPS module.
  ///
  bool Initialise();

  ///
  /// Change the timezone of the resultant data. You can either specify a custom
  /// timezone (just specify the hours/minutes difference) or use a hard-coded
  /// constant.
  ///
  void SetTimezone(int hours, int minutes);
  void SetTimezone(Timezone t);

  ///
  /// Get GPS location from the module.
  ///
  const Location* location();

private:
  // Actual Adafruit GPS object.
  Adafruit_GPS _gps;

  // We only require this because we can't initialise it when we are
  // constructed.
  HardwareSerial* _connection;

  // Timer to use. Required to read GPS data constantly.
  DueTimer* _timer;

  // Storage location for thr returned location.
  Location _location;

  // Timezone information.
  int _timezone_hours, _timezone_minutes;

  // True if the IMU has been initialised.
  bool _initialised;

  // Utility method: update _location to match any required timezone changes.
  // Will account for overflowing minutes/days/months/years.
  void HandleTimezoneChange();

  // Utility method: convert a latitude + longitude measurement to a decimal
  // degrees notation (easier to use).
  float ToDecimalDegrees(float latlng, char direction);

  // Interrupt function: read GPS information and check for any NMEA sentences.
  friend void _gps_read_interrupt();

  // Instance required by the _gps_read_interrupt(). Implies that GPS is a
  // singleton, which is kinda is... this will not work if there are multiple
  // GPSes (but that's never going to happen?).
  static GPS* _instance;
};

#endif  // _GPS_H_
