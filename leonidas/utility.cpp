#include "utility.h"

namespace Utility {

bool SDCopy(const char* source_filename, const char* dest_filename) {
  // First, make sure the source exists.
  File source = SD.open(source_filename, FILE_READ);
  if (!source) {
    return false;
  }

  // Now, remove and create the destintion.
  SD.remove(const_cast<char*>(dest_filename));
  File dest = SD.open(dest_filename, FILE_WRITE);
  if (!dest) {
    source.close();
    return false;
  }

  // Now, copy the one buffer at a time.
  char buffer[256];
  int buffer_size = 0;
  while (source.available()) {
    buffer[buffer_size++] = source.read();
    if (buffer_size > sizeof(buffer)) {
      dest.write(buffer, buffer_size);
      buffer_size = 0;
    }
  }

  // Flush the buffer.
  if (buffer_size > 0) {
    dest.write(buffer, buffer_size);
  }

  // All done!
  source.close();
  dest.close();
  return true;
}

bool Equal(const char* a, const char* b) {
  return strcasecmp(a, b) == 0;
}

} // namespace Utility
