#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <SD.h>

namespace Utility {

///
/// Copy one file to another file on the SD card.
///
bool SDCopy(const char* source_filename, const char* dest_filename);

///
/// Compare two strings for equality.
///
bool Equal(const char* a, const char* b);

} // namespace Utility

#endif  // _UTILITY_H_
