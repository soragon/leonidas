#include "command.h"

const int kFrameLength = Communication::AX25::kFrameLength;
const int kInfoLength = Communication::AX25::kInfoLength;

using Utility::Equal;

Command::Command() {
  _data.id = INVALID;
}

Command::~Command() {

}

Command Command::Parse(Transmitter::CharacterBuffer* buffer) {

  // Validate the buffer. It should be non-null and have at least kFrameLength
  // bytes in it (i.e. we've received a whole frame).
  if (buffer == NULL || buffer->size() < kFrameLength) {
    return Command();
  }

  // Create the new command.
  Command command = Command();

  // Save a reference to the data.
  Data* data = &command._data;

  // Frame buffer.
  char frame[kFrameLength];
  memset(frame, '\0', sizeof(frame));

  // Load the frame into memory.
  for (int i = 0; i < kFrameLength; i++) {
    frame[i] = buffer->Next();
  }

  // Grab some key parts of the frame.
  char flag1 = frame[0];
  char flag2 = frame[kFrameLength - 1];
  char* addresses = &(frame[1]);
  char control = frame[15];
  char pid = frame[16];
  char fcsh = frame[17 + kInfoLength];
  char fcsl = frame[17 + kInfoLength + 1];

  if (flag1 != flag2) {
    Log::Error("Flag mismatch while reading frame: {%c} != {%c}", flag1, flag2);
    buffer->Reset();
    return Command();
  }

  /// Extract and process the info.
  char* info = &(frame[17]);

  // Find the first newline character; this is the end of the command and the
  // start of the data.
  char* command_end = strchr(info, Communication::kTerminatingCharacter);
  *command_end = '\0';
  char* command_str = info;
  char* data_str = command_end + 1;

  // Tokenize the command into components using strtok().
  char* token = strtok(command_str, " ");

  // SET command: "SET <key> <value>"
  if (Equal(token, "SET")) {
    data->id = SET;

    /// Load arguments.
    // Key.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.SET.key, sizeof(data->args.SET.key), "%s", token);
    }

    // Value.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.SET.value, sizeof(data->args.SET.value), "%s", token);
    }
  }

  // GET command: "GET <key>"
  else if (Equal(token, "GET")) {
    data->id = GET;

    /// Load arguments.
    // Key.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.GET.key, sizeof(data->args.GET.key), "%s", token);
    }
  }

  // DOWNLOAD command: "DOWNLOAD <filename>"
  else if (Equal(token, "DOWNLOAD")) {
    data->id = DOWNLOAD;

    /// Load arguments.
    // Filename.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.DOWNLOAD.filename,
        sizeof(data->args.DOWNLOAD.filename), "%s", token);
    }
  }

  // DOWNLOAD-BLOCK command: "DOWNLOAD-BLOCK <filename> <block_id>"
  else if (Equal(token, "DOWNLOAD-BLOCK")) {
    data->id = DOWNLOAD_BLOCK;

    /// Load arguments.
    // Filename.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.DOWNLOAD_BLOCK.filename,
        sizeof(data->args.DOWNLOAD_BLOCK.filename), "%s", token);
    }

    // Block ID.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      data->args.DOWNLOAD_BLOCK.block = atoi(token);
    }
  }

  else if (Equal(token, "UPLOAD")) {
    data->id = UPLOAD;

    /// Load arguments.
    // Filename.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.UPLOAD.filename,
        sizeof(data->args.UPLOAD.filename), "%s", token);
    }

    // Block start.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      data->args.UPLOAD.block_start = atoi(token);
    }

    // Block length.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      data->args.UPLOAD.block_length = atoi(token);
    }

    // Load the data.
    for (int i = 0; i < data->args.UPLOAD.block_length; i++) {
      data->args.UPLOAD.block_data[i] = data_str[i];
    }

    return command;
  }

  else if (Equal(token, "LIST")) {
    data->id = LIST;
  }

  else if (Equal(token, "REMOVE")) {
    data->id = REMOVE;

    /// Load arguments.
    // Filename.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    } else {
      snprintf(data->args.REMOVE.filename,
        sizeof(data->args.REMOVE.filename), "%s", token);
    }
  }

  else if (Equal(token, "SELFIE")) {
    data->id = SELFIE;

    /// Load arguments.
    // None to load...
  }

  else if (Equal(token, "RESET")) {
    data->id = RESET;

    /// Load arguments.
    // None to load...
  }

  else if (Equal(token, "TELEMETERY")) {
    data->id = TELEMETERY;

    /// Load arguments.
    // X.
    if (!(token = strtok(NULL, " "))) {
      data->id = INVALID;
    }

    // If the token is equal to "SUN", the control via the sun.
    if (Equal(token, "SUN")) {
      data->args.TELEMETERY.use_sun = true;
      data->args.TELEMETERY.x = 0;
      data->args.TELEMETERY.y = 0;
      data->args.TELEMETERY.z = 0;
    } else {
      data->args.TELEMETERY.use_sun = false;
      data->args.TELEMETERY.x = atoi(token);

      // Y.
      if (!(token = strtok(NULL, " "))) {
        data->id = INVALID;
      } else {
        data->args.TELEMETERY.y = atoi(token);
      }

      // Z.
      if (!(token = strtok(NULL, " "))) {
        data->id = INVALID;
      } else {
        data->args.TELEMETERY.z = atoi(token);
      }
    }
  }

  else {
    Log::Error("Unrecognised command {%s}.", token);

    // We probably have an invalid buffer if this happened. To be safe, just
    // clear it. This isn't such a great idea for actual production, but should
    // be OK for a proof of concept.
    buffer->Reset('\0');
  }

  return command;
}

bool Command::IsValid(Transmitter::CharacterBuffer* buffer) {
  return buffer->size() >= Communication::AX25::kFrameLength;
}
