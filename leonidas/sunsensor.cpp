#include "sunsensor.h"

SunSensor::SunSensor(int pin, int x, int y, int z)
  : _pin(pin) {
  pinMode(pin, INPUT);

  _direction_vector[0] = x;
  _direction_vector[1] = y;
  _direction_vector[2] = z;
}

SunSensor::~SunSensor() {

}

double SunSensor::Value() const {
  return ValueNormalised() * kSupplyVoltageLevel;
}

double SunSensor::ValueNormalised() const {
  #if defined (__arm__)
    const int possible_values = pow(2, 12);
  #else
    const int possible_values = pow(2, 10);
  #endif

  return ((analogRead(_pin) + 1) / ((float)possible_values));
}

double SunSensor::Theta() const {
  return 1.1635 * (1 - ValueNormalised());
}

void SunSensor::FindSun(
  const SunSensor& a, const SunSensor& b//, const SunSensor& c
  , double* sun_vector) {
  CalculateSunVectorComponent(a, sun_vector);
  CalculateSunVectorComponent(b, sun_vector);
  //CalculateSunVectorComponent(c, sun_vector);
}

#include "log.h"

void SunSensor::CalculateSunVectorComponent(
  const SunSensor& a, double* sun_vector) {
  if (a._direction_vector[0] != 0) {
    Log::Info("x component (%d) = %.2f (%.2f, %.2f)", a._pin, a.Theta(), a.ValueNormalised(), cos(a.Theta()));
    sun_vector[0] = cos(a.Theta()) * a._direction_vector[0];
  } else if (a._direction_vector[1] != 0) {
    Log::Info("y component (%d) = %.2f (%.2f, %.2f)", a._pin, a.Theta(), a.ValueNormalised(), cos(a.Theta()));
    sun_vector[1] = cos(a.Theta()) * a._direction_vector[1];
  } else if (a._direction_vector[2] != 0) {
    sun_vector[2] = cos(a.Theta()) * a._direction_vector[2];
  }
}
