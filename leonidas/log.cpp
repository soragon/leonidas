#include "log.h"

// Include the full definition of Communication here.
#include "communication.h"

// Allocation of static variables.
Log::Level Log::_max_level = Log::DEBUG;

#ifdef LOG_SERIAL_ENABLE
// Serial stream to write to. It must be initialised to work properly.
Stream* Log::_serial = NULL;
#endif

#ifdef LOG_COMMS_ENABLE
// Communication device to write log to.
Communication* Log::_comms = NULL;
#endif

#ifdef LOG_SCREEN_ENABLE
UTFT* Log::_display = NULL;
int Log::_current_display_line = 1;
#endif

#ifdef LOG_FILE_ENABLE
File Log::_file;
char* Log::_filename;
#endif

char Log::_format_buffer[Log::kFormatBufferSize];
char Log::_user_format_buffer[Log::kFormatBufferSize];
bool Log::_initialised = false;

// Font selection.
extern uint8_t SmallFont[];
static uint8_t* FONT = SmallFont;

bool Log::Initialise() {
  _initialised = true;

  // Setup the LED.
  pinMode(kLED, OUTPUT);
  digitalWrite(kLED, LOW);
  return true;
}

void Log::Debug(const char* fmt, ...) {
  if (!_initialised) {
    return;
  }

  va_list args;
  va_start(args, fmt);
  LogToAll(DEBUG, fmt, args);
  va_end(args);
}

void Log::Info(const char* fmt, ...) {
  if (!_initialised) {
    return;
  }

  va_list args;
  va_start(args, fmt);
  LogToAll(INFO, fmt, args);
  va_end(args);
}

void Log::Error(const char* fmt, ...) {
  if (!_initialised) {
    return;
  }

  va_list args;
  va_start(args, fmt);
  LogToAll(ERROR, fmt, args);
  va_end(args);
}

void Log::Serial(Level level, const char* message) {
#ifdef LOG_SERIAL_ENABLE
  if (!_initialised || level < _max_level || _serial == NULL) {
    return;
  }

  const char* final_message = FormatHeader(level, message);
  _serial->println(final_message);
#endif
}

void Log::Comms(Level level, const char* message) {
#ifdef LOG_COMMS_ENABLE
  if (!_initialised || level < _max_level || _comms == NULL) {
    return;
  }

  const char* final_message = FormatHeader(level, message);
  _comms->LogMessage(final_message);
#endif
}

void Log::Screen(Level level, const char* message) {
#ifdef LOG_SCREEN_ENABLE
  if (!_initialised || level < _max_level || _display == NULL) {
    return;
  }

  // Setup the board based on the log level.
  switch (level) {
    case DEBUG:
      _display->setColor(200, 200, 200);
      break;
    case INFO:
      _display->setColor(255, 255, 255);
      break;
    case ERROR:
      _display->setColor(255, 0, 0);
      break;
    default:
      _display->setColor(255, 255, 255);
      break;
  }

  const char* final_message = FormatHeader(level, message);
  _display->print(final_message, 1, _current_display_line);
  _current_display_line += FONT[1];

  if (_current_display_line + FONT[1] > _display->getDisplayYSize()) {
    _display->clrScr();
    _current_display_line = 1;
  }
#endif
}

void Log::File(Log::Level level, const char* message) {
#ifdef LOG_FILE_ENABLE
  if (!_initialised) {
    return;
  }

  if (!_file) {
    // If no file has been specified, do nothing.
    if (_filename == NULL) {
      return;
    }

    // If a file was specified but the file isn't open, open it.
    _file = SD.open(_filename, FILE_WRITE);
  }

  const char* final_message = FormatHeader(level, message);
  _file.println(final_message);
  _file.flush();
#endif
}

void Log::serial(Stream* serial) {
#ifdef LOG_SERIAL_ENABLE
  _serial = serial;
#endif
}

void Log::comms(Communication* comms) {
#ifdef LOG_COMMS_ENABLE
  _comms = comms;
#endif
}

void Log::display(UTFT* display) {
#ifdef LOG_SCREEN_ENABLE
  _display = display;
  _display->setFont(SmallFont);
  _display->clrScr();
#endif
}

void Log::file(const char* filename, bool overwrite) {
#ifdef LOG_FILE_ENABLE
  // Close any currently open file.
  if (_file) {
    _file.close();
  }

  // If requested, delete the file before opening it.
  if (overwrite) {
    SD.remove(const_cast<char*>(filename));
  }

  // Open the file for writing.
  _file = SD.open(filename, FILE_WRITE);
#endif
}

void Log::FormatInto(char* buffer, int size, const char* fmt, va_list args) {
  #ifdef __AVR__
    vsnprintf_P(buffer, size, fmt, args);
  #else
    vsnprintf(buffer, size, fmt, args);
  #endif
}

const char* Log::FormatHeader(Log::Level level, const char* message) {
  snprintf(_format_buffer, sizeof(_format_buffer)
    , "[%s] %s", level_to_string(level), message);
  return _format_buffer;
}

void Log::LogToAll(Log::Level level, const char* fmt, va_list args) {
  // If this is an error, turn on the LED.
  if (level == ERROR) {
    digitalWrite(kLED, HIGH);
  }

  FormatInto(_user_format_buffer, sizeof(_user_format_buffer), fmt, args);
  Serial(level, _user_format_buffer);
  Screen(level, _user_format_buffer);
  File(level, _user_format_buffer);
  Comms(level, _user_format_buffer);
}

const char* Log::level_to_string(Level level) {
  switch (level) {
    case DEBUG: return "DEBUG";
    case  INFO: return " INFO";
    case ERROR: return "ERROR";
    default: return "";
  }
}
