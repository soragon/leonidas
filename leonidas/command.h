#ifndef _COMMAND_H_
#define _COMMAND_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "communication.h"
#include "log.h"
#include "transmitter.h"
#include "utility.h"

class Command {
 public:
  static const int kMaxParameterNameLength = 32;
  static const int kMaxParameterValueLength = 32;
  static const int kMaxFilenameLength = 32;
  static const int kMaxDataLength = Communication::AX25::kInfoLength;

  ///
  /// Constructor + Destructor.
  ///
  Command();
  ~Command();

  ///
  /// Parse the given command into a Command object. It is assume that this is
  /// a complete command (and nothing more); this should be called when the
  /// TERMINATING_CHARACTER is received. This does not mean the end of input in
  /// all cases (e.g. BLOCK).
  ///
  static Command Parse(Transmitter::CharacterBuffer* buffer);

  ///
  /// Check whether the given buffer has a valid command inside it (because of
  /// the framing we are using, just make sure it has the correct number of
  /// characters).
  ///
  static bool IsValid(Transmitter::CharacterBuffer* buffer);

  enum ID {
    INVALID

    // Write operations (response from S: ACK).
    , SET, UPLOAD, BLOCK, SELFIE, REMOVE, TELEMETERY

    // Read operations (response from S: varied).
    , GET, DOWNLOAD, DOWNLOAD_BLOCK, LIST

    // Special commands.
    , RESET
  };

  struct Data {
    ID id;

    union {
      /// Satellite commands (GS --> S).
      // SET arguments.
      struct {
        char key[kMaxParameterNameLength];
        char value[kMaxParameterValueLength];
      } SET;

      // GET arguments.
      struct {
        char key[kMaxParameterNameLength];
      } GET;

      // DOWNLOAD arguments.
      struct {
        char filename[kMaxFilenameLength];
      } DOWNLOAD;

      // DOWNLOAD-BLOCK arguments.
      struct {
        char filename[kMaxFilenameLength];
        int block;
      } DOWNLOAD_BLOCK;

      // UPLOAD arguments.
      struct {
        char filename[kMaxFilenameLength];
        int block_start;
        int block_length;
        char block_data[kMaxDataLength];
      } UPLOAD;

      // LIST arguments.
      struct {

      } LIST;

      // REMOVE arguments.
      struct {
        char filename[kMaxFilenameLength];
      } REMOVE;

      // SELFIE arguments.
      struct {

      } SELFIE;

      // RESET arguments.
      struct {

      } RESET;

      // TELEMETERY arguments.
      struct {
        bool use_sun;
        int x, y, z;
      } TELEMETERY;
    } args;
  };

  const Data* data() const { return &_data; }
  const int id() const { return _data.id; }
  bool operator==(ID id) const { return id == _data.id; }

 private:
  Data _data;
};

#endif  // _COMMAND_H_
