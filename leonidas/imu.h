#ifndef _IMU_H_
#define _IMU_H_

// Common libraries.
#include <Wire.h>

// IMU Libraries.
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_BMP085_U.h>
#include <Adafruit_Simple_AHRS.h>

// UV Libraries.
#include "Adafruit_SI1145.h"

#include "log.h"

///
/// Interface class to the Adafruit IMU.
///
class IMU {
public:
  ///
  /// Constructor + Destructor.
  ///
  IMU();
  ~IMU();

  ///
  /// Initialise the IMU.
  ///
  bool Initialise();

  ///
  /// Get the orientation vector and return it.
  ///
  sensors_vec_t Orientation();

  ///
  /// Get the current temperature in degrees C.
  ///
  double Temperature();

  ///
  /// Get the current pressure in kPa.
  /// TODO(jeshuam): Verify the units of this.
  ///
  double Pressure();

  ///
  /// Getter methods for the UV sensor.
  ///
  int ReadUV();
  int ReadIR();
  int ReadVisible();
  int ReadProx();

private:
  Adafruit_LSM303_Accel_Unified _accelerometer;
  Adafruit_LSM303_Mag_Unified _magnetometer;
  Adafruit_BMP085_Unified _barometer;
  Adafruit_Simple_AHRS _ahrs;
  float _sea_level_pressure;
  Adafruit_SI1145 _uv;

  // True if the IMU has been initialised.
  bool _initialised;
};

#endif  // _IMU_H_
