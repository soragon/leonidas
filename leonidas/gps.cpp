#include "gps.h"

GPS* GPS::_instance = NULL;

void _gps_read_interrupt() {
  GPS* _gps = GPS::_instance;
  if (_gps != NULL) {
    // Get the GPS to read the next character.
    _gps->_gps.read();

    // Parse a new sentence (if available).
    if (_gps->_gps.newNMEAreceived()) {
      _gps->_gps.parse(GPS::_instance->_gps.lastNMEA());
    }
  }
}

GPS::GPS(HardwareSerial& connection, DueTimer* timer)
  : _gps(&connection), _connection(&connection), _timer(timer)
  , _initialised(false) {
  _instance = this;
}

GPS::~GPS() {

}

bool GPS::Initialise() {
  // Setup the GPS connection.
  _gps.begin(9600);
  _connection->begin(9600);

  // Initialise the GPS.
  _gps.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  _gps.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  _gps.sendCommand(PGCMD_ANTENNA);

  // Start refreshing GPS information.
  _timer->attachInterrupt(_gps_read_interrupt);
  _timer->start(kReadDelay * 1000);

  // Delay for a while to allow some GPS information to accumulate.
  delay(1000);

  Log::Info("GPS: Initialised!");
  _initialised = true;
  return true;
}

void GPS::SetTimezone(int hours, int minutes) {
  _timezone_hours = hours;
  _timezone_minutes = minutes;

  // Update the current time.
  HandleTimezoneChange();
}

void GPS::SetTimezone(GPS::Timezone t) {
  switch (t) {
    // Australian Eastern Standard Time (Sydney, Melbourne)
    case AEST:
      SetTimezone(10, 0);
      break;

    // Australian Eastern Standard Daylight Savings Time.
    case AEDT:
      SetTimezone(11, 0);
      break;

    // Coordinated Universal Time (what the GPS returns).
    case UTC:
    default:
      SetTimezone(0, 0);
      break;
  }
}

const GPS::Location* GPS::location() {
  if (!_initialised) {
    return NULL;
  }

  // Update the time.
  _location.hour = _gps.hour;
  _location.minute = _gps.minute;
  _location.second = _gps.seconds;
  _location.year = _gps.year;
  _location.month = _gps.month;
  _location.day = _gps.day;
  _location.milliseconds = _gps.milliseconds;
  HandleTimezoneChange();

  // Update the location.
  _location.latitude = ToDecimalDegrees(_gps.latitude, _gps.lat);
  _location.longitude = ToDecimalDegrees(_gps.longitude, _gps.lon);
  _location.altitude = _gps.altitude;
  _location.speed = _gps.speed * 0.514;
  _location.angle = _gps.angle;

  // Update the fix.
  _location.fix = _gps.fix;
  _location.fix_quality = _gps.fixquality;
  _location.satellites = _gps.satellites;

  return &_location;
}

void GPS::HandleTimezoneChange() {
  /// Handle timezone changes.
  // What happens if minutes roll over?
  _location.minute += _timezone_minutes;
  if (_location.minute >= 60) {
    _location.hour++;
    _location.minute -= 60;
  } else if (_location.minute < 0) {
    _location.minute = -1 * _location.minute;
    _location.minute -= 60;
    _location.hour--;
  }

  // What happens if hours roll over?
  const int DaysAMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  _location.hour += _timezone_hours;
  if (_location.hour >= 24) {
    _location.hour -= 24;
    _location.day++;

    if (_location.day > DaysAMonth[_location.month - 1]) {
      _location.day = 1;
      _location.month++;

      if (_location.month > 12) {
        _location.year++;
      }
    }
  } else if (_location.hour < 0) {
    _location.hour += 24;
    _location.day--;

    if (_location.day < 1) {
      if (_location.month == 1) {
        _location.month = 12;
        _location.year--;
      } else {
        _location.month--;
      }

      _location.day = DaysAMonth[_location.month - 1];
    }
  }
}

float GPS::ToDecimalDegrees(float latlng, char direction) {
  // Account for direction changes.
  int multiplier = 1;
  if (direction == 'S' or direction == 'W') {
    multiplier = -1;
  }

  // Perform the conversion.
  float ddmmss = latlng / 100.0;
  int degrees = (int)ddmmss;
  float minute_seconds = ((ddmmss - degrees) * 100) / 60.0;
  return multiplier * (degrees + minute_seconds);
}
