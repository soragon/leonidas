from __future__ import print_function

import serial
import select
import sys
import re
import os
import time

from collections import defaultdict

from threading import Thread, Timer

from Tkinter import *
import tkFileDialog
from tkFont import Font
from ttk import Progressbar
# from tkinter import *
# from tkinter import filedialog as tkFileDialog
# from tkinter.font import Font
# from tkinter.ttk import Progressbar

class AX25(object):
  # Flag bit at the start and end of the frame
  kFlag = chr(0b01111110)

  # Address information.
  kDestinationAddress = 'LEON  '
  kSourceAddress = 'USYD  '

  # The destination SSID changes based on transmission of science or
  # navigation data.
  kSciencePackage = chr(0b00001111)
  kOrbitPackage = chr(0b00001110)
  kSourceSSID = chr(0b01100000)

  # Control frames indicating UI frames and responses.
  kControlNeedAck = chr(0b00010011)
  kControlNoAck = chr(0b00000011)

  # PID bit indicating no Layer 3 protocol is used
  kPid = chr(0xF0)

  # Length of the information packet.
  kInfoLength = 512

  # Overall length of the whole frame.
  # FLAG + ADDRESSES + CONTROL + PID + INFO + FCS + FLAG.
  kFrameLength = 1 + 14 + 1 + 1 + kInfoLength + 2 + 1

class Application(Frame):
  def AddLogMessage(self, message):
    """
    Add a new log message received from the satellite. This message will be
    displayed in the log area.
    """
    self.log_area.config(state=NORMAL)
    self.log_area.insert(END, message + '\n')
    self.log_area.config(state=DISABLED)
    self.log_area.see(END)

  def SendCommandWithFrame(self, command, data='', science_data=False, expect_ack=False):
    frame = ''

    # Attach the header.
    frame += AX25.kFlag
    frame += AX25.kDestinationAddress
    frame += AX25.kSciencePackage if science_data else AX25.kOrbitPackage
    frame += AX25.kSourceAddress
    frame += AX25.kSourceSSID
    frame += AX25.kControlNeedAck if expect_ack else AX25.kControlNoAck
    frame += AX25.kPid

    # Attach the info.
    frame += command
    frame += '\n'
    frame += data
    frame += '\0' * (AX25.kInfoLength - (len(command) + len(data) + 1))

    # Attach the footer.
    frame += chr(0xFF) + chr(0xFF)
    frame += AX25.kFlag

    # Send the command.
    print('Sending "{}" ({})'.format(frame, len(frame)))
    self.leonidas.write(frame)

  def SendCommand(self):
    command = self.command_entry.get()
    self.command_entry.delete(0, END)

    self.SendCommandWithFrame(command, expect_ack=True)

  def DownloadTimeout(self):
    """
    If a block isn't received before (timeout) has passed, then request another
    block.
    """
    print('[Retrying...]')
    self.RequestNextDownloadBlock()

  def RequestNextDownloadBlock(self):
    to_download = len(self.download_file_blocks)
    for i in range(len(self.download_file_blocks)):
      if i not in self.download_file_blocks:
        to_download = i
        break

    command = 'DOWNLOAD-BLOCK {} {}'.format(self.download_filename, to_download)
    self.SendCommandWithFrame(command, science_data=True, expect_ack=True)

    self.download_timeout = Timer(10, self.DownloadTimeout)
    self.download_timeout.start()

  #
  # Reading thread; block infinitely to try and read lines from the Arduino. Once
  # a line is read, add it to the to process queue.
  #
  def ReadLines(self):
    INFO_LENGTH = 512 # bytes

    while self.connected:
      try:
        #message = flag
        message = ''

        # FLAG + ADDRESSES + CONTROL + PID + INFO + FCS + FLAG
        for i in range(1 + 14 + 1 + 1 + INFO_LENGTH + 2 + 1):
          message += self.leonidas.read()

        print('Message received: "{}"'.format(message))

      # Probably caused by a failed connection, because the main loop stopped.
      except serial.SerialException as e:
        print('Exception while reading! {}'.format(e))
        return

      # Extract the components of the message.
      flag = message[0]
      addresses = message[1:15]
      control = message[15:16]
      pid = message[16:17]
      try:
        command, data = message[17:17 + INFO_LENGTH].split('\n', 1)
      except ValueError:
        continue
      fcs = message[17 + INFO_LENGTH:17 + INFO_LENGTH + 2];
      flag2 = message[-1]

      # Just display it for now.
      if flag == flag2:
        # Log info, just display it.
        if command.startswith('LOG '):
          self.AddLogMessage(command[4:])

        elif command.upper() == "PING":
          self.AddLogMessage('[{}] Ping!'.format(time.strftime("%A, %B %d %Y (%I:%M:%S %p)")))

        # GET command, display it.
        elif re.match('GET .*? .*?', command):
          cmd, key, val = command.split()
          self.AddLogMessage('[RX] {}={}'.format(key, val))

        elif re.match('BLOCK .*', command):
          self.download_timeout.cancel()

          cmd, file, bid, nblocks, size = command.split()

          # Convert things to ints.
          bid = int(bid)
          nblocks = int(nblocks)
          size = int(size)

          # Create the progress bar if we haven't already.
          self.download_progress.config(maximum=nblocks)

          # If we haven't received this block before, increase the progress.
          if bid not in self.download_file_blocks:
            self.download_progress.step()

          self.download_file_blocks[bid] = data[0:size]

          if len(self.download_file_blocks) == int(nblocks):
            with open(file, 'wb') as output_file:
              for k, v in sorted(self.download_file_blocks.items()):
                output_file.write(v)
          else:
            self.RequestNextDownloadBlock()

        elif re.match('ACK UPLOAD', command):
          self.upload_block += 1
          self.UploadNextBlock()

        elif command == 'LIST':
          self.AddLogMessage('[RX] Available files: {}'.format(data.strip('\0').strip()))

        # Other response, log it to the console.
        else:
          print('[RX] {}'.format(command))
          # Empty the read buffer.
          self.leonidas.readall()

  def UpdatePorts(self, interval):
    """
    Thread to constantly update available COM ports. Will do so once every
    `interval` seconds (can be fractional).
    """
    while self.running:
      ports = self.GetListOfPorts()
      if ports != self.connection_port_list:
        self.connection_port_list = ports

        menu = self.connection_port['menu']
        menu.delete(0, END)
        if self.connection_port_val.get() not in ports:
          self.connection_port_val.set(ports[0])
        for port in self.connection_port_list:
          menu.add_command(label=port, command=lambda value=port:self.connection_port_val.set(value))
      time.sleep(interval)

  def Connect(self):
    """
    Connect to the satellite.
    """
    # UI control.
    self.connect_button.config(state=DISABLED)
    self.connection_status.config(text='Connecting...', fg='green')

    # Clear the log area.
    self.log_area.config(state=NORMAL)
    self.log_area.delete('1.0', END)
    self.log_area.config(state=DISABLED)

    # Actually connect.
    try:
      self.leonidas = serial.Serial(self.connection_port_val.get(), self.connection_baud.get(), timeout=2) 
    except serial.SerialException as e:
      self.AddLogMessage('Could not connect to LEONIDAS: {}'.format(e))
      self.connection_status.config(text='Not Connected', fg='red')
      self.connect_button.config(state=NORMAL)
      return
    self.connected = True
    self.initialised = False

    # Disable port config.
    self.connection_port.config(state=DISABLED)

    # Start the log reading thread.
    self.log_thread = Thread(target=self.ReadLines)
    self.log_thread.start()

    # UI control.
    self.connection_status.config(text='Connected', fg='green')
    self.disconnect_button.config(state=NORMAL)
    self.command_send_button.config(state=NORMAL)
    self.upload_file_button.config(state=NORMAL)
    self.download_file_button.config(state=NORMAL)

  def Disconnect(self):
    """
    Disconnect from the satellite.
    """
    # UI control.
    self.disconnect_button.config(state=DISABLED)
    self.command_send_button.config(state=DISABLED)
    self.upload_file_button.config(state=DISABLED)
    self.download_file_button.config(state=DISABLED)
    self.connection_status.config(text='Disconnecting...', fg='red')

    # Actually diconnect.
    self.connected = False
    self.leonidas.close()
    self.log_thread.join()
    self.leonidas = None

    # UI control.
    self.connection_status.config(text='Disconnected', fg='red')
    self.connect_button.config(state=NORMAL)
    self.connection_port.config(state=NORMAL)

  def GetListOfPorts(self):
    ports = []

    # Windows.
    if os.name == 'nt':
      import _winreg as winreg

      reg_path = 'HARDWARE\\DEVICEMAP\\SERIALCOMM'
      try:
        key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, reg_path)
      except WindowsError as e:
        return ['-']

      for i in xrange(256):
        try:
          val = winreg.EnumValue(key, i)
          ports.append(val[1])
        except EnvironmentError:
          break

    # Linux
    elif os.name == 'posix':
      import glob
      ports += glob.glob('/dev/ttyUSB*') + glob.glob('/dev/ttyACM*')# + glob.glob('/dev/ttyS*')

    if len(ports) == 0:
      ports = ['-']
    return ports;

  def UploadFile(self):
    filename = tkFileDialog.askopenfilename()

    print('uploading {}'.format(os.path.basename(filename)))

    BLOCK_SIZE = 400

    # Split the file into blocks.
    data = open(filename, 'rb')
    self.upload_filename = os.path.basename(filename)
    self.upload_file_data = []
    with open(filename, 'rb') as file:
      while True:
        block = bytearray(file.read(BLOCK_SIZE))
        if not block:
          break

        self.upload_file_data.append(block)

    self.upload_block = 0
    self.UploadNextBlock()

  def UploadNextBlock(self):
    BLOCK_SIZE = 400
    if self.upload_block >= len(self.upload_file_data):
      self.upload_progress.config(maximum=len(self.upload_file_data), value=0)
      print('Done uploading file')
      return

    # build the command
    self.upload_progress.config(maximum=len(self.upload_file_data), value=self.upload_block)
    print('Uploading block {}/{}'.format(self.upload_block, len(self.upload_file_data)))
    command = 'UPLOAD {} {} {}'.format(
      self.upload_filename
      , BLOCK_SIZE * self.upload_block
      , len(self.upload_file_data[self.upload_block]))

    self.SendCommandWithFrame(command
      , data=self.upload_file_data[self.upload_block]
      , science_data=True, expect_ack=True)

  def DownloadFile(self):
    filename = self.download_file_input.get()
    self.download_file_blocks = {}
    self.download_filename = filename

    command = 'DOWNLOAD {}'.format(filename)
    self.SendCommandWithFrame(command, science_data=True, expect_ack=True)

  def createWidgets(self):
    ##
    ## Control area.
    ##
    self.control_frame = Frame(self)
    self.control_frame.grid(row=0, column=0, padx=10, pady=10)

    self.control_label = Label(self.control_frame, text='Connection Status', font=Font(family='Helvetica', size=18, weight='bold'))
    self.control_label.grid(row=0, column=0, columnspan=2)

    self.connection_status = Label(self.control_frame, text='Not Connected', fg='red', font=Font(family='Helvetica', size=18, weight='bold'), relief=GROOVE, padx=10, pady=10, width=13)
    self.connection_status.grid(row=1, column=0, columnspan=2, pady=5)

    self.connection_port_label = Label(self.control_frame, text='Port:')
    self.connection_port_label.grid(row=2, column=0, sticky=E)

    self.connection_port_list = self.GetListOfPorts()
    self.connection_port_val = StringVar(self.control_frame)
    self.connection_port_val.set(self.connection_port_list[0])
    self.connection_port = OptionMenu(self.control_frame, self.connection_port_val, *self.connection_port_list)
    self.connection_port.grid(row=2, column=1, sticky=E + W)

    self.connection_baud_label = Label(self.control_frame, text='Baud:')
    self.connection_baud_label.grid(row=3, column=0, sticky=E)

    self.connection_baud = Entry(self.control_frame)
    #self.connection_baud.insert(0, '115200')
    self.connection_baud.insert(0, '38400')
    self.connection_baud.grid(row=3, column=1)

    self.connect_button = Button(self.control_frame, text="Connect", command=self.Connect, width=10)
    self.connect_button.grid(row=4, column=0, sticky=E + W)
    self.disconnect_button = Button(self.control_frame, text="Disconnect", command=self.Disconnect, width=10, state=DISABLED)
    self.disconnect_button.grid(row=4, column=1, sticky=E + W)

    ##
    ## Construct the command textbox.
    ##
    self.command_frame = Frame(self)
    self.command_frame.grid(row=1, column=0, padx=10, pady=10)

    self.command_frame_title = Label(self.command_frame, text='Commands', font=Font(family='Helvetica', size=18, weight='bold'))
    self.command_frame_title.grid(row=0, column=0, columnspan=2)

    def handleCommandEntry(e):
      if self.connected and e.keysym == 'Return':
        self.SendCommand()
    self.command_entry = Entry(self.command_frame)
    self.command_entry.bind(sequence='<KeyPress>', func=handleCommandEntry)
    self.command_entry.grid(row=1, column=0)

    self.command_send_button = Button(self.command_frame, text='Send', command=self.SendCommand, width=10, state=DISABLED)
    self.command_send_button.grid(row=1, column=1)

    self.upload_file_button = Button(self.command_frame, text='Upload File', command=self.UploadFile, state=DISABLED)
    self.upload_file_button.grid(row=2, column=0, columnspan=2)

    self.upload_progress = Progressbar(self.command_frame, maximum=100)
    self.upload_progress.grid(row=3, column=0, columnspan=2, sticky=W + E)

    self.download_file_input = Entry(self.command_frame)
    self.download_file_input.grid(row=4, column=0)

    self.download_file_button = Button(self.command_frame, text='Download File', command=self.DownloadFile, state=DISABLED)
    self.download_file_button.grid(row=4, column=1)

    self.download_progress = Progressbar(self.command_frame, maximum=100)
    self.download_progress.grid(row=5, column=0, columnspan=2, sticky=W + E)

    ##
    ## Construct the logging area.
    ##
    self.log_frame = Frame(self)
    self.log_frame.grid(row=0, column=1, rowspan=2, padx=10, pady=10, sticky=S + E)

    self.log_area_label = Label(self.log_frame, text='Log Output', font=Font(family='Helvetica', size=18, weight='bold'))
    self.log_area_label.grid(row=0, column=0)

    self.log_area = Text(self.log_frame, height=20, width=80, state=DISABLED)
    self.log_area.grid(row=1, column=0, sticky=N + S + E + W)

  def __init__(self, master=None):
    self.download_progress = None # progress bar
    self.upload_progress = None
    self.leonidas = None
    self.connected = False
    self.running = True
    self.download_timeout = Timer(10, self.DownloadTimeout)
    self.download_file_blocks = {}
    self.download_filename = ''
    Frame.__init__(self, master)
    self.pack()
    self.createWidgets()

    # Create update ports thread.
    self.update_ports_thread = Thread(target=self.UpdatePorts, args=[0.01])
    self.update_ports_thread.start()

  def __enter__(self):
    return self

  def __exit__(self, type, value, traceback):
    self.running = False
    if self.leonidas:
      self.leonidas.close()

root = Tk()
with Application(master=root) as app:
  app.mainloop()
